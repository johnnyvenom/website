---
# Display name
title: John Sullivan

# Username (this should match the folder name)
# authors:
# - admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: research / design / music

# Organizations/Affiliations
organizations:
- name: CIRMMT
  url: https://cirmmt.org
- name: IDMIL (associate researcher)
  url: http://idmil.org
- name: ex)situ - LISN (alumni)
  url: https://ex-situ.lri.fr/
- name: Université Paris-Saclay
  url: https://www.universite-paris-saclay.fr/
- name: McGill University
  url: https://mcgill.ca
# - name: CIRMMT
#   url: "https://www.cirmmt.org" 

# Short bio (displayed in user profile at end of posts)
bio: Postdoctoral researcher exploring research through design in the areas of music, movement, dance, and human-computer interaction. 

interests:
- Music Technology
- Movement and Embodied Interaction
- Interface Design
- User Experience
- Cycling
- Coffee

education:
  courses:
  - course: Ph.D. in Music Technology
    institution: McGill University
    year: 2021
  - course: MFA in Intermedia
    institution: University of Maine
    year: 2015
  - course: BFA in Contemporary Music Performance
    institution: College of Santa Fe
    year: 2003

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'mailto:johnny@johnnyvenom.com'  # For a direct email link, use "mailto:test@example.org".
  - icon: mastodon
    icon_pack: fab
    link: https://hci.social/@johnnyvenom
  - icon: camera-retro
    icon_pack: fas
    link: https://pixelfed.social/johnnyvenom
  - icon: bluesky
    icon_pack: fab
    link: https://bsky.app/profile/johnnyvenom.bsky.social
  - icon: google-scholar
    icon_pack: ai
    link: https://scholar.google.ca/citations?user=3CTPYngAAAAJ
  - icon: gitlab
    icon_pack: fab
    link: https://gitlab.com/johnnyvenom
  - icon: github
    icon_pack: fab
    link: https://github.com/johnnyvenom
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/john-sullivan-cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

John Sullivan is a researcher, designer, and musician residing in Montreal, Canada. His research focuses on the development of new technologies for music, dance and multimedia performance. As a designer, he creates interactive tools and media for dance and music productions, and produces web and multimedia content for a wide variety of creative outlets. 

Working under the pseudonym Johnny Venom, he has been a part of multiple indie rock groups from the northeastern US. He released several albums with various projects and has toured extensively in the US, Canada, and Europe. Most recently he co-created the interactive dance work "For Patricia", where he composed and performed the music, and designed the interactive technology for the AI-mediated live performance. 

<!-- He holds a Ph.D. in Music Technology from McGill University (2021) and an M.F.A. in Intermedia from the University of Maine (2015).  -->


<!-- With a background in music performance and human-computer interaction, his work includes user research, participatory design workshops and collaborations with musicians and performing artists to better understand and support professional performance practices. Additionally he has conducted research in the areas of motion capture analysis of live performance, haptic interaction, and accessible digital musical instrument design. 

As a designer, he creates interactive tools and media for dance and music productions, and produces web and multimedia content for a wide variety of creative outlets. 

Under the name Johnny Venom, he has been a part of multiple indie rock groups from the northeastern US. He released several albums with various projects and has toured extensively in the US, Canada, and Europe. Most recently he co-created the interactive dance work "For Patricia", where he composed and performed the music, and designed the interactive technology for the AI-mediated live performance.  -->

