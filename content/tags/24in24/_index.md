---

title: 🇪🇺 24 in 24
subtitle: In the spring of 2022, I accepted a post-doctoral fellowship at the University of Paris-Saclay that would last for two years, from September 2022 until August 2024. Between work and getting comfortable in Paris, we set about to explore the corners of France and Europe, and made a few well-timed trips back home as well. Over 24 months, we will average one trip per month, roughly speaking, thus, the 24 in 24 series was born. 
summary: ""
type: page

# image:
#   caption: ""
#   focal_point: ""
#   preview_only: false

view: 2
columns: 2

  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   5 = Showcase

---
<br/>

<!-- Super interesting here... see uMap to set up an interactive map! -->

<iframe width="100%" height="300px" frameborder="0" allowfullscreen allow="geolocation" src="//umap.openstreetmap.fr/en/map/untitled-map_1107163?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true"></iframe><p><a href="//umap.openstreetmap.fr/en/map/untitled-map_1107163?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true">See full screen</a></p>

