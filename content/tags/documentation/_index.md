---

title: Documentation
subtitle: These pages contain additional and in-depth documentation on certain research and creative projects.  
summary: ""
type: page

# image:
#   caption: ""
#   focal_point: ""
#   preview_only: false

view: 5
columns: 3

  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   5 = Showcase

---
<br/>