---

title: India
subtitle: At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.
summary: ""
type: page

# image:
#   caption: ""
#   focal_point: ""
#   preview_only: false

view: 2
columns: 2

  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   5 = Showcase

---
