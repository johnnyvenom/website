---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Experiments Demos & B-Sides"
summary: "A ragged collection of flotsam and jetsam from the early years."
authors: []
tags: ["music"]
categories: []
date: 2006-05-01T11:58:59-04:00
draft: false

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

<div style="width:600px; margin:auto">
<hr>
<br>
<iframe style="border: 0; width: 600px; height: 307px;" src="https://bandcamp.com/EmbeddedPlayer/album=2274732712/size=large/bgcol=333333/linkcol=da145d/artwork=none/transparent=true/" seamless><a href="https://johnnyvenom.bandcamp.com/album/experiments-demos-b-sides">Experiments Demos &amp; B-sides by Johnny Venom</a></iframe>
<hr>
</div>

- **Year(s):** 2000 - 2006
- **Contribution(s):** A ragged collection of flotsam and jetsam from the early years. All instruments and production by Johnny Venom. 