---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Post Provost"
summary: "A collective of indie/folk/post-rock led by David Gagne."
authors: []
tags: ["music"]
categories: []
date: 2012-01-01T11:58:11-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

<iframe src="https://open.spotify.com/embed/artist/5hNZ6kNLBrK9VWdarZJ46I" width="100%" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<!-- <iframe src="https://open.spotify.com/embed/album/3IKTVRxkMSGtaZSVu5wr82" width="720" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe> -->

<br>

- **Year(s):** 2010 - 2012  
- **Contribution(s):** Upright bass on album "Ancient Open Allegory Oratorio"; upright and electric bass, keyboards, glockenspiel and vocals for live band.