---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "EastWave Radio"
summary: "Indie roots reggae from Maine."
authors: []
tags: ["music", "touring"]
categories: []
date: 2008-03-15T11:58:00-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

<iframe src="https://open.spotify.com/embed/artist/1Ew9s3LEnYXBQLz49JOdFD" width="100%" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<!-- <div style="width:680px; margin:auto">

<hr>
<br>
<iframe style="border: 0; width: 680px; height: 340px;" src="https://bandcamp.com/EmbeddedPlayer/album=1485814010/size=large/bgcol=333333/linkcol=da145d/artwork=small/transparent=true/" seamless><a href="https://johnnyvenom.bandcamp.com/album/soulful-noise">Soulful Noise by EastWave Radio</a></iframe>
<hr>
</div> -->


- **Year(s):** 2007 - 2012  
- **Contribution(s):** Bass guitar and backing vocals on EP "Soulful Noise"; touring and countless live shows.
<!-- - **Streaming:** [Spotify]() | [Apple Music]() -->