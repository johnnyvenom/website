---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Jaye Drew and A Moving Train"
summary: "Soulful and eclectic R&B."
authors: []
tags: ["music"]
categories: []
date: 2007-01-01T11:58:34-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

<iframe src="https://open.spotify.com/embed/artist/6PGdNh3R5mTckLtv8hosLL" width="100%" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<br>

- **Year(s):** 2007 - 2010  
- **Contribution(s):** Bass guitar and production on "A Moving Train", countless live shows
