---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Megan Joe Wilson"
summary: "R&B and neo-soul from one of Maine's finest vocalists."
authors: []
tags: ["music"]
categories: []
date: 2012-07-01T11:59:14-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

<iframe src="https://open.spotify.com/embed/artist/0t106K9HwFCKkZ4PohoECU" width="100%" height="270" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<br>

- **Year(s):** 2011 - 2012  
- **Contribution(s):** Bass guitar on album "Tin", live band