---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Town Founder"
summary: "Traveling music by the one and only Johnny Fountain, who was taken from this world too soon."
authors: []
tags: ["music"]
categories: []
date: 2011-08-19T11:58:26-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

<iframe src="https://open.spotify.com/embed/artist/2OqLX4855biQcxe7Z4rt8C" width="100%" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<br>

- **Year(s):** 2011 - 2012  
- **Contribution(s):** Bass, Hammond organ, Fender Rhodes, Wurlitzer, piano, and Mellotron on album "Be Prepared". 

<br>

#### In memory of [Johnny Fountain](https://factoryportland.com/local-music/who-was-johnny-fountain/), a wonderful musician and even greater human being. 