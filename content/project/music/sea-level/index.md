---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Sea Level"
summary: "Sometimes funky, sometimes spooky, occasionally esoteric, and always cinematic music from the mind of Dan Capaldi."
authors: []
tags: ["music"]
categories: []
date: 2009-09-12T11:58:42-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

<div style="width:700px; margin:auto">
<hr>
<br>
<iframe style="border: 0; width: 100%; height: 307px;" src="https://bandcamp.com/EmbeddedPlayer/album=719894284/size=large/bgcol=333333/linkcol=da145d/artwork=small/transparent=true/" seamless><a href="https://sealevel.bandcamp.com/album/anjuli">Anjuli by Sea Level</a></iframe>
<hr>
</div>


<br>

- **Year(s):** 2008 - 2011  
- **Contribution(s):** Electric and upright bass, keyboards; recorded 2 EPs and played numerous live shows