---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "From Pythagoras to LaMonte"
summary: ""
authors: []
tags: ["installation", "interactive media", "research"]
categories: []
date: 2013-04-12T12:40:54-04:00
draft: true

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

During my M.F.A. studies at the [University of Maine](https://maine.edu), I created this combination installation/lecture piece to contextualize the interdisciplinary research I had been doing on harmony, tuning systems, timbre and sound synthesis. With Pythagoras and La Monte Young as historical bookends, the piece reviewed how harmonic concepts have been understood and utilized in composition from traditional music theory through modern and experimental compositional forms. 

The piece itself was an additive synthesis-driven surround audio environment, controlled by computer vision. A grid was laid out on the floor, through which visitors could walk and control several audio parameters that served to sonically demonstrate audio principles. 

In addition to research in audio synthesis and harmony, much of the design of this piece was based on interface design and mapping structures. The room was designed as an interactive space (the primary interface), while an iPad interface was designed for wireless control of the synthesizer’s several different modes (secondary interface). The sound synthesis and interaction was developed in Max, with additional audio production in Ableton. 

---

## Images

{{< gallery resize_options="500x" album="lamonte" >}}
