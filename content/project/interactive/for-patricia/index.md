---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "For Patricia"
summary: "For Patricia is a live performance for two dancers, two musicians, and one AI, based on the methods and movements of postmodern choreographer Trisha Brown."
subtitle: Generative dance and music performance based on the work of Trisha Brown. 
authors: []
tags: ["interactive media", "music", "dance", "research", "AI"]
categories: []
date: 2024-03-21T10:03:53+01:00


# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "📸: Mircea Topoleanu"
  focal_point: "Center"
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

{{< callout note >}}

<h3 style="text-align:center">🚨 FALL 2024 UPDATE: For Patricia is touring! 🚨</h3>
<br/>

We are excited to bring the show to four cities in Europe in October 2024. For all the details see the [Fall Tour page]({{< relref "blog/20240801-for-patricia-fall-tour" >}}).

{{< /callout >}}

“For Patricia” is an ode to postmodernism in Dance. 

“For Patricia”, or for Trisha, is in honor of one of the inventors of postmodernism Trisha Brown (1936-2017) whose choreographic style challenged traditional notions of dance and pushed the boundaries of dance by expanding what movement and choreography meant. 

Just like Brown explored choreographic structures such as accumulation, repeating, and reversing, etc., we explore ways to use compositional structures generated through Artificial Intelligence (AI) to organize simple choreographic and sonic patterns. We see AI as providing scaffolding to the piece rather than taking on the role of an interactive scenography. It creates the score that the dancers and musicians perform on stage. Each show is a renewed piece where both dancers and musicians perform a new score that the AI conducts them to perform.  These scores are displayed visually on stage for the audience to decipher how the dance and the music unfold while maintaining a sense of ambiguity that provides an aesthetic experience of movement and music for their own sake. 

The piece is a quartet between two dancers: Sarah Fdili Alaoui and one other (Bartosz Ostrowski for the premiere and Pauline Lavergne for additional performances); and two musicians: John Sullivan (piano and electronics) and Léo Chédin (drums and electronics).

### MODINA residency

This work was developed in residency at the [CNDB](https://cndb.ro) (National Centre for Dance Bucharest) in Bucharest, Romania, as part of the EU co-funded [MODINA](https://modina.eu/about/) (Movement, Digital Intelligence and Interactive Audience) project. The 8-week residency took place between 15/1/2024 and 10/3/2024, culminating with an artist talk and a public showing.

Additional performance dates are scheduled in fall 2024 throughout the EU. See the [Fall Tour page]({{< relref "blog/20240801-for-patricia-fall-tour" >}}) for details.

---

## Credits


- Choreography: Sarah Fdili Alaoui 
- Composition: John Sullivan 
- Dancers: Sarah Fdili-Alaoui, with Bartek Ostrowski and Pauline Lavergne
- Musicians: John Sullivan and Léo Chédin 
- AI development: John Sullivan, Sarah Fdili Alaou- and Léo Chédin
- Source materials provided by the Trisha Brown Company [trishabrowncompany.org/](https://trishabrowncompany.org/)
- Technical Mentorship and development support- MIREVI [mirevi.de](https://mirevi.de)
- Graphic Design: Diane-Line Farré
- Video and Documentation: Mircea Topoleanu
- Production and Tech support: CNDB [cndb.ro](https://cndb.ro/)

## Support

- Modina Consortium, Winner of the Modina residency program : https://modina.eu/projects/for-patricia/
- INRIA, Université Paris Saclay, CNRS, Creative Computing Institute, University of the Arts London 

---

## 🎥 Videos

Performance at the National Centre for Dance Bucharest, March 1st, 2024. Videos by [Mandeep Basi](https://sociotekno.com). 

{{< video src="albums/for_patricia_video_MB/for_patricia_1.mp4" controls="yes" >}}
{{< video src="albums/for_patricia_video_MB/for_patricia_2.mp4" controls="yes" >}}
{{< video src="albums/for_patricia_video_MB/for_patricia_3.mp4" controls="yes" >}}
{{< video src="albums/for_patricia_video_MB/for_patricia_4.mp4" controls="yes" >}}

## 📸 Images

MODINA residency: rehearsals, performance, and talk. All photos by Mircea Topoleanu. 

{{< gallery album="for-patricia" >}}