---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "High Striker!"
summary: ""
authors: []
tags: ["installation", "interactive media"]
categories: []
date: 2014-04-12T12:02:53-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

"High Striker!" was a large scale installation developed in collaboration with sculptor and intermedia artist John Carney. Based on the classic carnival game of strength and accuracy, this version was a video-based fully playable game for up to 6 people. 

Each player was given a mallet and attempted to strike the target with enough force to trigger a full video playback. The videos, which were projected onto a large multi-panel window, behaved much like the mechanism in the classic game. In the original, the force of the mallet strike would propel a lead weight vertically towards a bell. The player wins if the target is struck hard enough to ring the bell. In our recreated version, the force controlled the playback of a random video clip, propelling it forward according to the strength of the blow. If the force was not strong enough, the video would begin to play but then slow and reverse back to the beginning. 

The most compelling part of the installation design was the integration of large-scale and large-force interfaces with digital video manipulation. We crafted wooden mallets and concrete pedestals equipped with self-designed force sensors that detected strike velocity. We experimented with several different sensor designs before we arrived at an innovative and very DIY solution. We had done tried piezoelectric sensors previously but found them far too fragile and inaccurate to yield an accurate variable analog input signal. As the act of hitting a target with a large mallet generates a huge amount of force, to the naked eye the problem seemed to be one of scale: how could we translate a large force into something small enough to be measured by a small inexpensive sensor? The answer seemed to be that we could convert the energy of the blow into some intermediate stage that could be more easily measured by the resources we had at our disposal. After testing a spring mechanism without success, we tried a rubber bouncy ball suspended on a rubber band over a piezo sensor. Despite its crude materials this system actually worked very well. 

{{< figure src="high-striker-sensor.jpg">}}

In its finished form, a wooden "trigger" was mounted on top of a hollow concrete pedestal. On the underside of the trigger, a piezo sensor was attached to a flat panel, below which a bouncy ball was suspended on a rubber band. The trigger was seated on hard rubber blocks on top of the concrete pedestal, so a firm strike would compress the trigger but only slightly. When struck, the energy of the blow would travel down to the plate and rubber ball, which would then bounce off the piezo several times, depending on the force of the strike. The number of bounces above a predetermined threshold gave an accurate measurement of the force and accuracy of the strike.

## Images

{{< gallery resize_options="500x" album="high_striker" >}}