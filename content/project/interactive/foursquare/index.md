---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "fourSQUARE: Death by Pop Song"
summary: "An installation in collaboration with Sally Levi."
authors: []
tags: ["installation", "interactive media"]
categories: []
date: 2013-03-01T12:02:42-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
# url_video: "https://vimeo.com/63339098"

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

"fourSQUARE: Death by Pop Song" was an installation done in collaboration with [Sally Levi](https://www.sallylevi.com) during our M.F.A. studies at the at the [University of Maine](https://maine.edu) in 2013. The piece explores the emotional depth of the playground through this familiar lens. Utilizing activity sensing technology, spatialized sound, Max/MSP processing, pop music, lighting and physical installation, the soundscape evolves as people move through the space, winding through desolation to joyful exuberance and beyond as more join in.

---

{{< vimeo 63339098 >}}

## Images

{{< gallery resize_options="500x" album="foursquare" >}}