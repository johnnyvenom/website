---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Inside Out"
summary: "Inside Out was a site-specific one-time multimedia installation presented as part of the Montréal en Lumière 2015 official program."
authors: []
tags: ["installation", "interactive media"]
categories: []
date: 2015-02-15T12:03:02-04:00

# Optional external URL for project (replaces project detail page).
# external_link: "https://insideout.johnnyvenom.com"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

{{< vimeo 135427471 >}}

---

**Project website:** [insideout.johnnyvenom.com](https://insideout.johnnyvenom.com)

---

The work “Inside Out” confronts the spectator with the conflicting concepts exemplified by internet culture, such as the public outrage over surveillance and data gathering activities by intelligence services and corporations, while more and more personal information is willfully uploaded to social networks, sharing services, and alike. The fear of losing touch with the “outside” world of tomorrow, the social need for communication, affiliation and personal expression, as well as the promises of friendship, recognition, etc. give rise to a digital mass culture of self-display and -branding. The digital traces of these actions in turn, are gathered by corporations and intelligence agencies and used for professional and commercial purposes. The quantity of individual profiles (no matter how "customized"), conditioned by pre-conceived notions of personality and identity implied by the medium, can often lead to a self-objectification, loss of empathy, superficial connectivity, and social isolation.

The concept for the piece was developed by Marlon Shumacher and Graham Boyes, and the work was designed and installed by collaboratively by Shumacher, Boyes and myself.
