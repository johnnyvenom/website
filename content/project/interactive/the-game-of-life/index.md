---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The Game of Life"
summary: "The Game of Life is a sextet for three dancers and three musicians, whose ambition is to find new performative relationships between dance and music."
subtitle: Interactive dance performance by *Le principe d'incertitude*
authors: []
tags: ["interactive media", "research", "dance"]
categories: []
date: 2023-04-15T12:03:39+02:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "First rehearsal at CCN - Ballet de Lorraine (February 2022). 📸: CCN - Ballet de Lorraine"
  focal_point: "Center"
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

The Game of Life is a sextet for three dancers and three musicians, whose ambition is to find new performative relationships between dance and music. The piece's score, seen as a cellular ecosystem, is generative, and based on principles of reaction, signalling, and cooperation between the six performers.

The title of the piece is a reference to the simulation game conceived by the English mathematician John Horton Conway, in which groups of cells evolve according to their environment, based on simple rules, and which, from iteration to iteration, produce complex and intricate patterns. The Game of Life is the proposal of a form; an experience that embodies the porosity and fragility of our bodies crossed by sound and movement.

For this piece, I served in the dual role of interaction designer and embedded researcher. As designer, I worked with the creators to develop and integrate four interactive software modules connecting the rules of the game, stage lights, and audio together to deliver the performance that unfolds in real time, negotiated by the system and the performers onstage in a continually evolving choreography. As researcher, I documented my own work, as well as the emergent creation through two residencies leading up to the premiere performance. The work has been presented at the *[ACM Designing Interactive Systems conference (DIS)](https://dis.acm.org/2023/)* and is published in the conference proceedings. Read the paper **_[here]({{< relref "/publication/sullivan-2023a" >}})_**. 

---

## Credits

Choreography · **Pierre Godard** and **Liz Santoro** (**[Le principe d'incertitude](https://http://www.lpdi.org)**)  
Music · **[Pierre-Yves Macé](https://pierreyvesmace.com)**  
Dancers · **Mark Lorimer**, **Philippe Renard**, **Liz Santoro**  
Musicians from **[L'Instant Donné](http://www.instantdonne.net/)** · **Maxime Echardour**, **Saori Furukawa**, **Mayu Sato-Brémaud**  
Space · **Mélanie Rattier**  
Lighting · **Pierre Godard** and **Mélanie Rattier**  
Interaction design & research · **John Sullivan**  
Costumes · **Marguerite Tenot** and **Liz Santoro**  
Sound engineer · **Aria de la Celle**  
Lighting technician · **Emmanuel Fornès**  

Production · Le principe d'incertitude et L'Instant Donné. Coproduction · Cndc Angers, CCN - Ballet de Lorraine, CCN de Caen en Normandie, Théâtre du Beauvaisis. Support · Caisse des dépôts Mécénat, Ernst Von Siemens Foundation, SACEM, CNC - DICRéAM, La POP, CN D Centre national de la danse, Atelier de Paris / CDCN

---

## Performances

- NOV 22, 2022 | Cndc Angers | Angers, FR  
- NOV 26, 2022 | Espace Bernard Mantienne | Verrières-le Buisson, FR  
- MAY 31, 2023 | Théâtre du Beauvaisis | Beauvais, FR  
- JUN 13 to JUN 14, 2023 | Carreau du Temple | JUNE EVENTS | Paris, FR  
- APR 26 to APR 27, 2024 | Théâtre Garonne | Toulouse, FR  
- MAY 24 to MAY 25, 2024 | Scène de Recherche de l'ENS Paris-Saclay | Gif-sur-Yvette, FR  

For more information: http://www.lpdi.org/

---

## Publication

{{< cite page="publication/sullivan-2023a" view="4" >}}

---