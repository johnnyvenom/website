---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "EMI Survey"
summary: ""
authors: []
tags: ["research"]
categories: []
date: 2018-02-18T11:56:04-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
# url_pdf: "/project/research/emi-survey/CMMR_poster.pdf"
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Digital musical instruments are frequently designed in research and experimental performance contexts but few are taken up into sustained use by active and professional musicians. To identify the needs of performers who use novel technologies in their practices, a survey of musicians was conducted that identified desirable qualities for instruments to be viable in active use, along with attributes for successful uptake and continued use of instruments based on frameworks of long and short term user engagement. The findings are presented as a set of design considerations towards the development of instruments intended for use by active and professional performers.

**Website:** [emisurvey.johnnyvenom.com](https://emisurvey.johnnyvenom.com)

This survey was carried out as part of my doctoral research at the [Input Devices and Music Interaction Laboratory](http://idmil.org) (IDMIL). Initial findings were [presented]({{< relref "/publication/sullivan-2019" >}}) at the 2019 International Symposium on Computer Music Multidisciplinary Research (CMMR) in Marseille, France. The complete results of the study appear in my [Ph.D. dissertation]({{< relref "publication/sullivan-2021-phd">}}) on digital musical instrument design for professional performers and are published in the [Journal for New Music Research]({{< relref "sullivan-2022" >}}). 

{{< figure src="CMMR_poster.png" caption="Poster presented at CMMR.">}}

## Publications

{{< cite page="sullivan-2019" view="4" >}}
{{< cite page="sullivan-2021-phd" view="4" >}}
{{< cite page="sullivan-2022" view="4" >}}


