---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Gestural Control of Augmented Instrumental Performance"
summary: "This research creation project explored interface design for gestural control of instrumental performance through a motion capture study and the development of new hardware and software."
authors: []
tags: ["research", "design"]
categories: []
date: 2017-09-21T12:30:14-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
# url_pdf: "http://www-new.idmil.org/wp-content/uploads/2017/03/Sullivan-et-al.-2018-Gestural-Control-of-Augmented-Instrumental-Performance.pdf"
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

A gestural control system was developed to augment harp performance with real-time control of computer-based audio affects and processing. While the lightweight system was designed for use alongside any instrument, our choice of the concert harp represented a unique case study in gestural control of music. The instrument's large size and physically demanding playing technique leaves the performer with little spare bandwidth to devote to other tasks. A motion capture study analyzed instrumental and ancillary gestures of natural harp performances that could be mapped effectively to control of additional signal processing parameters. The initial findings of the study helped to guide the design of custom gesture control devices and user software, and a new work for solo harpist and electronics was created. 

This work was carried out at the [Input Devices and Music Interaction Laboratory](http://idmil.org) (IDMIL) and the [Centre for Interdisciplinary Research in Music Media and Technology](https://www.cirmmt.org) (CIRMMT), in collaboration with [Alexandra Tibbitts](https://alexandratibbitts.com), Olafur Bogason ([Genki Instruments](https://genkiinstruments.com)), and [Brice Gatinet](https://soundcloud.com/brice-gatinet). 

The project was presented in performance at the 2018 International Conference on Live Interfaces (ICLI) in Porto, Portugal, and we published a [paper]({{< relref "/publication/sullivan-2018">}}) about the work for the 2018 Movement and Computing Conference (MOCO) in Genoa, Italy.

## Video

{{< vimeo 291366942 >}}

<p style="text-align:center">Debut performance, Montreal, Quebec. (2017)</p>

## Images

{{< gallery resize_options="500x" album="gestural-control" >}}

{{< figure src="harp_gesture_mocap_example.gif" >}}