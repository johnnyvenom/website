---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Adaptive Use Musical Instrument"
summary: "AUMI - the Adaptive Use Musical Instrument - is a project that founded and directed by Pauline Oliveros. AUMI is an accessible digital musical instrument designed to allow individuals of all abilities to make music."
authors: []
tags: ["research", "design", "accessibility"]
categories: []
date: 2015-01-01T12:34:54-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

AUMI - the Adaptive Use Musical Instrument - is a project that founded and directed by Pauline Oliveros. AUMI is an accessible digital musical instrument designed to allow individuals of all abilities to make music. The project is headquartered at Rensselaer Polytechnic Institute in Troy, New York. A number of other sites, including McGill University, form a consortium across which a broad array of researchers, community members, artists, therapists and developers contribute to the many different facets of the project. From around 2010 to 2019, researcher from the [Input Devices and Music Interaction Laboratory](http://idmil.org) (IDMIL) have been in charge of support and development for the AUMI desktop application.

The AUMI software interface enables the user to play sounds and musical phrases through movement and gestures. This is an entry to improvisation that enables exploration of sounds ranging from pitches to noises rather than learning set pieces. This open approach to music enables anyone to explore and express a range of affects, both by themselves and in response to, or in conversation with, others. While the AUMI interface can be used by anyone, the focus has been on working with children who have profound physical disabilities. In taking these participants as its starting point the project attempts to make musical improvisation and collaboration accessible to the widest possible range of individuals. This approach also opens up the possibility of learning more about the relations between ability, the body, creativity and improvisation, from within a cultural context that does not always acknowledge or accept people with disabilities.

The AUMI project continues to be revised and improved with input from the technologists, students, therapists and feedback from registered users. An on-site training program is also available. The AUMI software is available for Windows, Mac OS, and iOS; the latest versions can be downloaded from the AUMI project webpage and in the iTunes Store.

#### Visit the AUMI project online at: [aumiapp.com](http://aumiapp.com/)

---

## AUMI Research Project Participants

- Pauline Oliveros (Principal Investigator and Coordinator of AUMI, 2006-2016)
- Leaf Miller (co-founder, practitioner)
- Ione (Founding Consultant & AUMI Dream Facilitator)
- Sherrie Tucker (Coordinator, AUMI-KU InterArts)
- Ellen Waterman (Coordinator, AUMI-Memorial)
- Gillian Siddall (Coordinator, AUMI-OCAD)
- Eric Lewis (Coordinator, AUMI-McGill)
- Jesse Stewart (AUMI-Carleton)
- Jonas Braasch (Director of the Center for Cognition, Communication, an
- Culture, where AUMI is Headquartered)
- Thomas Ciufo (Technical Support and Programming)
- Henry Lowengard (AUMI for iOS Designer/Programmer)
- …and many others: for more information on the full team behind AUMI, visit the [official AUMI website](http://aumiapp.com/teams.php).

## IDMIL Participants:

- [John Sullivan](http://idmil.org/people/john-sullivan/) (2016 - 2019)
- [Ivan Franco](http://idmil.org/people/ivan-franco/) (2015 - 2016)
- [Ian Hattwick](http://idmil.org/people/ian-hattwick/) (2012 - 2015)
- Chuck Bronson (2013 - 2014)
- [Aaron Krajeski](http://idmil.org/people/aaron-krajeski/) (2012 - 2013)
- [Doug Van Nort](http://idmil.org/people/doug-van-nort/)

---

{{< figure src="AUMI_interface.jpg" caption="The AUMI desktop instrument">}}



