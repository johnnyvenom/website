---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Bionic Harpist"
summary: ""
authors: []
tags: ["research", "design"]
categories: []
date: 2020-09-01T12:29:13-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

The Bionic Harpist is a is a research-creation collaboration with harpist [Alexandra Tibbitts](https://alextibbitts.com/). Building upon a previous work [Gestural Control of Augmented Instrumental Performance]({{< relref "../gestural-control-of-augmented-instrumental-performance" >}}), we present a new musical interface for the concert harp. Where the previous project focused on free-handed gesture for the control of extra-musical parameters during performance, the resulting wearable hardware was not instrument specific. This current work directly augments the harp with a custom sensor interface that is physically coupled with the instrument itself, providing the harpist with an ergonomic set of controls to expand their instrumental performance with additional computer-based synthesis and processing. Our design approach exploits the large and readily accessible surface of the harp as an ergonomic layout for manipulation, leveraging the tacit embodied connection between performer and instrument. From a research perspective, the project investigates the impact of – and methodologies for – participatory design of musical interfaces between instrument builders and performers, and the development of new works for new and evolving instruments.

Since the development of the Bionic Harpist controllers, Tibbitts has been performing regularly with them, including high profile appearances at the MUTEK Montreal, Japan, and Mexico festivals in 2020 and 2021. 

The Bionic Harpist project was developed as part of my Ph.D. research at the [Input Devices and Music Interaction Laboratory](http://idmil.org) and has been generously supported by the [Centre for Interdisciplinary Research in Music Media and Technology](https://www.cirmmt.org) (CIRMMT) at McGill University. Tibbitts and I continue to collaborate together with ideas for new instruments and performances to come later in 2021. 

Project website: https://bionic-harpist.gitlab.io/bionicharp.ist/

---

## Press 

- “[La harpiste bionique: comme dans un film de science-fiction](https://www.24heures.ca/2020/09/02/la-harpiste-bionique-comme-dans-un-film-de-science-fiction).” 24h Montréal *(french)*
- “[La Harpiste bionique : polyphonie amplifiée](https://lienmultimedia.com/spip.php?article77039).” \[PODCAST\] Lien Multimédia *(french)*

---

## Video

{{< youtube VDHlymgApTc >}}

<p style="text-align:center"><strong>Video:</strong> Music Rooms | National Arts Centre (2021)</p>

## Images

{{< gallery resize_options="500x" album="bionic-harpist" >}}