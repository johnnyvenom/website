---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Design for Performance"
summary: ""
authors: []
tags: ["research", "design"]
categories: []
date: 2019-01-15T12:17:00-04:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

"Design for Performance" is a project that entailed the development and execution of co-design workshops held with expert musicians to imagine novel musical instruments through design fiction. The workshops were based on the Magic Machine workshops developed by Kristina Andersen, in which participants crafted non-functional prototypes of instruments they would want to use in their own performance practice. Through in-situ activities and post-workshop thematic analysis, a set of design specifications were developed that can be applied to the design of new digital musical instruments intended for use in real-world artistic practice. In addition to generating tangible elements for design, the theories and methods utilized, based in human-computer interaction and human-centered design, can be seen as a possible model for merging imaginative idea generation with functional design outputs. 

This work carried out as part of my Ph.D. research at the [Input Devices and Music Interaction Laboratory](http://idmil.org) with the generous support of the [Centre for Interdisciplinary Research in Music Media and Technology](https://www.cirmmt.org) (CIRMMT) at McGill University. 

This project is the subject of a chapter from my [Ph.D. thesis]({{< relref "/blog/20210314-phd-defence" >}}) "Built to Perform: Designing Digital Musical Instruments for Professionals" and has subsequently been published in the [Computer Music Journal](http://computermusicjournal.org/). The design specifications generated from the workshops were utilized in the development of new instruments in a series called [the Noiseboxes]({{< relref "../noisebox" >}}). 

---

## Images

{{< gallery resize_options="500x" album="dfp-gallery" >}}

---

## Publications: 

{{< cite page="/publication/sullivan-2021-phd" view="4" >}}
{{< cite page="/publication/sullivan-2023" view="4" >}}