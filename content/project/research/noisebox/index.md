---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The Noiseboxes"
summary: "The Noiseboxes are a group of standalone embedded musical instruments that are the product of ongoing research on user-centered digital musical instrument design."
authors: []
tags: ["research", "design", "NIME", "DMI"]
categories: []
date: 2020-12-05T09:19:29-05:00
draft: false

# Optional external URL for project (replaces project detail page).
# external_link: "http://idmil.org/project/noisebox/"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image: 
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

The Noiseboxes are a group of standalone embedded musical instruments. They are the products of an ongoing research/creation project that examines how novel musical instruments can be designed to foster long-term engagement and sustained use in music performance. Three generations of Noiseboxes have been created so far. The first generation was a proof of concept that came out of practice based research in the design of embedded DMIs. A second generation followed, with several instruments produced using the Prynth framework for self-contained DMIs for a user study. For the third and current generation, three distinct versions have been developed with key design specifications coming out of a set of [co-design workshops]({{< relref "../design-for-performance" >}}) held with expert musicians.

Future work for the instruments is planned, including a longitudinal study where the finished instruments will be given back to workshop participants to to follow how they are taken up into their own performance practices.

The Noiseboxes have been an ongoing part of my graduate research. They began as an early investigation into embedded DMI design when I came to McGill University as a [graduate research trainee](https://www.mcgill.ca/gps/students/graduate-research-trainee) while I completed my masters degree from the [University of Maine](https://maine.edu).

---

## Images

### Versions 1 and 2

{{< gallery resize_options="500x" album="v1-and-v2">}}

### Version 3

{{< gallery resize_options="500x" album="v3" >}}