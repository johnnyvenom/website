---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Designing Purpose-Built Digital Musical Instruments"
event: "RPI Arts Graduate Colloquium"
event_url:
location: online (webEx)
address:
  street: "Rensselaer Polytechnic Institute"
  city: "Troy"
  region: "NY"
  postcode:
  country:
summary: "I discuss my ongoing research in digital musical instrument design through the presentation of two design projects focusing on the specific demands of two different use cases: First, a software-based adaptive musical instrument for accessible music making, and second, bespoke musical interfaces for a professional contemporary harpist."
abstract: "As a Ph.D. researcher in music technology, my work focuses on human-computer interaction and the development and applications of digital musical instruments (DMIs). Specifically, my interest lies in the design of technology that can be taken up into sustainable, long-term artistic practice, and my practice-based work has been made up of several applied design projects in which instruments have been developed and used in different artistic contexts. In this talk I discuss my research through the through the presentation of two design projects focusing on the specific demands of two different use cases: AUMI, a software-based adaptive musical instrument for accessible music making, and The Bionic Harpist, designing bespoke musical interfaces for a professional contemporary harpist."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-11-11T11:30:00-05:00
date_end: 2020-11-11T01:30:00-05:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-11-09T12:13:49-05:00

authors: []
tags: ["colloquium", "AUMI", "Bionic Harpist", "DMI", "design"]

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
links:
- name: "AUMI - The Adaptive Use Musical Instrument"
  url: http://idmil.org/project/aumi-the-adaptive-use-musical-instrument/
  # icon_pack: fab
  # icon: twitter
- name: "The Bionic Harpist"
  url: http://idmil.org/project/the-bionic-harpist/
  # icon_pack: fab
  # icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: RPI_colloquium_Nov_2020.pdf

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

The [slides](RPI_colloquium_Nov_2020.pdf) are comprised of two prior presentations:

- "Improvising Across Abilities: Pauline Oliveros and the Adaptive Use Musical Instrument." John Sullivan and Sherrie Tucker, 2018 OHMI Conference on Music and Physical Disability. Birmingham, UK.
- "Augmenting Harp Performance." Alex Tibbitts and John Sullivan. Colloquium d'études supérieures en composition et création sonore. Université de Montréal. Montreal, Canada.

-----

