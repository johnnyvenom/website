---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Actos en Vivo: Extended Topics (3/3)"
slug: javeriana-3
event: "Digital Interfaces for Live Events: Principles and Personalization (summer course)"
event_url:
location: Pontifica Universidad Javeriana
address:
  street: 
  city: Bogotá
  region: Colombia
  postcode:
  country: Colombia
summary: In 2019 I was invited to give three lectures for a summer course on live interface design. 
abstract: '**Lecture 3: Extended topics**. In the final of my three lectures, students learned how to implement Open Sound Control (OSC) with Arduino and  Pure Data, indirect gesture acquisition and inter-application audio routing.'

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-07-02T15:00:00-05:00
date_end: 2019-07-04T18:00:00-05:00
all_day: true

# Schedule page publish date (NOT talk date).
publishDate: 2019-07-02T15:00:00-05:00

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: javeriana_3.pdf

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
