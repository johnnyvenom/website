---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Actos en Vivo: Musical Interface Design (1/3)"
slug: javeriana-1
event: "Digital Interfaces for Live Events: Principles and Personalization (summer course)"
event_url:
location: Pontifica Universidad Javeriana
address:
  street: 
  city: Bogotá
  region: Colombia
  postcode:
  country: Colombia
summary: In 2019 I was invited to give three lectures for a summer course on live interface design. 
abstract: 'In lecture 1, I gave a short history and background of digital musical instrument design. Next, I provided a theoretical framework and practical guide for building DMIs. In the third part of the class, we conducted a workshop where the students crafted non-functional prototypes of interfaces they would like to build. The workshop was a recreation of the Design for Performance workshops I ran as a part of my dissertation research. A short description of the workshop can be found in my [paper](https://www.johnnyvenom.com/publication/sullivan-2020) from NIME 2020.'

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-07-02T15:00:00-05:00
date_end: 2019-07-04T18:00:00-05:00
all_day: true

# Schedule page publish date (NOT talk date).
publishDate: 2019-07-02T15:00:00-05:00

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: javeriana_1.pdf

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
