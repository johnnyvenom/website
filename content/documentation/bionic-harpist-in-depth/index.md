---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Designing "The Bionic Harpist"'
subtitle: Longitudinal participatory research on interface design for professional performance.
summary: Longitudinal participatory research on interface design for professional performance.
authors: []
tags: [research, design, HCI, documentation]
categories: []
date: 2023-11-20T14:48:56+01:00
draft: false

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: "Rehearsing with the Bionic Harp. CIRMMT, Montréal, Canada."
  focal_point: 
  preview_only: false
  alt_text: "A woman sitting in the middle of a large but messy research studio. She is playing a concert harp that is equipped with two electronic controllers that are fitted on either side of the harp strings. They rise up at an angle from the soundboard, and each has a layout of several knobs, buttons, and sliders. The units are made of red 3D printed frames and black acrylic panels. Around the room is strewn a large amount of research equipment: computers, black cases, a violin bow, and other miscellaneous objects." 

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

**The Bionic Harpist is an ongoing project around the research and design of augmented control interfaces for a professional concert harpist. The project has produced three iterations of hardware and software that have been, and still are, used in performances. *Read general information about the project [here]({{< relref "/project/research/bionic-harpist" >}}).***

{{< toc >}}

## Overview

<iframe width="100%" height="365" src="https://www.youtube.com/embed/w7aLIuBngtg?si=vcYfrFAXW2-95O68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<br />
<br />
<p class="video-caption">Alexandra Tibbitts performs with the Bionic Harpist controllers in Montreal, QC. 2021.</p>

{{% callout note %}}

# tl;dr

This project began in 2016 as a design-research collaboration with harpist [Alexandra Tibbitts](https://alextibbitts). The goals were twofold: 

1. To design bespoke interfaces for a concert harp to allow the performer to play live solo electroacoustic music entirely on the harp. 
2. To conduct a practice-based longitudinal study of participatory design with --- *and for* --- professional performers. 

### Activities

1. A motion capture study and analysis of movement and gesture in harp performance. 
2. Design and implementation of a wireless motion gesture system for augmenting instrumental performance.
3. Design and fabrication of hardware interfaces that physically attach to the harp. Two versions have been produced, both used regularly in professional performances. 
4. Evaluation of participatory co-design methods towards successful adoption and long-term use.

{{% /callout %}}

## I: Researching harp gesture

The project began with a motion capture study of harp performance. Eight harpists performed excerpts of harp music in a variety of different expressive styles while being recorded in a motion capture studio. Analysis of the performances revealed both instrumental (sound producing) and ancillary (non-sound producing) gestures. Using this as a guide, we experimented with gestures for processing and modulating sound (sampling and looping, and controlling audio effects). 

{{< figure src="media/harp_gesture_mocap_example.gif" caption="Synchronized motion capture reconstruction of harp performance" alt="An animated gif showing two synchronized videos side by side. On the right, a woman sits in a music studio playing a harp. On the left, the same movement is shown in a 3D reconstruction, with the performer and instrument drawn as points and connecting lines." >}}

{{< figure src="media/mocap_plot.jpg" caption="Comparing left hand the movement of harpists' playing the same excerpt. The X-axis on the left (the harpist's side to side movements) show consistent movements with different amplitudes. The Z-axis on the right (vertical movement) shows more expressive variation of movement between performers." >}}

Working with another collaborator, we developed wearable hardware motion controllers and a performance interface that would map the harpist's movements to standard music software over a wireless network. 

The system was put to use in a new performance for solo harp and electronics. 

{{< figure src="media/motion_controllers_performance.jpg" caption="Tibbitts performs with the gesture controllers" alt="A woman seated on stage playing a concert harp. A computer sits on a table next to her and a music stand is in front of her. Visible on the back of the performer's hand is a small device, which is the wireless gesture acquisition device." >}}

This early work was presented at the MOCO (Movement and Computing) and ICLI (Live Interfaces) conferences.[^1], [^2]

## II: Co-designing hardware controllers

After using the controllers for a period of time, we identified a number of limitations to the system and developed a set of design specifications for a new harp interface: 

1. Physically augment the harp (vs. open-handed gesture)
2. Permits simple configuration into the harpist's normal performance workflow
3. Non-permanent and non-damaging hardware
4. Ergonomic and non-invasive design to afford natural expressive performance. 

Tibbitts and I co-designed the interfaces with participatory approach that included: ideation and sketching, prototyping a t multiple levels of fidelity (non-functional to functional), CAD design and fabrication, testing, customization, and finally, performance. 

---

### Prototyping workflows

{{< figure src="media/prototypes_1.jpg" caption="Non-functional paper prototypes into CAD models" alt="Two images side to side. On the left, a paper prototype in the shape of two panels matching the soundboard of a harp, with cut out buttons and other controls attached. On the left is a 3D CAD model of the same layout in a semi-realistic image." >}}
{{< figure src="media/prototypes_2.jpg" caption="From sketches and CAD to functional digital interfaces" alt="Two images side by side. On the left is a notebook with a sketched GUI containing buttons, knobs and sliders. Next to it is an iPad with the same GUI displayed on the screen. The righthand image is the same GUI represented as a semi-realistic 3D CAD image." >}}
{{< figure src="media/prototypes_3.jpg" caption="Testing ergonomics with cardboard prototypes" alt="There are three images. On the left, A woman sits at a harp, holding two black cardboard rectangles to the harp soundboard on either side of the strings. This represents the approximate location of the controllers. The second image shows a close-up of the woman's hand holding one of the pieces of cardboard at a slight angle, making it easier for the performer to reach. The third panel shows a semi-realistic 3D CAD visualization of the finished controller housing, which has followed the same angled shape and rises up from the harp soundboard." >}}

### Fabrication

{{< figure src="media/assembly_station.jpg" caption="Workstation for controller assembly" alt="A large and cluttered desk is shown, with stacks of connecting wires, electrical components, 3D printed pieces, and tools to assemble the  electronics into controllers." >}}

### Finished hardware and live performance

{{< video src="media/first_tests.mp4" controls="yes" alt="A woman sits at a harp that is equipped with two hardware controllers sitting on the soundboard on either side of the strings. She is playing soem nites while experimenting with the controls and making adjustments on a computer that is on a table next to her. The room she is sitting in is a semi-anechoic chamber, with large patterned foam blocks on the wall." >}}
<p class="video-caption">📽️ First tests of the completed controllers.</p>

Tibbitts has been using the controllers in her professional performance and touring setup since their creation. This has included high profile appearances at the MUTEK festival in Canada, Mexico and (remotely) in Japan. 

{{< video src="media/BH_mutek_2020.mp4" controls="yes" alt="The video shows a solo harp performance, with varying lighting effects, from dark and slow moving, to bright and strobing red light during the loud passages. Camera close-ups show the harpist manipulating the hardware controllers mounted on the harp." >}}
<p class="video-caption">Alex Tibbitts performing as the Bionic Harpist at MUTEK festival, Montreal, QC.</p>

## III: Reflection and iteration

After three years of heavy use, Tibbitts and I began work on an updated version of the controllers for her continuously developing live show. I interviewed Tibbitts to get an understanding of her experiences and needs, as well as to get her perspective on the evolution of the project and continued work. 

The interviews provided valuable reflection on this type of design research, and gave a clear roadmap for a new controller design and build, which was completed in 2022. The new controllers preserve the same basic footprint and hardware architecture, however nearly every component and feature has been rethought and redesigned, to provide a more functional and robust controller interface that can withstand heavy professional use. 

{{< figure src="media/Bionic_Harp_2_controllers.jpg" caption="Version 2 of the Bionic Harp controllers." alt="Two hardware controllers sit on a desk. They are made out of black #D printed frames and black acrylic panels, and feature layouts of buttons, knobs and sliders." >}}

### Research and creative outcomes

On the creative end, Tibbitts is nearing completion of her first studio album "[The Bionic Harpist: Impressions](https://www.alextibbitts.com/album)".

{{< figure src="media/alex_tibbitts_impressions.jpg" caption="Tibbitts at work in the studio." alt="A photo of a woman in a recording studio sitting next to a harp. She is listening to something back in the headphones. The studio is cozy and full of wood, and there are pianos, amplifiers, and other instruments lining the walls." >}}

On the research end, am preparing a submission for the 2024 [New Interfaces for Musical Expression (NIME) Conference](https://nime2024.org) that provides an update on the research we have done and provides insights on interface design for professional users. This corresponds with a growing body of research I have carried out around design for professionals and other "extreme" users that place great demands on the technologies they use.[^3], [^4] 

[^1]: Sullivan, J., Tibbitts, A., Gatinet, B., & Wanderley, M. M. (2018). "Gestural Control of Augmented Instrumental Performance: A Case Study of the Concert Harp." Proceedings of the International Conference on Movement and Computing, Genoa, Italy. https://doi.org/10.1145/3212721.3212814
[^2]: Tibbitts, A., Sullivan, J., Bogason, Ó., & Gatinet, B. (2018). "A Method for Gestural Control of Augmented Harp Performance (performance)." Proceedings of the International Conference on Live Interfaces. Porto, Portugal.
[^3]: Sullivan, J., Guastavino, C., & Wanderley, M. M. (2021). "Surveying digital musical instrument use in active practice." Journal of New Music Research, 50(5), 469–486. https://doi.org/10.1080/09298215.2022.2029912
[^4]: Sullivan, J., Wanderley, M. M., & Guastavino, C. (2022). "From Fiction to Function: Imagining New Instruments Through Design Workshops". Computer Music Journal, 46(3). https://doi.org/10.1162/comj_a_00644
