---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Making "The Game of Life"'
subtitle: "An in-depth look at the design and research behind the interactive dance work." # shown on post page only
summary: "An in-depth look at the design and research behind the interactive dance work." # shown on aggregate pages 
authors: []
tags: ["interactive media", "research", "design", "dance", "documentation"]
categories: ["dance", "HCI", "arts-led research"]
date: 2023-11-21T15:21:10+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: "Discussion during rehearsal break. Centre national de danse contemporaine. Angers, France."
  focal_point: "Center"
  preview_only: false
  alt_text: "A photograph of a stage in a theater. The walls are black and the floor is white. illuminated by the overhead stage lights. Eight square panels hang at the back of the stage, some light up, some not. The same squares are also projected on the floor of the stage in the same pattern. A marimba sits in the back right corner of the stage. Eight individuals are in a circle in the middle of the stage, in discussion during a break in the rehearsal."

---

"The Game of Life" is a live interactive work performed by three dancers and three musicians. The piece is organized around modules of choreography and music which are sequenced in real time onstage, according to a set of predetermined rules and interrelations between the performers. *Read more about the production [here]({{< relref "/project/interactive/the-game-of-life" >}}).*

{{< toc >}}

## Overview

{{< video src="media/TGoL_rehearsal_excerpt_sm.mp4" controls="yes" alt="A fixed camera captures the stage during dress rehearsal. The dancers are dancing in and out of synchronization with each other, while three musicians - a violinist, flautist, and percussionist playing marimba and other pitched percussion instruments - play at the back of the stage arranged from left to right. All of the performers wear slightly different costumes of the same navy blue color.">}}
<p class="video-caption">📽️ An excerpt from the dress rehearsal</p>

{{% callout note %}}

# tl;dr:

I joined a contemporary dance company to work on a new production that premiered in November 2022. I performed two distinct roles: interaction designer and researcher. 

#### Objectives
- Interaction design: Develop interactive systems that would allow the work to be performed in a  generative, rule-based structure
- Research: Examine how design practice is carried out within artistic productions. 

#### Design highlights:
- Collaborated closely with the creators: two choreographers and a composer.
- Co-designed four interactive modules that utilized biosignal acquisition, multimodal feedback, wireless networking, and interfacing with theater audio and lighting systems.
- Engineered, implemented, and tested all of the technical systems.

#### Research highlights:
- Collected heterogeneous qualitative research data through two residencies where the work was created.
- Interviewed choreographers and performers to capture their insights and experiences about the creation and technical design process. 
- Thematic analysis generated high-level themes around design for creative productions.  
- Presented findings at the 2023 ACM Designing Interactive Systems (DIS) Conference. 

{{% /callout %}}

---

## I. Background 

I joined *[Le principe d'incertitude](https://lpdi.org)*, the French dance company of choreographers Liz Santoro and Pierre Godard, to work on this piece in the fall of 2022 after my arrival in Paris for a 2-year postdoc working on a project entitled "Living Archive: Interactive Documentation of Dance", at the [ex)situ laboratory](https://ex-situ.lri.fr/) of the [Université Paris-Saclay](https://www.universite-paris-saclay.fr/) under the supervision of [Sarah Fdili Alaoui](https://saralaoui.fr). The initial plan was to join the crew to conduct research on the development of a new technology-mediated dance piece. However, an initial discussion revealed that they were also in need of a designer and technician to help them develop interactive systems they envisioned for the piece, so I took up dual roles of researcher and interaction designer. 

My involvement with the creation took place over three main periods leading up to the premiere in November 2022: a two-week preliminary planning stage, and two two-week residencies leading up to the premiere. "The Game of Life" has been performed multiple times around France, with additional dates scheduled for 2024. 

---

## II: Interaction Design

### Stage 1: Pre-planning. 

{{< figure src="media/early_rehearsal.jpg" caption="Early rehearsals before the residencies" alt="Three dancers execute a synchronized move. The are in a line with their backs to the camera and in mid-air, with their right leg slightly kicked out to the side.">}}

I joined Santoro and Godard at the studio and at their home, where they explained how they envisioned the piece being performed, and some early ideas they had for implementing interactive technologies that would permit the sequencing to be performed in a live, generative way. We chose to focus our attention on the following aspects: 

-  Exploring and implementing feedback loops via biosignal sensing, between dancers and musicians, and between audience and performers. 
-  Developing a central control system to encompass and enforce the "rules" of the composition, and integrate with custom theater lighting to provide realtime onstage cuing for the performers. 

{{< figure src="media/hr-to-bpm-diagram.jpg" caption="Diagram of an early prototype of a feedback module between dancers and musicians, that modulates the tempo in an inverse relationship to the dancers' changing heart rates. In the final version, the haptic metronomes in the diagram were replaced with an audiovisual metronome." alt="A digital drawing shows a diagram of the heart rate to tempo feedback loop. On the right, a body outline (indicating a dancer) wears a heart rate acquisition chest strap. Dotted lines show the signal flow going to a) smartphone running a biosignal broadcasting app called Pulsoid, b) to a Pulsoid.net server offsite, c) back to a local wifi access point and router, d) to a computer running the software Max for Live, e) to another smartphone running the Soundbrenner metronome app, to f) the musician wearing a Soundbrenner haptic metronome." >}}

### Stage 2: First residency

I arrived at the residency with some early proof-of-concept designs to try out. Working closely with Godard and composer [Pierre-Yves Macé](https://pierreyvesmace.com/), I iterated and refined system designs, testing them on the spot in daily afternoon rehearsals. By the end of the residency, two main modules were implemented: a basic control hub and lighting interface, and a feedback system that mapped the dancers' heart rates to the evolving musical tempo. An audience-performer feedback system was also developed and tested, but ultimately discarded as it was unreliable and didn't quite fit the aesthetics of the piece. During this time, the dance and music creation was still very much in development, with changes and progress made every day. 

{{< figure src="media/TGoL-new-lighting-ui.jpg" caption="Sketches and GUI of an early version of the control hub and interactive lighting module" alt="A digital note with a sketched GUI with some buttons and labels. Above it on the page is a pasted screenshot of the actual GUI with the same section implemented." >}}

<!-- {{< figure src="media/B1D2_20221019_IMG_1013.JPG" caption="Open Max for Live patch of the heart rate to tempo module. The module were developed with Max for Live as this provided the best integration with the audio portion of the performance." alt="Picture of a laptop screen showing the editing patcher window of teh software Max for Live." >}} -->

### Stage 3: Second residency

During the second residency, the control-lighting and heart rate-tempo modules continued to be refined as the choreography came together into finished form. Two additional modules were also developed. The first, developed in coordination with composer Macé's evolving musical composition, was a communication bridge deliver realtime information and cues to a separate audio computer that performed an ever-growing series of processes, sampling and modulating the sounds of the live instruments and adding additional layers of sound synchronized to with the unfolding onstage performance. The last module, delivered just the day before the premiere, took full control of the entire theater lighting and automated it to the generative sequences of the dance. 

{{< figure src="media/TGOL_interactive_systems7.jpg" caption="A diagram of the four interactive modules implemented into 'The Game of Life'" alt="A digitally drawn diagram of an overhead view of the stage. Icons show the positions of three dancers and three musicians on the stage. Four modules are displayed with their parts and interaction flows drawn out. Module 1, the control hub and performer cuing system (shown in gray) indicates three banks of eight lights positioned around the stage. Module 2, the heart rate to feedback loop (shown in red) shows the heart rates of the dancers sent to a box that modulates the BPM of the music, which is sent to the musicians. Module 3, is labeled audio notifications but is not diagrammed, as it doesn't have a visible presence on the stage. Module 4, automated control of venue lighting (shown in blue), shows theater lights surrounding the stage." >}}

{{< figure src="media/regie_dress_rehearsal.JPG" caption="Testing new software at the control booth during a break in rehearsal" alt="Looking down the long table of the control booth at the back of the theater. A man sit sitting in front of two computers, and a woman sits beyond him working at a sound board. " >}}

{{< figure src="media/B3D3_20221110_IMG_1063.JPG" caption="Rehearsal during the second residency" alt="View of a dance stage in a theater, with three dancers and three musicians mid-performance." >}}

---

## III: Auto-ethnographic research on dance technology design

{{< figure src="media/research_data.png" caption="Heterogeneous research data collected during the residencies. Clockwise from top left: Handwritten technical notes from my notebook on lighting interaction design; the 'régie' (control booth for sound, lights, and stage direction at the back of the audience); excerpt from my entries in my research journal, Godard leading a discussion during a rehearsal break, still from a video excerpt of an early rehearsal, mounting light panels for the interactive lighting (center)." alt="A digital collage of 8 images of different pieces of research data collected during the residencies." >}}

In my role as researcher, I documented the creation process during the residences through notes and sketches, photos and videos, and a research journal. Through these media, I recorded my own daily activities and those of the team, and made observations and reflections on the entire process as well as my own roles. I used these sources to assemble a design story that provides my perspective on the residencies and the design process, one that I characterize through the lens of design occurring *within* an artistic creation. 

I augmented my own perspective by conducting conversational interviews with the choreographers and performers. We conducted a thematic analysis of the transcribed interviews, through which we identified nine themes divided into two categories: a) *"the realities of a constrained and messy process*, and b) *"trusting technology to become an invisible scaffolding for an embodied performance"*. 

{{< figure src="media/tgol_thematic_analysis.jpg" caption="Codes generated from the thematic analysis of interviews" alt="Overhead image of a wooden table. Yellow, pink and green Post-It notes are neatly arranged in nine clusters. Unused pads of Post-Its, pens, folders, and other miscellaneous objects are places around the table." >}}

As mentioned, I presented the research at the *[2023 ACM Designing Interactive Systems conference (DIS)](https://dis.acm.org/2023/)* in Pittsburgh, USA on behalf of my coauthors: Sarah Fdili Alaoui, Liz Santoro, and Pierre Godard. In our [paper]({{< relref "/publication/sullivan-2023a" >}}), we characterize research occuring within artistic creation as messy and situated, defined by overlapping roles and idiosyncratic practices. We consider related work in design research, especially related to artistic practice, and find Steve Benford and colleagues' "Performance-Led Research in the Wild"[^1] to be a particularly useful model for understanding artist-led research. We suggest that our findings can extend this even farther to include the dynamic roles and activities that occur within and between artistic creation and design. Further, we propose that our work can serve as an exemplar for other design contexts that may be resistant to formal methods and require flexible approaches. 

{{< figure src="media/signal-2023-07-13-153607.jpeg" caption="Presenting our paper at DIS '23" alt="Man giving a presentation in a large white auditorium, with a slide projected on a large screen behind him.">}}

---

## IV: Epilogue: Human-Machine Interaction Workshop: Inventing new "Games of Life"

{{< figure src="media/2023-10-09-120828_002.jpeg" caption="Final group performance from the ARRC Human-Machine Workshop: Audience members are invited to participate in a movement exercise where they are organized using different sorting algorithms according to different types of personal and socioeconomic data." alt="Nine people stand on a stage, forming a 3 by 3 grid. Each stands on a white circle on the ground and are bathed in white light from spotlights above. One person is reading from a paper while the others watch." >}}

In Fall 2023, Fdili Alaoui and I organized a week-long [workshop on Human-Machine Interaction](https://cirrus.universite-paris-saclay.fr/s/iX8gktf2XZ23sa8) that is part of the École Normale Supérieure (ENS) Paris-Saclay [Research-Creation Diploma](https://ens-paris-saclay.fr/formations/autres-diplomes/diplome-arrc-recherche-creation) curriculum. We invited Santoro and Godard as guest instructors, and proposed for the students to create and perform their own interactive performance works, based on four principles of interaction that inspired "The Game of Life": 

1.	Generative score (initial conditions, rules, variability from one performance to the other) 
2.	Presence of feedback loops (cybernetic, self-regulating systems)
3.	Interdependency between performers (the state of each performer influences that of the others, as in an ecosystem)
4.	Shared score for dance and music (common rules and codes for both media)

The class assembled into three groups and created three diverse interactive works that were performed for a packed house at the end of the week. 

{{< video src="media/signal-2023-10-09-120828_008.mp4" controls="yes" alt="Two performers are on a stage bathed in soft purple light. The person on the right is sitting and manipulating water in a bucket. A microphone is placed over the bucket. The person on the left is dancing in an abstract way, to the sounds of the water." >}}
<p class="video-caption">📽️ Final group performance from the ARRC Human-Machine Workshop: Recorded sounds of water being manipulated are processed according to the captured movements of a dancer, who in turn is responding to the modulated sounds of the water.</p>

{{< video src="media/signal-2023-10-09-120828_009.mp4" controls="yes" alt="Three performers lay down and stand in a line onstage in front of a blue and white screen that has a sequence like grid on it. They are making coordinated movements together, occasionally falling out of sync, which makes everyone laugh." >}}
<p class="video-caption">📽️ Final group performance from the ARRC Human-Machine Workshop: A sequencer that formulates sentences out of a corpus of words, which instructs the dancers to perform specific movements.</p>

[^1]: Steve Benford, Chris Greenhalgh, Andy Crabtree, Martin Flintham, Brendan Walker, Joe Marshall, Boriana Koleva, Stefan Rennick Egglestone, Gabriella Giannachi, Matt Adams, Nick Tandavanitj, and Ju Row Farr. 2013. "Performance-Led Research in the Wild." *ACM Transactions on Computer-Human Interaction.* 20, 3 (July 2013), 14:1–14:22. https://doi.org/10.1145/2491500.2491502





