---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Sourdough 2: Mabel"
subtitle: ""
summary: ""
authors: []
tags: ["sourdough", "fermentation"]
categories: ["bread"]
date: 2020-10-09T14:31:42-04:00
lastmod: 2020-10-09T14:31:42-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

In search of bread nirvana, lately I've been trying many different techniques, tools and recipes for my go-to whole wheat sourdough. One important change has been the transition from feeding the starter mostly all-purpose flour to rye flour exclusively. Thus, Mable, the new kid in town, has taken over. (Long live Rupert; Rupert is dead!) Though, Mable *is* technically Rupert, just on a different diet. 

Anyways, this is the current recipe I've been working off of, thanks to [The Bread Code](https://github.com/hendricius/the-bread-code). Anyone who has a Git repo dedicated to sourdough is alright in my book. 

## Recipe: 

Adopted from [The Bread Code](https://www.the-bread-code.io/), in particular his YouTube video "[The Last Sourdough Recipe You Ever Need | Amazing Bread Every Time](https://www.youtube.com/watch?v=NMglhwp2lNs)".

Following this schedule, it is basically a 3 day process (though it can be easily reduced to 2 or even 1 day with a little planning and adjustment.)

- Day 1 (evening): Levain and autolyse
- Day 2: fermentation and shaping, then in the fridge overnight to proof
- Day 3: Baking

## The night before (day 1): 

Mix up your levain (starter mixture) and autolyse (the dough minus starter) in separate containers in the evening. 

### Levain

- 10g starter
- 55g flour
- 55g water

### Autolyse

- go for 65% - 75% hydration
- 500g flour
- 375g water

Mix by hand and let sit overnight. Check for windowpane effect. Make sure temp is somewhere around 22°-25°. Can put in oven with oven light on if kitchen is too cold.

Then the next day...

## Strength development (day 2):

Add the following to the autolyse: 

- 20% starter calculated on the flour mass
  - (can do 5% - 10% if doing an overnight bread, though I usually stick to 20%)
  - mix into dough by hand
- 12g salt
  - mix into dough after starter has already been incorporated
- round the dough up by hand
- rest for 15 minutes

Then...

- stretch and fold till dough comes out in one piece
- on counter, stretch and fold until dough holds shape in a ball, 1 - 3 minutes
    - if the dough is still sticky (high hydration), stretch and fold in bowl, and can continue to stretch and fold every so often into the bulk fermentation stage before transferring into baking dish for coil folds? 
- then turn it several times to round it up
- let's sit for a few minutes

(Optional) Take a sample...

- Cut a little piece off and put it in a little glass container
- put a rubber band around at the level of the dough
- then you can monitor how much it rises throughout the bulk fermentation. It should roughly double in size before shaping.

For fermentation.

- round the rest of the dough up again and put it into 9x13 rectangular baking dish. Cover. 

## Bulk fermentation

Check [fermentation table](http://table.the-bread-code.io) for ballpark fermentation time, based on temperature and starter %. 

- do a coil fold every couple hours

Bulk fermentation is complete when dough doubles in size?

- Coil fold to pull it out
- preshape
- let rest a few min
- shape and out in banneton

## proofing

Finger poke test:

- if it springs right back, not ready
- slowly comes back, ready 
- doesn't come back, over proofed

Generally, 1.5 - 3 hours out or overnight. Or 16 (6⁰) - 24 (4⁰) hours in fridge

## Baking (typically day 3, with the loaves proofed in refrigerator overnight)

- Use a dutch oven for best results. 
- turn out on parchment paper
- Score bread with a lame.
- Put in preheated dutch oven (parchment paper and all)
- slip an ice cube or 2 under the parchment paper to generate some extra steam.
- 500°F for about 20 min; uncover and reduce to 445° for 15 to 20 more min. 

When done let cool on wire rack for at least 1 - 2 hours, preferably more. 

Leave out for several hours before putting in bag. 

-----

