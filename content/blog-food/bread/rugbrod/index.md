---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Sourdough 3: Rugbrod"
subtitle: 
summary: "Celebrating international research collaboration with cross-cultural bread-baking: Traditional Danish sourdough rye."
authors: []
tags: ["sourdough", "fermentation", "rye"]
categories: ["bread"]
date: 2020-11-08T16:59:57-05:00
lastmod: 2020-11-08T16:59:57-05:00
featured: false
draft: false

# aliases: 
#   - /tastes/2020/11/08/sourdough-3-rugbrod/

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

One of the fun aspects of working as a [NIME environmental officer]({{< ref "blog/20201031-nime-environment" >}}) is getting to know my two colleagues [Adam](http://www.adampultz.com/) and [Raul](https://www.researchgate.net/profile/Raul_Masu2), and especially finding out that the three of us share a common interest and commitment to sourdough breadmaking, fermentation and related culinary adventures. It took only a short time for us to begin trading our favorite recipes, investigations, and results. 

So this week I tried my hand at [rugbrød](https://en.wikipedia.org/wiki/Rugbr%C3%B8d), a traditional Danish sourdough rye bread, courtesy of Adam. There were a few small alterations: cracked rye and malt were impossible to find on short notices so I used whole rye and molasses, and I don't have a nice squared off wooden bread mold so I used some small loaf pans I had laying around. But no matter, the results are incredibly tasty, and it was a good use of [Mabel]({{< ref "blog-food/bread/mabel" >}}), the all rye sourdough starter I've been keeping around. 

Nothing fancy or particularly unique about the recipe for this one, straight from the interwebs to my kitchen: https://foodgeek.dk/en/danish-rye-bread-recipe/. 

Thanks Adam for sharing! 

Now on to Raul's house made tempeh... 

{{< gallery resize_options="500x" album="rugbrod" >}}

-----

