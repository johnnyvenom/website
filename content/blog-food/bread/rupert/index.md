---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Sourdough 1: Rupert"
subtitle: ""
summary: ""
authors: []
tags: ["sourdough", "fermentation"]
categories: ["bread"]
date: 2020-10-09T13:40:44-04:00
lastmod: 2020-10-09T13:40:44-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Around Christmas 2018, we started up a sourdough starter. His name was Rupert (don't worry, Rupert lives on, just... differently...) I've been making many different sourdough breads ever since. My go-to is a whole wheat sourdough with a mix of white and wheat bread flours, and I've also made a few different rye breads, spelt, seeded breads, [vegan spelt scones](https://minimalistbaker.com/coconut-oil-blueberry-scones-with-rosemary/) (not sourdough but amazingly good and highly recommended), vegan sourdough pancakes, etc.

Here is the first sourdough recipe I used and refined, From winter 2018/19 to spring 2020. 

----

## Go-to whole wheat sourdough:

It takes the basic ingredient list from ilovecooking recipe ([recipe page][1] and [YouTube link][2]) but doubles the salt (a la _vitalivesfree_). The technique is from [_vitalivesfree_ recipe][3], which makes for less kneading, wetter dough, and so far better overall results. 

### **Ingredients for 1 loaf: (x3 loves in parentheses)** 

- 250ml (750ml) lukewarm water
- 160g (480g) sourdough starter (see section below)
- 400g (1200g) bread flour (2:1 ratio white/whole wheat)
- 25ml (50-75ml) water
- 10g (30g) salt

#### Mixing instructions (do this the day before): 

- whisk together water and starter
- add flour and mix quickly by hand
- let sit for 30 min
- add remaining water and salt
- knead for 5 - 10 min
- proofing (a): turn dough every 30 minutes (for 2 hours, so 4x turns total)
- proofing (b): then 1 x per hr for an additional 1 - 3 hours
- shape and put loves in proofing baskets
- refrigerate overnight

#### (the next day)

- turn oven on high (550º F) and make sure stone is fully heated
- take loaf out of fridge, add seeds if desired (see below)
- score top with a knife
- put loaf on hot stone
- add water to steam tray, add a few ice cubes to make lots of steam
- once ice melts, reduce oven temp to 445º
- cook for 30 min +/-, until crust is nice and dark. 
- remove and place on rack to cool

[1]: https://www.ilovecooking.ie/features/sourdough-bread-masterclass-with-patrick-ryan/
[2]: https://www.youtube.com/watch?v=2FVfJTGpXnU
[3]: http://vitalivesfree.com/foolproof-sourdough-bread-recipe/

————

### Notes: 

- Added 50mL of water at first step bc dough was too dry. That seemed to work. (now incorporated into recipe)
- Sesame seeds on top work well. Pumpkin and sunflower seeds tended to burn and/or fall off. 
    - To get seeds to stick, when bread comes out of proofing basket place top down in shallow bowl covered with a wet towel (so the top of the dough gets a little moist) then place in another shallow bowl with seeds so they stick. Or brush with egg wash.

————

### SOURDOUGH STARTER: 

- To make sourdough starter: 
    - Combine 200g unbleached all purpose flour with 200g water. 
    - Combine thoroughly and let sit lightly covered in room temperature for 24 hours. 
    - After 24 hours, discard half and add another 200g flour and 200g water; combine and leave for 24 hours. 
    - Repeat this process until the starter gets bubbly. (see THE INTERNET for a million sites with in depth info on this)
- To maintain your sourdough starter: 
    - Once all bubbly it’s ready to use. So you don’t have to continue feeding it every day, cover and put in the refrigerator, where it will be happy for a week. 
    - 1x per week, take it out and let it warm up to room temperature. Feed it with the following ratio: 1x existing starter : 1x flour : 1x water. 
    - Combine well and let sit for 12 - 24 hours. It will get all bubbly and increase in size. It is now ready to use. 
    - Use as much as you need and put the rest back in the fridge for another week or until you need it again. 
    - If you don’t use it in a week, just discard 1/2, feed it, and put it back in the fridge. 

-----



