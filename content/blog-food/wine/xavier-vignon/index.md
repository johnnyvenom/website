---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Xavier Vignon"
subtitle: ""
summary: ""
authors: []
tags: ["red", "wine", "organic" ]
categories: ["wine"]
date: 2020-11-12T18:07:49-05:00
lastmod: 2020-11-12T18:07:49-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

- **Category:** Red
- **Grape:** Grenache 50%, Mourvèdre 40%, Cinsault5 %, Syrah 5%
- **Country:** France
- **Region:** Côtes du Rhône
- **Price:** $20
- **Rating:** 3 or 4, TBD. Need to try it again. 
- **Organic:** yes

## Thoughts:

First taste: jammy, perfumey, berries, but not overly sweet. Light in color and taste. No real tannins to speak of. Quite enjoyable, if a little underwhelming. 

### SAQ notes: 

Rich of his «flying winemaker» experiences in Oceania, California, Bordeaux and Champagne, Xavier Vignon is today a consultant-oenologist in the Rhône Valley where he also produces wines under his own brand. A blend of Grenache, Syrah and Mourvèdre, the wine is fruit-forward and suggests scents of red and black fruits, candied licorice and a subtle spice note. [[link](https://www.saq.com/en/14214018)]

-----

