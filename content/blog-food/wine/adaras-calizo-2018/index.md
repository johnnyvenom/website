---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Adaras Calizo 2018"
subtitle: ""
# summary: "Sometime we'll remember to take the picture BEFORE we open it..."
authors: []
tags: ["organic", "red", "wine"]
categories: ["wine"]
date: 2020-10-09T13:02:54-04:00
lastmod: 2020-10-09T13:02:54-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

- **Category:** Red
- **Grape:** Garnacha Tintorera
- **Country:** Spain
- **Region:** Almansa-Albacete
- **Price:** $17.65
- **Organic:** yes
- **Rating:** ⭐️ ⭐️ ⭐️

## Thoughts:

Very nice wine, don't remember too much about it but quite enjoyable. 

Sometime we'll remember to take the picture BEFORE we open it...

-----

