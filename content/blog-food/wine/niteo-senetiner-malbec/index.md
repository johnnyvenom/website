---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Nieto Senetiner Malbec Reserva Mendoza"
subtitle: ""
# summary: "Good, very fruity but dry."
authors: []
tags: ["red", "wine"]
categories: ["wine"]
date: 2020-10-03T18:55:59-04:00
lastmod: 2020-10-03T18:55:59-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

- **Color:** Red
- **Grape:** Malbec
- **Country:** Argentina
- **Region:** Mendoza
- **Price:** $14
- **Rating:** ⭐️ ⭐️ ⭐️

## Thoughts:

Good, very fruity but dry. Easy. Would buy it again. 

-----

