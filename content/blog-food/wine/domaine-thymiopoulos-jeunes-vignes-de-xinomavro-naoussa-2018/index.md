---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Domaine Thymiopoulos Jeunes Vignes de Xinomavro Naoussa 2018"
subtitle: ""
# summary: "Great up front aroma and flavor, light and clean on the tongue, with  a crisp finish."
authors: []
tags: ["red", "wine"]
categories: ["wine"]
date: 2020-11-01T13:14:28-05:00
lastmod: 2020-11-01T13:14:28-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

- **Category:** Red
- **Grape:** Xinomavro 100%
- **Country:** Greece
- **Region:** Macedoine
- **Price:** $18.60
- **Rating:** ⭐️ ⭐️ ⭐️ ⭐️

## Thoughts:

Exceptional wine unbeknownst to us before yesterday. Great up front aroma and flavor, light and clean on the tongue, with  a crisp finish. 

-----

