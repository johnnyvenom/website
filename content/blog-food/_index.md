---
title: Tastes

# View.
#   1 = List
#   2 = Compact
#   3 = Card
view: 3

layout: post

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""

cascade: 
  commentable: true
  show_related: true
  
---

When I'm not thinking about music technology and cycling, I spend a fair amount of time obsessing about different flavours: exploring the world of sustainable specialty coffee, baking sourdough breads, making hot sauce with peppers from our balcony garden, and keeping track of wines we like.  

----
