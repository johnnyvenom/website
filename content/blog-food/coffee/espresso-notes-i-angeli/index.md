---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Espresso Notes I: Angeli"
subtitle: ""
summary: ""
authors: []
tags: ["espresso", "coffee"]
categories: ["coffee"]
date: 2020-10-09T11:40:59-04:00
lastmod: 2020-10-09T11:40:59-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "Smart"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

My partner and I go through a lot of coffee, even more so now that we both work at home. With every new bag of beans there is an adjustment process to dial in the perfect espresso shot. It's far from an exact science, but seeing as we buy frequently from the same roasters I started keeping track of the metrics so I don't have to blindly guess with each new bean. 

Perhaps some day I'll get motivated and start doing individual reviews etc., but for now I'll just post the sheet and keep it updated. 

## Equipment

- **Machine:** Angeli (single boiler, not much info I could find on it but essentially the exact same machine as the Isomac Brio)
- **Grinder:** Baratza Sette 270Wi

## Shot metrics

| Roaster | Name | Origin | Alt. (m) | Process | Grind Setting | In (g) | Out (g) | Time |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| De Mello | Edmond House Blend | 70% Brazil 30% Ethiopia | | washed | 6G | 18 | 36 | 36 |
| Dispatch | Buhorwa | Burundi | | washed | 5F | 18 | 36 | 30 |
| Dispatch | Iyenga | Tanzania | 1,675 - 1,900 | washed | 5H | 18 | 35 | 32 | 
| Dispatch | La Claudinota | Colombia | ? | 7D | 16 | | |
| La Vieille Europe | ??? | Colombia | | washed | 6E? | 17 | 32 | 28 | 
| La Vieille Europe | Colombian | Colombia | | washed | 7B | 16.5 | 32 | 28 | 
| Lenoir & Lacroix | Kaffa | blend | | washed | 5E? | 18 | 35? | 28? | 
| Monogram | Moonlight | Brazil | | Semi-washed, natural | 9E | 18 | 36 | 27 |
| Social | People's Daily | blend | | washed | 7A | 17 | 36 | 28 |
| Structure | Brunch Blend | Brazil/ Colombia | 1650/1000 | Washed, Pulped Natural | 9? | 16.5 | 33 | 30 |
| Structure | Ethiopian | Ethiopia | ? | Natural? | 7D | 17 | ? | ? |
| Traffic | Marcelo Assis | ? | Brazil | Natural | 9D | 16.5 | 37 | 32 |
| Traffic | Sauvage | Ethiopia/ Brazil | 1200 - 2300 | Natural & Yellow Honey | 9C | 18 | 37 | 30 | 
| Zab | Pepe Arguello | Mexico | 1700 | washed | 6H | 18 | 38 | 24 |
| Zab | Tuma Tesso | Ethiopia | 1980 - 2000 | washed | 6G | 18 | 36 | 28 |

-----

