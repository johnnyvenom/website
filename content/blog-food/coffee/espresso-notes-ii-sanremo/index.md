---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Espresso Notes II: Sanremo Treviso"
subtitle: ""
summary: ""
authors: []
tags: ["espresso", "coffee"]
categories: ["coffee"]
date: 2021-02-15T10:28:39-05:00
lastmod: 2021-02-15T10:28:39-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

In a previous [post]({{< relref "/blog-food/coffee/espresso-notes-i-angeli" >}}) I kept a running list of different coffees we go through and metrics for pulling shots. Now my venerable Angeli espresso machine has been [retired]({{< relref "/blog-food/coffee/saying-goodbye">}}) and replaced with a beautiful Sanremo Treviso. In due time I'll get to a post on that magical machine. But for now, the measurements for the old machine are obsolete, so here is a new chart for the new machine. 

## Equipment

- **Machine:** Sanremo Treviso, circa 2011. (HX)
- **Grinder:** Baratza Sette 270Wi

## Shot metrics

| Roaster | Name | Country | Grind Setting | In (g) | Out (g) | Time |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Balance | Peaberry | Tanzania | 3B | 18 | 40 | 27 |
| De Mello | Edmond | Brazil, Ethiopia |  |  |  |  |
| Dispatch | Jumarp | Peru | 5E | 18 | 38 | 25 |
| Dispatch | La Coipa | Peru | 4H | 18 | 40 | 25 |
| Eleven Speed | Yirgacheffe Koke | Ethiopia | 4D | 18 | 36 | 29 |
| Eleven Speed | Chebich Co-op AA | Kenya | 4D | 18 | 36 | 29 |
| Hale | São Carlos | Brazil | 4C | 18 | 40 | 26 | 
| Lenoir & Lacroix | Kaffa | blend | 4C | 18 | 38 | 27 | 
| Lenoir & Lacroix | Arthur | Nicaragua, Peru |  | 18 |  |  | 
| Nektar | Toarco | Indonesia | 4H | 18 | 36 | 25 | 
| Social | People's Daily | Brazil/Colombia | 5G | 18 | 36 | 27 |
| Transcend | Transcend Espresso | Guatemala | | 18 | | |
| Transcend | Argyll Blend | Peru | 3A | 18 | 35 | 28 |
| Monogram | Banko Gotiti | Éthiopie | 6F | 18 | 34 | 24 |
| Monogram | Los Santos | Guatemala | | 18 | | |
| Monogram | Luz Dary Pol | Colombia | 4E | 18 | 34 | 31 |
| Zab | Banko Gotiti | Ethiopia | 4H | 18 | 35 | 30 |
| Zab | Banko Michicha | Ethiopia | 4i | 18 | 35 | 27 |

----

([Who cares, just show me some pictures...](#gallery))

----

## The beans

### Balance 

- **[Peaberry](https://www.balancecoffeeroaster.com/shop/colombia-supremo-gh8bw-5mct4)**
  - Country: Tanzania
  - Region: Arusha
  - Varietal(s): Bourbon, Typica
  - Altitude: 1400 - 1800m
  - Process: Washed
  - Notes: "Smooth body with fruit toned aromas. Grapefruit, plum, black tea."

### [De Mello Palheta](https://hellodemello.com/)
- Edmond
  - Country: 70% Brazil, 30% Ethiopia 
  - Region: Serras de Minas (BR), Dari (ET)
  - Varietal(s): 
  - Altitude: 
  - Process: Washed
  - Notes: Peanut butter, candied apple, creamy 

### Dispatch

- **[Jumarp](https://dispatchcoffee.ca/collections/coffee/products/jumarp)**
  - Country: Peru
  - Region: Amazonia
  - Varietal(s): 
  - Altitude: 
  - Process: Washed
  - Notes: 
- **[La Coipa](https://dispatchcoffee.ca/collections/coffee/products/lacoipa)**
  - Country: Peru
  - Region: Cajamarca
  - Varietal(s): Typica, Caturra, Pache
  - Altitude: 1900m
  - Process: Washed
  - Notes: "Golden raisin, cantaloupe, caramel"

### Eleven Speed Coffee Roasters

- **[Yirgacheffe Koke](https://www.elevenspeedcoffee.ca/collections/coffee/products/ethiopia-yirgacheffe-koke-washed)**
  - Country: Ethiopia
  - Region: Yirgacheffe
  - Varietal(s): Kurume, Dega, Wolisho
  - Altitude: 1800m
  - Process: Washed
  - Notes: "Blueberry, peach, jasmine, silky mouth, stunningly clean"
- **[Chebich Co-op AA](https://www.elevenspeedcoffee.ca/collections/coffee/products/kenya-chebich-co-op-aa)**
  - Country: Kenya
  - Region: Cheptais, Bungoma, Mt. Elgon
  - Varietal(s): SL-28, Sl-34, Ruiru 11, Batian K7
  - Altitude: 2000m
  - Process: Washed
  - Notes: Grapefruit, peach, wine

### Hale Coffee
- **[São Carlos](https://www.halecoffee.com/collections/coffee/products/hale-coffee-single-origin-brazilian-coffee-beans-sao-carlos)**
  - Country: Brazil 
  - Region: São Carlos
  - Varietal(s): Bourbon
  - Altitude: 1120 - 1250m
  - Process: yellow honey
  - Notes: chocolate, toasted almonds, currant

### Lenoir & Lacroixga

- **Kaffa**
  - The bean, the myth, the legend.
- **Arthur**
  - Country: Nicaragua/Peru
  - Process: Washed
  - Notes: Organic
  
### Monogram

- **Banko Gotiti**
  - Country: Ethiopia
  - Region: Gedeb District
  - Variety: Heirloom
  - Process: Washed
- **Los Santos**
  - Country: Guatemala
  - Process: Washed
- **Luz Dary Pol**
  - Country: Colombia
  - Process: Washed

### Nektar
- **Indonésie Sulawesi Toarco**
  - Country: Indonesia
  - Region: Toarco
  - Varietal(s): S795
  - Altitude: 900 - 1600m
  - Process: washed
  - Notes: "amber honey, fresh tobacco, creamy"
  <!-- - [link]() -->

### Social Coffee & Tea Co.
- **[People's Daily](https://www.socialcoffee.com/product-category/coffee/signature-blends-coffee/)**
  - Country: Brazil, Colombia
  - Varietal(s): Bourbon, Caturra
  - Altitude: 1150/1800m
  - Process: Natural/washed
  - Notes: "Glorious coffee for the people"

### Transcend

- **Transcend Espresso**
  - Country: Guatemala
  - Region: Hunapu
  - Variety: Bourbon and Caturra
  - Process: Washed
- [Argyll Blend]([link](https://transcendcoffee.ca/collections/coffee/products/argyll-blend))
  - Country: Peru
  - Region: La Coipa 75% Claro de Luna; 25% Roberto Figueroa
  - Varietal(s): Bourbon, Caturra, Catuai, Pache, Pacas
  - Altitude: 1750-2000m
  - Process: Fully washed, fermented, dried on raised and covered beds and patios
  - Notes: Milk Chocolate | Toffee | Orange
    - Starting Parameters: 20g in | 30 seconds | 40g out

### Zab

- **Banko Gotiti**
  - Country: Éthiopie
  - Altitude: 2000-2100m
  - Process: Natural
- **[Banko Michicha](https://zabcafe.com/collections/cafe/products/ethiopie-banko)**
  - Country: Ethiopia
  - Region: Oromia, Guji ouest, Kercha
  - Varietal(s): Heirloom
  - Altitude: 1900 - 2030m
  - Process: Natural
  - Notes: Recommended extraction in:18/out:34/time:25 

<!-- %% TEMPLATE %%
- [name](link)
  - Country: 
  - Region: 
  - Varietal(s): 
  - Altitude: 
  - Process: 
  - Notes: 

-->

----

## Gallery

Here are some of the coffees...

{{< gallery resize_options="500x" album="coffees">}}

-----

