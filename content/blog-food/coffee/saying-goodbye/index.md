---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Saying goodbye to Angeli"
subtitle: "There's a new kid in the kitchen."
summary: "There's a new kid in the kitchen."
authors: []
tags: ["espresso", "coffee"]
categories: ["coffee"]
date: 2020-11-13T00:59:18-05:00
lastmod: 2020-11-13T00:59:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Big news. BIIIIG NEWS on the way. But first, decorum. And respect. 

The year was 2015 and things were on the up and up. I was a year into Canadian life, having relocated in 2014 to spend a year as a Graduate Research Trainee at McGill University while I researched and wrote my master's thesis. That chapter completed, I was rolling into year 1 of my PhD and life was good. I lived upstairs from the now defunct Café Plume (I'll be real, that was half the reason I moved into that goddamn apartment) and coffee life was good. But there comes a time in a coffee drinker's life, there comes a time... 

Or there doesn't, I suppose. 

But for me there came a time, and the time was then, to bring an espresso machine home. So I searched one out, knowing next to nothing about what I was getting myself into. But it was nice, all chrome, had an Italian name and a grinder built in. What's not to love? 

{{< figure src="angeli.jpg" title="So long old friend, you've done us proud." >}}

And seriously, I do love that machine. 5 plus years later, multiple coffees a day, and it's still going strong. (The grinder kicked the bucket a while back, though I could/should/probably will fix it, just never got around to it and upgraded...)

But, for every time there comes a time, there comes another time, and that time is now. So, I'm pouring a couple drops of espresso out today, and saying goodbye to my old friend. You've served me well and I'll be damned if I won't miss you just a bit. 

*While we're here ahh, anyone in the market for an old but beloved espresso machine? [Hit me up](/#contact).*

{{< gallery resize_options="500x" album="angeli" >}}

-----

