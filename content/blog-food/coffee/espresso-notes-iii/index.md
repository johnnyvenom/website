---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Espresso Notes - III"
subtitle: ""
summary: "Talking about the Baratza Sette 270Wi grinder, and an updated grind chart for the home setup."
authors: []
tags: ["coffee"]
categories: ["coffee"]
date: 2021-11-09T13:28:52-05:00
lastmod: 2021-11-09T13:28:52-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Well, we've been at this a bit already, so here's a new chapter and a bit of a review about the [Baratza Sette 270Wi](https://baratza.com/grinder/sette-270wi/) grinder.

> "The Sette 270Wi is the first-ever home grinder offering grind-by-weight functionality directly into your portafilter or grounds bin."

So, that's the gimmick -- the grinder has a built-in scale that perfectly weighs out your coffee every time. We've been using it for a year now and I have to say wieghing the beans is excellent. It removes a little bit of the manual labour with every shot. The conical burrs are fine, not great, but I'm pretty happy. It's loud and a little jarring early in the morning. On the other hand it grinds 18g of beans in about 3 seconds which is very nice. 

### The stepless grinder

There's really just one point that I'm not extremely happy about. The Sette 207Wi boasts of being "stepless", and it is. It has macro - stepped - settings from 1 to 31. And then it has micro *stepless* settings from A to I, so for each macro setting you can dial in the perfect micro adjustment. All good so far, but in practice I've found that:

1. The macro steps aren't very consistent (for example, if you adjust from 7 to 14, do a couple grinds, and back to 7, the grind won't be the same as where you started from). Same with the micro settings as well. 
2. Over time the micro settings can slide to the coarsest setting. So if you start, say at E (around the middle), by the end of 300g coffee it might have slid all the way down to F.

Anyways, for the price (around $750 CAD), we've found it to be a pretty good little machine. And I'm not really feeling the pull to drop a couple Gs to level up. 

### The shim

One other anomaly of the Sette 270  is that it comes with an optional shim. I ignored it when we got the machine, but several months in I noticed that it was harder and harder to achieve a really fine grind - I simply got to the end of the finest settings for certain coffees. 

Enter the shim. 

With some minor effort the grinder assembly comes apart and the shim can be installed (there are actually 2 shims provided, which look like a nearly paper thin large metal washer) that raise one side of the burrs up ever so slightly closer to the other burrs. Once finished, what was previously the '1' (finest) macro setting is now around '10' and there is ample headroom to dial the grind in as fine as one would like. 

With that, the previous table of shot metrics is no longer accurate, and I have, for my own records (and for anyone who finds themselves in a similar situation) started a new chart based on the new shimmed Sette 270Wi setup.  

## Shot metrics

So here is another coffee chart, based on the updated settings: 


|     Roaster      |     Name     |  Country  | Grind Setting | In (g) | Out (g) | Time |
|:----------------:|:------------:|:---------:|:-------------:|:------:|:-------:|:----:|
| Lenoir & Lacroix |    Kaffa     |   blend   |      10C      |   18   |   32    |  28  |
|     Dispatch     | Santa Marina | Guatemala |      10C      |   18   |   TBD   | TBD  |
|     Dispatch     | Buhorwa | Burundi |      10C      |   18   |   TBD   | TBD  |

-----

