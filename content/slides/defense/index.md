---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Defense"
summary: ""
authors: []
tags: []
categories: []
date: 2021-03-01T21:23:45-05:00
draft: true
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: white
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: dracula
---

# Built to Perform: 

### Designing Digital Musical Instruments for Professional Use

## Ph.D. Thesis

<hr>

John D. Sullivan

---

## Slide 2

...

