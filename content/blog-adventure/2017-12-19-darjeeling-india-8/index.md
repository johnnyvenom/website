---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Darjeeling"
subtitle: "India #8"
summary: "#8 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-19"
lastmod: "2017-12-19"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: 7. Toy Train]({{< relref "../2017-12-16-toy-train-siliguri-darjeeling-india-7" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 9. Kochi ➡️]( {{< relref "../2017-12-21-kerala-kochi-india-9" >}})**

> **Post #8 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

Darjeeling. Hill station. Gateway to the Himalayas. Still now, 5 months later, hard to describe the essence of this place. Pretty magical - with Kanchenjunga, the 3rd highest mountain in the world, standing tall just 100km away (see multiple pictures below). Cold mostly, but highlights included delicious steaming bowls of thukpa (Nepalese noodle soup), high altitude running, sunrise on Tiger Hill, monastery visits, a trip to the well hidden botanical gardens, and proper afternoon tea at the Windamere Hotel on top of the hill. And everywhere, clean crisp air and breathtaking vistas.

---

{{< youtube 0OBdf6SI_3k >}}
<br>
{{< youtube RHPjk3gqRao >}}

---

{{< gallery resize_options="500x" album="india_8_darjeeling" >}}

--- 



