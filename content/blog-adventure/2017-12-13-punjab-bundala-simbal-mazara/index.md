---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Punjab - Bundala and Simbal Mazara"
subtitle: "India #4"
summary: "#4 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-13"
lastmod: "2017-12-13"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: 3. Amritsar]({{< relref "../2017-12-09-amritsar-golden-temple" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 5. Chandigarh ➡️]( {{< relref "../2017-12-14-chandigarh-rock-garden" >}})**

> **Post #4 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

The next few days are spent in the small farming village of Bundala, home of Mandeep's grandfather. Mandeep's father has come from Canada to Bundala for the month to stay with him and spend time with relatives. We stay with family who have a large house on the edge of the village and enjoy a week of the most amazing home-cooked food, walks, visits with family and friends, and local excursions.

{{< gallery resize_options="500x" album="india_4_punjab_bundala" >}}

### Simbal Mazara

Towards the end of the week it's time to continue on. We travel with Mandeep's father to Simbal Mazara, hometown of Mandeep's mother, to have lunch with her aunts, uncles and cousins there. After lunch we continue on to Chandigarh, where we will fly the next day to New Delhi.

{{< gallery resize_options="500x" album="india_4_punjab_simbal_mazara" >}}

### Videos: 

<div class="yt-container">
<iframe class="yt-playlist" width="720" height="405" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22GPFMOjyjn4l6kWtl6M6Hot" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

- Video playlist:
    1. "Mandeep meets a baby cow"
    2. "Crossroads"
    3. "Puppet show at Haveli"
    4. "Simbal Mazara rooftops"
    5. "Leaving Simbal Mazara"

-----

