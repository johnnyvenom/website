---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "24-in-24: 🚵‍♀️ Cycling the Pyrénées"
subtitle: "We are BACK in the mountains for another training camp!" # shown on post page only
summary: "Another of 24(ish) adventures while my partner and I live in Paris for 24 months. We return to famed Tour de France cols for a two week training camp with our friend Felix. " # shown on aggregate pages 
authors: []
tags: [24in24, France, Europe, cycling, Pyrénées]
categories: [travel, cycling]
date: 2024-07-14T10:43:21+02:00
featured: false
draft: false
toc: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

# gallery captions
# run: python captions.py <gallery_folder> <output_folder>
# to generate text file for gallery image captions
---

<div class="btn-row">
  {{< cta cta_text="👈 Previous<br/>🏔️ Chamonix-Mont Blanc" cta_link="adventures/2024/03/30/24-in-24-back-to-chamonix/" cta_new_tab="false" >}}
  {{< cta cta_text="See the whole series!<br/>🇪🇺 24 in 24" cta_link="/tag/24-in-24" cta_new_tab="false" >}}
  {{< cta cta_text="Next 👉<br/>coming soon..." cta_link="#" cta_link="/" cta_new_tab="false" >}}
</div>
<!-- content goes here -->


So last August, my wife Mandeep and I celebrated one year living and Paris. We wanted to August like the locals August, which means getting out of the city and into the countryside. We ended up [on our bikes in the Pyrénées mountains](https://pixelfed.social/c/503590910940140090) (which follow the border of France and Spain) for a solid 2 1/2 weeks. Fast forward one year, and here we are again. After debating a number of other possible destinations, and factoring the untimely demise of my passport (in my backpack along with lapgtop, iPad, and countless other precious things, but that's a story for another day...) we soundly agreed that the Pyrénées mountains were hard to beat, and so we planned our return. Our friend and [fellow cyclist Felix]({{< relref "../20220829-24in24-zurich-cannobio" >}}) had joined us for a week last year, and we would join us for a full two weeks in the commune of Argelés-Gazost, a bullseye village perfectly placed in the middle of countless alpine climbs straight out of the Tour de France roadbook. 🎯

{{% callout note %}}
<p style="text-align:center; font-weight:700; text-transform: uppercase">Skip to day: <a href="#D1">1</a> | <a href="#D12">2</a> | <a href="#D3">3</a> | <a href="#D4">4</a> | <a href="#D5">5</a> | <a href="#D6">6</a> | <a href="#D7">7</a> | <a href="#D8">8</a> | <a href="#D9">9</a> | <a href="#D10">10</a> | <a href="#D11">11</a> | <a href="#D12">12</a> | <a href="#D13">13</a> | <a href="#D14">14</a> | <a href="#D15">15</a></p>
{{% /callout %}}

---

<h3 class="flex-container" id="D1">
  <span class="left">🚄 Day 1: Heading for the hills.</span>
  <span class="right"><a href="https://www.strava.com/activities/11920787398" alt="Strava" target="_blank">27km / 313m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

We are so back! Early train from Paris to Lourde for Mandeep and I, passing by the city of Dax in the southeast. A few hours later and we are installed at our AirBnB in Argéles-Gazost, nestled between the grand cols of the Pyrénées. Our training companion Felix arrived in the afternoon from Zurich and once our bikes were assembled it was time for a warmup ride. We started with a 27km loop around the valley to loosen up the legs and make sure our bikes were ready to go. It's so incredible to be back here.

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j1-arrival" >}}

---

<h3 class="flex-container" id="D2">
  <span class="left">🐄 Day 2: Col du Soulor & Col d'Aubisque</span>
  <span class="right"><a href="https://www.strava.com/activities/11926865162" alt="Strava" target="_blank">67km / 1544m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Time to hit the mountains! We start the training camp with one of our favourite rides, hitting these two mainstays of the Tour de France. The pictures here don't do it justice but the route between Soulor and Aubisque is pretty much the most beautiful stretch of riding that exists in the world. Prove me wrong. Perfect weather all day and to top it off, we had no idea that the entire route was closed to vehicle traffic, as a post-Tour cycling event. What a way to start this trip. We. Are. So. Back!!

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j2-soulor-aubisque" >}}

---

<h3 class="flex-container" id="D3">
  <span class="left">📈 Day 3: Hautacam.</span>
  <span class="right"><a href="https://www.strava.com/activities/11934862773" alt="Strava" target="_blank">46km / 1347m / fucking steep! {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

*"Hautacam is a motherfucker."* - Mandeep. I think Hautacam is our spirit animal or something. It's the closest climb to the door of our AirBnB and just takes you straight from the valley up into the sky. It's been home to a few TdF summit finishes, and was the first mountain we tackled last year. But its real claim to fame are the gradients, as it frequently pitches up for stretches of 10, 11, 12%. Hard to find a climbing groove and on our unseasoned legs this was a grind. But once at the top, the views were top notch and the feeling was pure exhilaration. Finally, in the evening we decided to reward ourselves with dinner in town (Argéles-Gazost). Sub-par pizza for a way over par day of riding.

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j3-hautacam" >}}

---

<h3 class="flex-container" id="D4">
  <span class="left">🪶 Day 4: Col des Spandelles</span>
  <span class="right"><a href="https://www.strava.com/activities/11943808187" alt="Strava" target="_blank">78km / 1413m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

This is our second year cycling in this region, so it feels like we are just going about business, systematically knocking out the world-class climbs one by one. Col des Spandelles is on the menu today, lesser known and quieter than the rest (but still regularly featured in the Tour). This was our farewell ride last year, done in the drizzle and fog. This year it was still cloudy but everything was green and soft. There's something about this one. Almost no cars and few cyclists. Plenty of cows and birds. There's little at the top. A dirt parking area for a handful of cars and some hiking trails disappearing up the further slopes in all directions. It's a vibe. This time we continued down the descent on the other side which was a jawdropping gorgeous ride snaking down S-curves into the adjacent valley below. I watched an enormous bird of prey (no idea, an eagle? hawk? falcon?) glide silently a few meters above Mandeep's head. Ominous? I don't think so - it was just the angle of view, but it made me so very grateful to be so far out in nature. If it doesn't show already, I am so in love with this little slice of the world. Our ride continued with a loop through the Pyrénées Atlantique region (a subject for a second post), and soon we were on the bike path approach from Lourdes back to Argéles-Gazost.

We reunited with Felix, who had headed off his own way to climb Col de Soulor from the other direction, and made up an incredible vegetarian dinner of chana masala, baingan bharta, raita, rice and salad, using market-sourced farm-fresh local produce.

#### 👴👵 On retirement.

{{< figure src="20240721_133602_w.webp" caption="La maison de retraite de Bétharram" >}}

But wait there's more. We descended into the adjacent valley to where we are staying in Argéles-Gazost, then road a rolling loop out of the mountains back home. The ride took us from the Haute-Pyrénées region to the Pyrénées-Atlantiques, and it felt like the flavour changed a little. The roads were largely deserted, and Mandeep and I rolled along soaking it all in for the first time. Then we stumbled across the commune of Lestelle-Bétharram. I don't have many words for this little place except this.

{{< figure src="20240610_102253_w.webp" caption="My dad, Scarborough Beach, Maine. June 2024." >}}

For those that know me, you'll be familiar with the ongoing decline of my dad's health due to Lewy Bodies Disease and his steady progression into dementia. This has necessitated moving him 4 times in the last couple of years into homes with increasing levels of medical care and supervision. He was on my mind as we enjoyed our ride down new-found roads though field and forest. But then we finally came to this little village and pulled into a lot facing a beautiful and peaceful-looking stone building. We wondered what exactly it was, and a little looking revealed it to be a retirement home named [la Maison de retraíte de Bétharram](http://www.lestelle-betharram.fr/betharram-histoire/la-vie-a-betharram/la-maison-de-retraite). The location and the peace surrounding it was exquisite. We should all be so lucky to serve out our days at such an idyllic location.

Filled with these thoughts, we continued on just a few meters to the heart of Lestelle-Bétharram and stood on a stone bridge. While it may not ultimately come to pass, I declared that it is a place that I would like to retire to some day, and live out the rest of my days, of course with Mandeep by my side. ❤️

Somehow, this ride was meaningful, from a deep inner place. Healing for the mind and soul.

Little discoveries like these are one of many reasons why I love to ride a bike. 

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j4-pt1-spandelles-betharram" >}}

#### Lestelle-Bétharram
{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j4-pt2-lestelle-betharram" >}}

#### Just some great fucking #cows.
{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j4-pt3-cows" >}}

---

<h3 class="flex-container" id="D5">
  <span class="left">🍻 Day 5: Recovery day!</span>
  <span class="right"><a href="https://www.strava.com/activities/11951793784" alt="Strava" target="_blank">26km / 367m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Can't believe we need a little break already 🥵 Took an easy spin around the valley, then we made our way to La brasserie du pays des gaves to soak up the rest day vibes. The brasserie feels like a little slice of my old home town of Portland, Maine - warehouse brewery in the back, with a big makeshift terrasse under a tent outside. Excellent craft beers here.

Of note, everywhere we went, we saw the town signs turned upside down. This is a demonstration of protest by farmers, organized by les Jeunes Agriculteurs (Young Farmers) union, to voice opposition against government and EU policies impacting their livelihood. "Nous marchons sur la tête." (We are walking on our heads.) https://www.jeunes-agriculteurs.fr/

More mountains tomorrow.

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j5-rest-day-1" >}}

---

<h3 class="flex-container" id="D6">
  <span class="left">🥕 Day 6: Market day! -> Luz Ardiden</span>
  <span class="right"><a href="https://www.strava.com/activities/11960519463" alt="Strava" target="_blank">67km / 1388m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

It's market day in Argélés-Gazost, and we have been waiting for it since our arrival. So before we get into the mountains, we spend the day stocking up on farm fresh vegetables, cheese (local tomme: vache, brebis, chevre, and mixed... yes all of them), olives, and anything else that looks good. It's an amazing haul. 

After our morning market raid it's time to get riding. We head for a new climb for us, Luz Ardiden. It is a ski area not far from Col du Tourmalet, that looks like it has some impressive switchbacks up the ~1000m climb. We never got to it last year, so off we go to check it out. The climb delivers, with incredible views over Luz San Saveur towards Tourmalet and Pic du Midi and a great windy road with almost no traffic. At the top, the road delivers us to an enormous basin with peaks rising in almost every direction, sheep everywhere, and the road snaking back down below us. 10/10 day and climb.

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j6-luz-ardiden" >}}

---

<h3 class="flex-container" id="D7">
  <span class="left">🦁 Day 7: Col du Tourmalet (west side)</span>
  <span class="right"><a href="https://www.strava.com/activities/11969640025" alt="Strava" target="_blank">105km / 2084m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Time to step it up. It's Col du Tourmalet day. Tourmalet is the most famous col in this area, and probably the most famous cycling destination in the Pyrénées. A mainstay of the Tour de France mens and womens edition, it's a big, open, steady ~8% climb up 1500m, plus a few hundred more to get to the base. It's a beast, but what it lacks in subtlety it makes up in grandeur.

It's a great scene at the top. Cyclists from all over the world arrive there to catch their breath, have a drink and snack at the little resto, and take pictures. We follow suit, then continue on. There is one road that crosses the col, so today we came from the west. It's a straight shot up the valley from Argélés-Gazost where we are staying, and today after the summit we continue down the other side for a nice big loop ride.

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j7-tourmalet" >}}

---

<h3 class="flex-container" id="D8">
  <span class="left">Day 8: 🧘 Spa day!</span>
  <span class="right"><a href="https://www.strava.com/activities/11976398090" alt="Strava" target="_blank">Run 8.28 km {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Needed to switch gears a little after yesterday's Tourmalet raid, so I headed out for a run on the trails in the valley. Definitely a different flavour than cycling up the sides of mountains, but super fun and gorgeous out there.

Mandeep and I closed out the quasi-rest day with a visit to [Le Jardin des Bains](http://www.lejardindesbains.com/), the remarkable public bath and spa complex in the centre of town. This place is a gem.  

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j8-rest-day-2" >}}

#### Thermes d’Argeles-Gazost
{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j8-rest-day-2-spa" >}}

---

<h3 class="flex-container" id="D9">
  <span class="left">🇪🇸 Day 9: Cirque de Gavernie and Col de Tentes.</span>
  <span class="right"><a href="https://www.strava.com/activities/11986326272" alt="Strava" target="_blank">101km / 2348m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Ok, back to the Pyrénées adventure (sidetracked by Olympics taking place all around me in Paris...). With our legs and lungs properly rested, it was time to hit one of the big ones. Col de Tentes is bigger than Tourmalet (approx. 1700m of continuous climbing, basically from our door) but had never been used in the Tour due to its location in a national park. The route runs up to the Cirque de Gavernie, an incredible semicircle of massive peaks, and ends at Col de Tentes, marked by a modest parking area and several hiking trails that span the last few meters to the border of Spain and beyond. We did this route last year and agreed it was our favourite, and same this year. It is the rare mix of being up close to awestrikingly massive mountains, and getting there on a beautiful, winding, nearly deserted road.

---

#### 🚨 **WIFE APPRECIATION POST** 🚨

{{< figure src="20240726_143607_w.webp" caption="" >}}

One other thing: the climb up to Gavernie and Col des Tentes is insanely large and demanding. It's the largest climb I've ever done, easily besting Tourmalet and even some of the insane cycling I did in Colombia a few years ago. (Note to self: still need to put together a travelogue of that trip!)

Col des Tentes is not the most popular Pyrénées climb but still frequented by cyclists in the know. At the top there is an ever rotating cast of cyclists arriving, taking a break, taking photos, and resting before turning around and heading back down.

{{< figure src="20240726_143553_w.webp" caption="A buncha dudes getting ready to head back down" >}}

It is notable and mega-impressive that this day the only woman I saw on the climb or at the top was my superhero wife Mandeep!! She is such a champion. She has been a lifelong commuter cyclist, and got her first road bike a season or who after I got mine - as the joke went, she decided to buy the bike to be able to hang out with her boyfriend. Since that time she has turned into a bonafide baddass cyclist, and together we have spent hours, days, and weeks on our bikes together. Doing world-class hors categorie climbs has become a bit of a normal occurrence for her, and more than a few times I find myself trying to hold her wheel when the road goes up.

{{< figure src="20240726_112630_w.webp" caption="" >}}

I'm super proud and all that she achieves on and off the bike and just can't believe how lucky I am to have her in my life.

I ❤️ you Mandeep!!! 

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j9-gavernie" >}}

---

<h3 class="flex-container" id="D10">
  <span class="left"> 🍆 Day 10: Col des Spandelles (opposite side)</span>
  <span class="right"><a href="https://www.strava.com/activities/11993543669" alt="Strava" target="_blank">77km / 1283m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Double digit days in the Pyrénées now and there's still so much riding to do! At this point we had pretty much hit all the A-list climbs from Argelés-Gazost. Now we had scoped out other routes that we were interested to explore. Today was Spandelles three ways: each of us (me, my wife Mandeep, and our friend Felix) took our own route to the top of Col des Spandelles. If we timed it right, we'd all end up there together. This was perfect as each of us was ready for a different level of difficulty after our big Gavernie day previously.

Mandeep wanted something "easy" so did the direct climb from town. (It's hardly easy, a proper 1st cetegory ~1000m / 7% climb!!) Felix went in the opposite direction and already this trip was smashing double climbs and triple digit kms daily, so he planned a long loop over Cols de Soulor and d'Aubisque (see our Day 2 ride), and around to the opposite approach up Col des Spandelles (aka the non-reverse climb that is more commonly used in in the Tour de France). I wanted a longer ride without too much more climbing, so I did the reverse of our Day 4 ride out the valley, past Lourdes, through Lestelle-Bétharram (site of my future retirement - see my day 4 posts for more ;), up the adjacent valley and up the non-reverse Spandelles climb.

I love this climb - it is maybe my second favorite (after Gavernie / Col des Tentes). So different this one - quiet, almost no cyclists and fewer cars, narrow and windey the whole way.

At the top, Mandeep had arrived just minutes before me. Well-timed! Not so much for meeting Felix. Having taken on a much longer and harder route, he was still about an hour back. So Mandeep and I descended back to town and parked ourself at a terrasse bar in the centre to enjoy some well-deserved beverages and watch the Olympics womens' time trial.

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j10-spandelles" >}}

---

<h3 class="flex-container" id="D11">
  <span class="left">🫠 Day 11: Running through the canicule</span>
  <span class="right"><a href="https://www.strava.com/activities/12000763247" alt="Strava" target="_blank">8km run {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Sooo.... running again? Heat wave is in full swing now, and the legs are really feeling it. So fitting in a run here and there is a prefect rest/not-rest day activity. Mandeep and I took off on a trail down the river (Rive Gave) to last year's swimming spot, then I continued on to check out the villages of Boô-Silhen and Silhen, and randomly discovered the pirate hideout Deth Potz 🏴‍☠️. Nearly melted under the sun and finished at our new swimming spot a few meters from our door. AaaRRRRrrrrrrrrrrgh.

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j11-deth-potz-run" >}}

---

<h3 class="flex-container" id="D12">
  <span class="left">⛰️⛰️ Day 12: Les Cols d'Aspin et du Tourmalet.</span>
  <span class="right"><a href="https://www.strava.com/activities/https://www.strava.com/activities/12010119333" alt="Strava" target="_blank">128km / 2724m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

If there was a queen stage this trip, for me this was it - my only two-col day, plus some serious heat. I headed out early on an approximate reverse loop of our Day 7 Tourmalet ride: Leaving Argelés-Gazost, and heading north down the valley towards Lourdes, then east across a lovely and deserted windy road to the adjacent valley. The mission: to climb the other (east) side of Tourmalet, which for some is the more "official" or exciting ascent. But before that, an added bonus of Col d'Aspin. Felix headed out after me but soon caught up and we made our way up Col d'Aspin. It was great and quiet, not much car or bike traffic and another beautiful winding road. Aspin isn't huge (600 or 900 meters depending where you count from, and around 4%) and the sun was still low to the temps were manageable and the climb enjoyable. Then came the main event, back down Aspin and settling in to the mighty Tourmalet once again. This side is a monster, 1250m at average 7.5%. And by now the sun was blazing with nowhere to hide. Felix was long up the road, and I took my time up to the top. 35 water refills later I made it, enjoyed a cheese and baguette sandwich and a Coke, then Felix and I headed back down towards home. The descent was pretty insane - it's long (maybe 40km back to home, almost all downhill), and with every meter back down it got hotter and hotter. We were well cooked by the time we got back, and celebrated with another visit to the swimming hole and a walk into town for pizza in the evening. 

5 star day. I'm dead. ☠️

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j12-2-cols" >}}

---

<h3 class="flex-container" id="D13">
  <span class="left">🏖️ Day 13: Rest day!</span>
</h3>

Not much to report here. Major heat wave outside and tired tired legs. Some swimming, relaxation, and not much else!

---

<h3 class="flex-container" id="D14">
  <span class="left">🥵 Day 14: Cirque de Gavernie reprise</span>
  <span class="right"><a href="https://www.strava.com/activities/12027638795" alt="Strava" target="_blank">100km / 2207m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

Here is a testament to how insane Argelés-Gazost and its surroundings are for cycling. It's day 14 of this trip (11th day on the bike, I think?) and I had yet to repeat a climb. But we all were in agreement the this Col de Tentes route was our favourite, so the tree of us decided to do it one more time before the end of the trip. Same as before, this is a beast with close to 2000 meters of climbing straight from our door to the top of the Col. It's pretty gentle for the first 30 or so km, then slowly goes up, with the last 10+ km a proper hors categorie slog, in the baking sun. I was definiteyl slow getting my legs in gear, but by the time we hit the upper slopes we were back in business and I loved every moment of it. Felix was Felix and skipped ahead, then casually threw in Luz Ardiden (see Day 6) on the way back for a massive day. Mandeep and I kept it chill (though honestly there was no other way for me).

Major news, saw TWO marmots, but they're elusive little guys so couldn't snap a photo. 🦔 🦔

We capped it off with a proper lovely meal in town at the restaurant [Au Fond Du Gossier](http://aufonddugosier.com/), which I can't recommend highly enough if you are ever in Argelés-Gazost!


{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j14-gavernie" >}}

---

<h3 class="flex-container" id="D15">
  <span class="left">✌️ Day 15: Last day! Lac Estang</span>
  <span class="right"><a href="https://www.strava.com/activities/12034856159" alt="Strava" target="_blank">40km / 893m {{< icon name="strava" pack="fab" >}}</a></span>
</h3>

And so we arrive to our final day. Two weeks in the beautiful Pyrénées mountains, for the second year in a row. I'm out of words. This place is my favourite. I hope I continue to come back here for the rest of my life. I can't think of a better place to ride: great roads, insane world-famous climbs, low traffic, great food, vegetables, and markets, clean rivers to swim in, nice people... I can't think of anything that tops it. But then, the world is big, and surely there are other mountains to be discovered.

#### 📊 Final stats
- 🗺️ 879 kilometers traveled
- ⬆️ 18,063 meters climbed
- 🚵 12 days riding
- 🏃‍♂️ 2 days running
- 🛌 1 true rest day
- 🌊 1 trip to the spa
- ♾️ Infinite joy

#### 🤩 Parting thoughts

I finished our last trip (Tuscany), saying we could have gone further and faster, but every moment was thoroughly enjoyed. *(I'm not suggesting that we should have gone further or faster; on the contrary, our pace was PERFECT :)* This time, I truly don't think I could have gone further, or much faster. It was a hard one. But the rest holds, every moment was beautiful.

Until next time! 👋

{{< gallery resize_options="500x" album="24in24/20240802-pyrenees/j15-lac-estang-last-day" >}}

{{< figure src="IMG_0735_w.webp" >}}

