---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Coffee time"
subtitle: ""
summary: "Dubious facts about coffee scraped from the internet..."
authors: []
tags: ["coffee", "research"]
categories: []
date: 2017-11-30
lastmod: 2017-11-30
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Dubious facts about coffee scraped from the internet...

1\. Do you know that 2.25 billion cups of coffee is consumed daily across the [world](http://www.spinfold.com/38-amazing-and-interesting-facts-about-earth/ "38 Amazing and Interesting Facts about Earth").

2\. You will be wondered to know that after petroleum, coffee is the world’s second most valuable traded commodity.

3\. If you want to have your coffee’s action most effective, consume it between 9:30 am and 11:30 am.

4\. If you love to hang out with [cats](http://www.spinfold.com/50-amazing-facts-about-cats/ "50 amazing facts about cats"), there is a cat cafe in Korea and [Japan](http://www.spinfold.com/amazing-facts-about-japan/ "Amazing Facts about Japan") where you can drink coffee and hangout with cats for hours.

5\. What do you think the first webcam was built for? You will be laughing after knowing this. To check the status of a coffee pot the first webcam was made in Cambridge.

6\. Most of us think that coffee beans are beans but they aren’t, coffee beans are fruit pits.

7\. While drinking coffee you must thank George Washington(not the [U.S](http://www.spinfold.com/amazing-facts-about-usa/ "Amazing Facts about USA"). president) , he was the man who has  invented instant coffee around 1910.

8\. A cup of caffeinated coffee will significantly improve [blood](http://www.spinfold.com/amazing-facts-about-blood/ "Amazing Facts about blood") flow.

9\. You will be shocked to know this. In France there’s a coffee shop, where not saying “please” and “hello” makes your coffee expensive.

10\. You will be amazed to know that over 93 million gallons of milk per year is used by Starbuck coffee shops, which would be enough to fill 155 Olympic-sized swimming pools.

11\. Do you know that the smell of coffee is the second most recognizable odor in America, first comes the smell of burning wood.

12\. Coffee is good for your health, it contains antioxidants which helps to prevent free radicals from damaging cells.  Studies show that a typical serving of coffee contains more antioxidants than a serving of blueberries, raspberries, grape juice or oranges.

13\. Physicians advise that pregnant [women](http://www.spinfold.com/less-known-facts-about-women/ "Less known facts about Women") shouldn’t drink more than 200 milligrams amount of coffee per day, as the organs and systems in a fetus are not able to metabolize and excrete caffeine and it can stay in blood.  It can stay in fetus blood 10 times longer than in an adult, so its better woman not to have coffee while they are pregnant.

14\. Coffee helps [men](http://www.spinfold.com/amazing-and-interesting-facts-about-men/ "Amazing and Interesting Facts about Men") as  it decreases their risk of developing prostate cancer.  Studies show that men who have six or more cups of coffee  daily ,reduce their risk by 20% for developing prostate cancer.

15\. Do you know that National Coffee Day is celebrated in many countries on different dates, in United states it’s celebrated on September 29, in Costa Rica it’s September 12, in Japan it’s October 1 and in Ireland it’s September 19.

16\. It is interesting to know that amount of caffeine in coffee depends on how they were roasted, how the coffee was brewed, and the type of beans. Typically, a small 8-ounce cup of coffee has 115 milligrams of caffeine if it’s drip brewed and 65 milligrams if it’s instant.

17\. Indonesia’s Kopi Luwak or civet coffee is the world’s most expensive coffee in the world, which costs $5o a cup.

---


