---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "New Delhi - Agra - Taj Mahal"
subtitle: "India #6"
summary: "#6 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-15"
lastmod: "2017-12-15"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: 5. Chandigarh]({{< relref "../2017-12-14-chandigarh-rock-garden" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 7. Toy Train ➡️]( {{< relref "../2017-12-16-toy-train-siliguri-darjeeling-india-7" >}})**

> **Post #6 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

We fly from Chandigarh to New Delhi. It is cold and our phones don't work, but no matter - after settling in for a night we take the train to Agra to see the Taj Mahal and Agra Fort. The Taj Mahal, tourist attraction #1 in India, is utterly mind blowing. The best way to explain it that I've heard is that it is beauty at every scale: gazing at it from a rooftop miles away, standing in front of it, and examining its ornately carved surfaces up close. The story of Shah Jahan, the emperor who commissioned the Taj Mahal, is bizarre and somewhat tragic:

> After Mumtaz Mahal \[Shah Jahan's wife\] died in 1631 while giving birth to their 14th child, Shah Jahan undertook the work of constructing world's most beautiful monument in her memory. This monument, which entombs Mumtaz Mahal as well as Shah Jahan, came to be known as "Taj Mahal", the building of which took 22 years and 22000 laborers. It was in 1657 that Shah Jahan fell ill, and Dara, Mumtaz Mahal's eldest son assumed responsibility of his father's throne. **His other son, Aurangzeb, accompanied by his younger brothers Shuja and Murad marched upon Agra to in order to claim their share. They defeated Dara's armies and declared their father Shah Jahan incompetent to rule and put him under house arrest in Agra Fort.** After Shah Jahan died in 1666 in captivity, his body was taken quietly by two men and was laid beside Mumtaz. ([tajmahal.org](https://www.tajmahal.org.uk/shah-jahan.html))

So ahh, yeah, to recap: Emperor spends 22 years building "the world's most beautiful monument" in memory of his beloved wife who died during childbirth; son stages a coup, takes over throne and imprisons the Shah in the fort where he can only stare across the river to the Taj Mahal for the rest of his life. Harsh, bro. (Admittedly this is an utterly incomplete accounting of an incredibly complex period of history, one assumes - and hopes - there was more to the story ;) )

Following the Taj, we tour Agra Fort, splendid in its own magnificence, then head back to Delhi.

The rest of our short stay in New Delhi is is defined by rest after a few long days of travel. Mandeep recovers from stomach illness, I do a few runs around Defense Colony (nothing like running outdoors in the most polluted city in the world!!), and an old friend of Mandeep's pays a visit. Time to head to the Himalayas.

<div class="yt-container">
<iframe width="720" height="405" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22GiP8x8Xesp_RaDDPnS71zC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" class="yt-playlist" allowfullscreen></iframe>
</div>

- Video playlist:
    1. "Staring out a train window, Agra bound"
    2. "Agra traffic from a tuk tuk"
    3. "Monkey business, part 2"

---

### Image gallery

{{< gallery resize_options="500x" album="india_6_new_delhi_agra_taj" >}}

---


