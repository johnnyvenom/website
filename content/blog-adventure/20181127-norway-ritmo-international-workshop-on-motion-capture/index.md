---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Norway - RITMO International Workshop on Motion Capture"
subtitle: "From Oslo to Bergen" # shown on post page only
summary: "In November 2018 I traveled to Norway to participate in a research workshop; afterwards I explored by train." # shown on aggregate pages 
authors: []
tags: [ "travel", "adventure", "academic", "norway", "mocap", "research", "Europe"]
categories: [ "travel", "research", "Europe" ]
date: 2018-11-27T12:55:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---



Last week I returned from Norway, where I participated in the RITMO International Workshop on Motion Capture. [RITMO](https://www.hf.uio.no/ritmo/english/) is a new research center at the University of Oslo for interdisciplinary studies in rhythm, time and motion. I work with motion capture to do movement analysis of musical performance, and recently completed a long [project for concert harp and custom-designed motion controllers for augmented instrumental performance]({{< relref "project/research/gestural-control-of-augmented-instrumental-performance" >}}). For the workshop I gave a short presentation on that project along with the [Noisebox]({{< relref "project/research/noisebox" >}}) project as part of a panel on interactive music systems. Several colleagues from McGill also participated, and when we weren't at the university we found a little time to get out and explore Oslo.

{{< figure src="mcgill_crew_at_ritmo.jpg" caption="The McGill crew: (L-R) me, Carolina Brum, Isabella Cossette, Catherine Massie-Laberge, Esteban Maestre, Marcelo Wanderley" >}}

## Some exploring... 

After the workshop and a 1-day conference to officially open the center, I headed north by train to see a little bit of Norway. From Oslo I traveled to Myrdal, where I boarded the famous Flåm railway which is declared to be the steepest in the world (side note: I question this; see my post on the [Toy Train to Darjeeling]({{< relref "../2017-12-16-toy-train-siliguri-darjeeling-india-7" >}}) - that one may have it beat!). The train beautifully winds down from high elevation to a tiny town (pop. 350) at the opening of a fjord.

<br/>
{{< youtube viqX5gDHRUo >}}
<br/>

After intense fog the first night, I got out in the morning for a hike. After lunch I took the train back up, and caught another to Bergen. However, for my short stay in Bergen it was tightly socked in clouds, wet, and cold. I enjoyed a day of work and a damn fine cup of coffee in town, and headed back to Montreal the following morning.

{{< figure src="Norway-66-of-70-Bergen.jpg" caption="A foggy glimpse of Bergen." >}}

---

## 📸 Photo gallery: 

Some photos: Oslo, RITMO, train travel, Flåm, Bergen, and flying back over the fjords of Norway...

{{< gallery resize_options="500x" album="20181127-norway" >}}

---

## 📽️ RITMO videos

#### Dancing stick figures (motion capture demo of violinists)
{{< youtube 8w29hDkspyE >}}

#### Music at the workshop
{{< youtube 4oUu8WuEiTk >}}

-----

