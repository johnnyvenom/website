---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Amritsar - Golden Temple"
subtitle: "India #3"
summary: "#3 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-10"
lastmod: "2017-12-10"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: Bangalore, 2. Pt. 2]({{< relref "../2017-12-08-a-few-days-in-bangalore-pt-ii" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 4. Bundala, Punjab ➡️]( {{< relref "../2017-12-13-punjab-bundala-simbal-mazara" >}})**

> **Post #3 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

We arrive in Amrisar and are met by Mandeep's father and Arman, teenage wunderkid whose family we'll be staying with in Bundala for the next week. 

### Jallianwala Bagh

{{< figure src="Amritsar-Golden-Temple-02.jpg" caption="Martyr's Well at Jallianwala Bagh" numbered="false" >}}

First we visit **Jillianwala Bagh**.

> **Jallianwala Bagh** ([Hindi](https://en.wikipedia.org/wiki/Hindi "Hindi"): जलियांवाला बाग़) is a public garden in [Amritsar](https://en.wikipedia.org/wiki/Amritsar "Amritsar") in the [Punjab](https://en.wikipedia.org/wiki/Punjab_(India) "Punjab (India)") state of [India](https://en.wikipedia.org/wiki/India "India"), and houses a memorial of national importance, established in 1951 by the Government of India, to commemorate the massacre of peaceful celebrators including unarmed women and children by British occupying forces, on the occasion of the Punjabi New Year on April 13, 1919 in the [Jallianwala Bagh Massacre](https://en.wikipedia.org/wiki/Jallianwala_Bagh_Massacre "Jallianwala Bagh Massacre"). Colonial [British Raj](https://en.wikipedia.org/wiki/British_Raj "British Raj") sources identified 379 fatalities and estimated about 1100 wounded.[\[1\]](https://en.wikipedia.org/wiki/Jallianwala_Bagh#cite_note-1) Civil Surgeon Dr. Smith indicated that there were 1,526 [casualties](https://en.wikipedia.org/wiki/Casualty_(person) "Casualty (person)").[\[2\]](https://en.wikipedia.org/wiki/Jallianwala_Bagh#cite_note-2) The true figures of fatalities are unknown, but are very likely to be many times higher than the official figure of 379. _(source: [Wikipedia](https://en.wikipedia.org/wiki/Jallianwala_Bagh))_

{{< gallery resize_options="500x" album="india_3_amritsar_jallianwala_bagh" >}}

---

### Golden Temple

Then we spend the afternoon at the [Golden Temple](https://en.wikipedia.org/wiki/Golden_Temple), largest and holiest site of the Sikh religion.

<div class="yt-container">
<iframe class="yt-playlist" width="720" height="405" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22FAX6FkJDDvDDAwr-vd3kO0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

- Video playlist:
  1. First Look
  2. Serene
  3. On the way out

#### Images

{{< gallery resize_options="500x" album="india_3_amritsar_golden_temple" >}}

<!-- \[playlist order="DESC" type="video" ids="292,304,305"\] -->

<!-- \[gallery type="rectangular" link="file" ids="290,291,293,294,295,296,297,298,299,300,301,302,303,306,307,308,309,310,311,312,289"\] -->

---

