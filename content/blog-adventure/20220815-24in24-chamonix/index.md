---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "24-in-24: 🏔️ Chamonix-Mont-Blanc"
subtitle: "Entry #0.2 in the series Paris 24 in 24. August 2022." # shown on post page only
summary: "Entry #0.2 of 24 adventures while we live in Paris for 24 months. In this next prequel chapter, we head for Chamonix to discover Mont Blanc and the French Alps." # shown on aggregate pages 
authors: []
tags: [24in24, cycling, France, Paris, Europe]
categories: [travel, cycling]
date: 2022-08-15T11:57:30+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

gallery_item:
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220815-002.jpg
  caption: Scenes from the train between Paris and Lyon.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220815-003.jpg
  caption: Scenes from the train, mountains getting closer...
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220815-004.jpg
  caption: Headed up the vally to Chamonix
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220815-005.jpg
  caption: Getting out on the hotel's townie bikes. (Look ma, no boot!)
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220815-007.jpg
  caption: Turquoise waters coming straight from the Mer de Glace (glacier).
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220815-008.jpg
  caption: Loving the townie bikes
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220815-009.jpg
  caption: Rafters coming down the chute
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-010.jpg
  caption: Mont Blanc towers over Chamonix
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-011.jpg
  caption: Our first sighting of paragliders!
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-012.jpg
  caption: Stunning clear views of Mont Blanc
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-013.jpg
  caption: Stunning clear views of Mont Blanc
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-014.jpg
  caption: Mont Blanc watching over the streets of Chamonix
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-015.jpg
  caption: Exploring by bike, and finding our picnic spot where the paragliders land. 
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-016.jpg
  caption: Halfway up the Télépherique de l'Aguille du Midi. 
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-017.jpg
  caption: Up close and personal with the mountains on Aguille du Midi. 
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-018.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-019.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-020.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-021.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-022.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-023.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-024.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-025.jpg
  caption: Stunning views in all directions from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-026.jpg
  caption: Mountaineers doing mountaineer things. 
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-027.jpg
  caption: Mountaineers doing mountaineer things.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-028.jpg
  caption: More views from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-029.jpg
  caption: More views from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-030.jpg
  caption: More views from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-031.jpg
  caption: More views from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220816-032.jpg
  caption: More views from Aguille du Midi.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220817-033.jpg
  caption: Exploring up the valley in Vallorcine.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220818-034.jpg
  caption: Exploring up the valley in Vallorcine.
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220818-035.jpg
  caption: Nice bike! (not mine, unfortunately...)
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220819-036.jpg
  caption: Clouds in, we out ✌️
- album: 24in24/20220815-chamonix
  image: 202208-Chamonix-20220819-037.jpg
  caption: Clouds in, we out ✌️

---

> *In the spring of 2022, I accepted a post-doctoral fellowship at the University of Paris-Saclay that would last for two years, from September 2022 until August 2024. My partner Mandeep and I moved to France in August to set up shop before I would get to work in September.*

<br />

<div class="btn-row">
  {{< cta cta_text="👈 Previous:<br/>🍇 Rhône Valley" cta_link="/adventures/2022/08/08/24-in-24-rhone-valley" cta_new_tab="false" >}}
  {{< cta cta_text="See the whole series!<br/>🇪🇺 24 in 24" cta_link="/tag/24-in-24" cta_new_tab="false" >}}
  {{< cta cta_text="Next 👉<br/>🇨🇭🇮🇹 Swiss/Italy" cta_link="/adventures/2022/08/29/24-in-24-zurich-and-the-alps/" >}}
</div>

---

## 🏙️ 48 hours in Paris

After returning from the [Rhone Valley]({{< relref "../20220808-24in24-rhone-valley" >}}) we spent a short weekend in Paris before we were off again. We had a tiny Airbnb next to the Gare de Lyon train station, where we enjoyed our first home cooked meal in a long time. It wasn't much, but we welcomed it.

{{< figure src="202208-Chamonix-20220814-001.jpg" caption="First home-cooked meal in France!!! 🍾" >}}

## 🛤️ To the Alps!

And then we were back on a train. This time we headed east towards the Alps. We stared out the window is endless fields turned into hills. And then we got the first glimpses of the mountains coming closer. Before we know it, and a couple train switches later we were making our way up the valley in the heart of the Alps to Chamonix.

Mandeep's foot was healing; she was able to walk farther and no longer needed to wear the protective boot. But we were still not ready for any major hiking or cycling. We got a nice surprise at our hotel though, which had town bicycles that we could take out, which we took great advantage of throughout our stay.

{{< figure src="202208-Chamonix-20220815-006.jpg" caption="Townie bikes were our jam." >}}

On the bikes we circled around Chamonix and got a lay of the land, enjoying the stunning views of the mountains and turquoise waters of the river that flowed through the center of town.

The next morning, as we walked out for coffee, the sky was filled with paragliders. It was new for us.
Maybe we've seen one or two here or there, but seeing the sky filled with them was a truly awesome experience.

{{< video src="22-08-16 10-32-56 1025.mov" controls="yes" >}}

Touring around Chamonix again, we discovered the huge field where the paragliders would circle into and land. We took a blanket, some food, and a bottle of rosé, and enjoyed a leisurely afternoon picnic watching the paragliders swoop down.

{{< youtube qOATuBu9OXc >}}

<br />

## 🚠 Aguille du Midi

Seeing as hiking was out of the question but we were still interested in getting up into the mountains, it only made sense that we would take what is, perhaps, Chamonix's number one tourist attraction: [Téléphérique de l'Aiguille du Midi](https://www.montblancnaturalresort.com/en/aiguille-du-midi).
This is a piece of work, comprised of two cable cars, that ascend 2,800 meters from the town to the top of the Aguille du Midi, a rocky spire that looks directly across to Mont Blanc.

Mont Blanc, of course, is the tallest of the Alps, and it is truly breathtaking to see up close.

{{< figure src="202208-Chamonix-20220816-024.jpg" caption="Face to face with Mont-Blanc." >}}

Surprisingly, there's a lot of infrastructure at the top of Aguille du Midi, plenty of places to look out and gaze at all parts of the mountain. 
It was incredible to see all of the different mountaineering activities in full swing.
Close inspection of a rock would reveal climbers and their ropes, rappelling down the sides, and ants speckling the snow fields that upon closer inspection revealed lines of hikers and explorers making their way to points unknown.
The enormity of Mont Blanc and the Alps was staggering.

{{< youtube Bau_pNdO43Q>}}

<br />

## 🧀 Settling in to Chamonix

The subsequent days took us exploring by bicycle and by the train that leads up the valley to some of the smaller towns to explore the quieter side of Chamonix.
We enjoyed some excellent dinners, including a questionable call to eat raclette in August. I was especially excited for this, and we had a wonderful meal of, well, mostly cheese and potatoes, and decided wholeheartedly that, yes raclette is absolutely appropriate in August in the Alps.

<!-- <div class="center-embed">
  <iframe title="Pixelfed Post Embed" src="https://pixelfed.social/p/johnnyvenom/503594788407800697/embed?caption=true&likes=true&layout=full" class="pixelfed__embed" style="max-width: 100%; border: 0" width="554" allowfullscreen="allowfullscreen"></iframe><script async defer src="https://pixelfed.social/embed.js"></script>
</div> -->

{{< youtube 0eciGgfNUKc >}}

<br />


We took a moment to write and send off some postcards and all of our wedding thank you cards, as we were still basking in the glow of [our recent wedding](https://mandeepandjohnny.ca).

{{< figure src="202208-Chamonix-mail.jpg" >}}

Finally, as the clouds moved in, we were ready to say goodbye and make our way back to Paris and continue getting settled for our new life there.

---

## 📸 Photo Gallery

Scenes from Chamonix-Mont-Blanc, August 2022. 

{{< gallery resize_options="500x" album="24in24/20220815-chamonix" >}}

---

## 📹 Video playlist


<div class="yt-container">
<iframe class="yt-playlist" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22FfqEkgM8u2ygHrdJZEc3n6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

> *Check out all the videos in the playlist (top left icon in the video player!)*

---
