---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Costa Rica 2022: Part 1"
subtitle: "La Fortuna, Arenal Volcano National Park, and a snake"
summary: "#1 of 3: La Fortuna, Arenal Volcano National Park, and a snake. It's February 2022, and my wife Mandeep and I are taking our first vacation together since COVID encircled the world in early 2020."
authors: []
tags: [adventure, travel, "Costa Rica", "Latin America"]
categories: [travel, "Latin America" ]
date: 2022-03-06T10:21:42-06:00
lastmod: 2022-03-06T10:21:42-06:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

gallery_item:
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-01.jpg
  caption: Approching La Fortuna at sunset 
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-02.jpg
  caption: Arenal Volcano
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-03.jpg
  caption: Peering into the darkness at the Arenal Oasis Eco Lodge
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-04.jpg
  caption: Some of the wildlife looks kind of funny around here
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-05.jpg
  caption: The softest jungle creature
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-06.jpg
  caption: The squirrels don't look like Montreal's...
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-07.jpg
  caption: Bananna plant?
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-08.jpg
  caption: Propeller?
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-09.jpg
  caption: Vacation is the best
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-10.jpg
  caption: Work hard, play hard
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-11.jpg
  caption: That volcano tho
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-12.jpg
  caption: Colors!
- album: costa-rica/part-1/1-la-fortuna
  image: Costa-Rica-2022-30.jpg
  caption: The fearsome Fer-de-Lance!

- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-13.jpg
  caption: That's it, that's the park
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-14.jpg
  caption: This blue bird was obsessed with Mandeep.
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-15.jpg
  caption: Fancy blue jay.
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-16.jpg
  caption: But, like, obsessed.
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-17.jpg
  caption: "Mandeep: not impressed."
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-18.jpg
  caption: "Fancy-ass blue jay: not impressed either."
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-19.jpg
  caption: Lava flows on the west side of the volcano.
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-20.jpg
  caption: Lake Arenal in the distance
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-21.jpg
  caption: So big it takes two pictures
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-22.jpg
  caption: Crazy to be that close to a volcano!
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-23.jpg
  caption: But we weren't skerred.
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-24.jpg
  caption: 400 year old ceiba tree
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-25.jpg
  caption: Massive
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-26.jpg
  caption: Crazy view
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-27.jpg
  caption: So much going on up top!
- album: costa-rica/part-1/2-arenal-volcano
  image: Costa-Rica-2022-28.jpg
  caption: Well deserved post hike patacones (fried plantains) and local beer

- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-32.jpg
  caption: You can't see it but that tree is filled with howler monkeys
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-33.jpg
  caption: There's one
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-34.jpg
  caption: That blob is a two-toed sloth
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-35.jpg
  caption: The sloth a little closer up
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-36.jpg
  caption: Even closer
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-37.jpg
  caption: Definitely a monkey (howler monkey)
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-38.jpg
  caption: Don't know what was in this tree but probably something cool... (an owl?)
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-39.jpg
  caption: Where the Pieñas Blancas meets the San Carlos
- album: costa-rica/part-1/3-penas-blancas
  image: Costa-Rica-2022-40.jpg
  caption: Where the Pieñas Blancas meets the San Carlos

- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-41.jpg
  caption: We made it to the next destination and went straight for the pool for sunset
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-42.jpg
  caption: Hot tub over infinity pool over lake
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-43.jpg
  caption: What a stunning view!
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-44.jpg
  caption: The colors....
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-45.jpg
  caption: Stahhhhp already
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-46.jpg
  caption: Just. Incredible.
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-47.jpg
  caption: Panaoramic view from our balcony in the morning
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-49.jpg
  caption: Arenal volcano shrouded in clouds
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-50.jpg
  caption: Looking across the lake
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-52.jpg
  caption: We could have stayed here for a week
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-53.jpg
  caption: Daily exercise
- album: costa-rica/part-1/4-lake-arenal
  image: Costa-Rica-2022-54.jpg
  caption: Daily exercise



---

It's February 2022, and my wife Mandeep and I are taking our first vacation together since COVID encircled the world in early 2020. We're going to Costa Rica. For an activity, to preserve some memories, and for the pure fun of writing, I've kept a running travelogue of our trip. It's hardly a travel guide and far from philosophical. Enjoy.

*[This seems like an awful lot of text; can't I just look at some pretty pictures?](#-photo-galleries)*

---

{{< toc >}}

---
<br>

## 🇨🇷 Prelude: Travel day!
<br>

Montreal can be brutal in February. We leave in the middle of a minor ice storm. The day before, temps had soared well above 0°C, it rained all day and the city was a sloppy mess. Freezing temps overnight turned rain to ice, sidewalks to skating rinks in the predawn morning as we piled into a taxi and headed for the airport. By the time we boarded the plane the temperatures had risen once again, and the airplane windows were streaked with rain while we endured a lengthy delay for de-icing before takeoff.

### 🚨 Beware the TSA

A moment of panic occurred when we made our way through the always hectic security screening on the way to our departure gate. After sending my carry-on luggage, including my brand new MacBook Pro and iPad Mini in their own separate bin, through the security screening system, we were momentarily delayed in the line for personal screening. Emerging on the other side, the bin with my laptop and iPad was nowhere to be found. A frantic search ensued for the next 30 minutes or so, involving Jesse, the owner of [Pompui](https://pumpui.ca/), an incredible Thai restaurant just steps from our house, and who coincidentally owns a nearly identical laptop and iPad. Jesse and his friends were on their way to British Colombia to do some backcountry snowboarding and went through screening a few minutes before us. Making the encounter even more coincidental, Jesse's partner is Mandeep's hairdresser, who recommended us to stay at her friend's hotel in Monteverde where we booked two nights during our stay as the crown jewel of our vacation. (More on the hotel later; spoiler alert: it. Was. Excellent.) 

Jesse had a different ordeal, in which he was selected for additional screening and all of his luggage was thoroughly emptied and searched. In his hasty repacking, he neglected to collect his computer and iPad and they were left at the same security line what we went through shortly after. Thus hilarity ensued when I was handed his electronics, tracked him down assuming he had taken mine by accident, and he found out he in fact had never collected his, yet mine were still nowhere to be found. After assuming the worst, a TSA agent returned behind their area and after a more diligent search, somehow reappeared with my devices. A massive sigh of relief was exhaled, two tall beers were consumed (at 6am, trust me they were needed), and we were on our way.

After a straightforward direct flight, we arrived in San José, Costa Rica in the afternoon, picked up our rental car, and drove three hours into the mountains to La Fortuna, which sits in the shadow of the now dormant Arenal Volcano. 

{{< figure src="Costa-Rica-2022-02.jpg" alt="A picture of Arenal Volcano at sunset">}}

We stayed at the [Arenal Oasis Eco Lodge](https://www.arenaloasis.com/) in a little cabin in the forest. We were exhausted so we had dinner at the little outdoor restaurant at the resort and exhaustedly put ourselves to bed serenaded by a chorus of frogs and a million unknown and unseen wild things around us.

<br>

---

<br>

## 🇨🇷 Day 1: Arenal Volcano, La Fortuna
<br>

Woken by crazy loud and close howls of wild animals around 4am. Correctly guessed monkeys, and upon rising learned they were the very appropriately named howler monkeys. That sound would be a constant early morning wake up alarm.

We ate the first of several casado breakfasts: rice and beans, plantains, fresh cheese and, depending on the preparation, eggs, tortillas, and salsa.

### 🌋 Arenal Volcano National Park

{{< figure src="Costa-Rica-2022-13.jpg" alt="Picture of the Arenal Volcano National Park map.">}}

After breakfast we walked the short nature trail at the eco lodge, then went to the [Arenal Volcano National Park](http://www.sinac.go.cr/EN-US/ac/ACAHN/pnva/Pages/default.aspx) and hiked for the afternoon. A dry and hot hike yielded spectacular close up views of the volcano and laval flows. We tried to go the the Peninsula Section of the park, which protrudes into Lake Arenal but our little 2WD sedan rental car was no match for the rugged dirt road and we turned back halfway in.

### 🐍 Fer-de-Lance

{{< youtube fuNifRqdtXg >}}

The sighting of the day (for me, at least) occurred when we drove into town to find a place for dinner. There is a short dirt road to get out of the eco lodge. and about halfway down the headlights shined on what looked like a stick laid across the road. I stopped a few feet away and realized quickly it was a long brown snake. We got out and, from a safe distance looked closer and I shot a video as it slowly made its way into the grass on the side of the road. Its size and distinctive triangular markings were impressive, though its unremarkable light brown color made me believe it was a harmless, albeit large, species.

A day later, on our river adventure, I asked the guide about poisonous snakes in Costa Rica and he told me about the [Fer-de-Lance](https://animalia.bio/fer-de-lance), the most dangerous, lethal, and common poisonous snake in Central America, and based on his description I wondered if that is what we had encountered. I looked back at the video when we returned and compared it with some images online. Sure enough, it left little doubt, we had been face to face with a killer. Much respect snake, much respect.

<br>

---

<br>

## 🇨🇷 Day 2: River Safari and Lake Arenal
<br>

{{< figure src="Costa-Rica-2022-39.jpg" alt="Picture of the confluence of two rivers" >}}

Fully prepared for our howler monkey revelry, our early morning alarm came as no surprise and we dozed in our cozy jungle bungalow through the sounds of monkeys, birds, frogs and more, topped off with a sudden and dense rainfall. We enjoyed our morning breakfast, then headed off in a tour van for the day's main activity: a guided "river safari".

This was a leisurely float down the Peñas Blancas and San Carlos Rivers aboard an inflatable raft. There were 6 of us, plus one chatty guide in the boat, along with another raft and a few kayaks in the whole group. The float took about three hours in total, and we saw several birds: spectacled and black and white owls, herons, egrets, vultures, sandpipers, and many more. We saw a keel-billed toucan for the briefest of moments as it flew away. Four footed friends (and foes) were well represented as well. Howler monkeys (previously heard but not seen) presented themselves high in the treetops above the river at several points. A two-tweed sloth was found comatose in the fork of a tree as we floated by. We witness two crocodiles and one 'trunkadile' (looks like a croc but it's just a floating log). Despite the cramped boat inflicting a numb backside and creaky stiff legs by the end of it, we got a great feel for the Costa Rican interior and were glad for the experience.

We were back to our eco lodge in the early afternoon in time to check out and head around to the back side of Lake Arenal for a one night stay at the [La Fortuna Costa Rica Hotel Linda Vista, Arenal Volcano and Lake View](https://hotellindavista.com/), a faded but resplendent hotel with a hot tub and infinity pool overlooking Lake Arenal and the volcano. After enjoying the cozy jungle cabin of our previous stay, the massive open expanse and breathtaking view were a welcome contrast.

{{< figure src="Costa-Rica-2022-45.jpg" alt="View from the hotel pool over Lake Arenal" >}}

<br>

## ---

<br>

## 📸 Photo Galleries

1. 🎋 [Approching La Fortuna and the Arenal Oasis Eco Lodge](#-1-approching-la-fortuna-and-the-arenal-oasis-eco-lodge)
2. 🌋 [Arenal Volcano National Park](#-2-arenal-volcano-national-park)
3. 🛶 [Floating down the Rio Pieñas Blancas](#-3-floating-down-the-rio-pieñas-blancas)
4. 🐟 [The Hotel Linda Vista overlooking Lake Arenal](#-4-the-hotel-linda-vista-overlooking-lake-arenal)

<br>

---

<br>

#### 🎋 1. Approching La Fortuna and the Arenal Oasis Eco Lodge

<br>

{{< gallery resize_options="500x" album="costa-rica/part-1/1-la-fortuna" >}}

<br>

---

<br>

#### 🌋 2. Arenal Volcano National Park

<br>

{{< gallery resize_options="500x" album="costa-rica/part-1/2-arenal-volcano" >}}

<br>

---

<br>

#### 🛶 3. Floating down the Rio Peñas Blancas

<br>

{{< gallery resize_options="500x" album="costa-rica/part-1/3-penas-blancas" >}}

<br>

---

<br>

#### 🐟 4. The Hotel Linda Vista overlooking Lake Arenal

<br>

{{< gallery resize_options="500x" album="costa-rica/part-1/4-lake-arenal" >}}

<br>

-----


