---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "24-in-24: 🍇 Rhône Valley"
subtitle: "Entry #0.1 in the series Paris 24 in 24. August 2022." # shown on post page only
summary: "Entry #0.1 of 24 adventures while we live in Paris for 24 months. In this prequel, we explore France's Rhône Valley by bike and train." # shown on aggregate pages 
authors: []
tags: [24in24, cycling, France, Paris, Europe]
categories: [travel, cycling]
date: 2022-08-08T10:14:58+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

gallery_item:
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (01 of 84).jpg
  caption: A view out our hotel window in Paris the night before leaving
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (02 of 84).jpg
  caption: Gare de Lyon in Paris, heading out before dawn
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (03 of 84).jpg
  caption: Bike assembly at the train station in Lyon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (04 of 84).jpg
  caption: Bike assembly at the train station in Lyon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (05 of 84).jpg
  caption: Packed up and ready to roll!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (07 of 84).jpg
  caption: Leaving Lyon, looking across the Rhône
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (08 of 84).jpg
  caption: Some Lotto Soudal boys rolled past...
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (09 of 84).jpg
  caption: Found some shade to take a break from the heat!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (10 of 84).jpg
  caption: The famous "cotes de Rhône"
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (11 of 84).jpg
  caption: Welcome shade
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (12 of 84).jpg
  caption: Welcome shade
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (13 of 84).jpg
  caption: Cooling off, looking for vegetarian food...
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (14 of 84).jpg
  caption: Slim pickings for a veg lunch, but this cheese quiche will have to do. 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (15 of 84).jpg
  caption: Made it! 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (16 of 84).jpg
  caption: La Grand Maison on arrival
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (17 of 84).jpg
  caption: La Grand Maison on arrival
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (18 of 84).jpg
  caption: La Grand Maison on arrival
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (19 of 84).jpg
  caption: La Grand Maison on arrival
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (20 of 84).jpg
  caption: La Grand Maison on arrival
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (21 of 84).jpg
  caption: Exploring the little village of Montmeyran
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (22 of 84).jpg
  caption: Look, no air boot! Mandeep's getting better...
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (23 of 84).jpg
  caption: Map of the Rhône Valley wine region
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (24 of 84).jpg
  caption: Lovely way to spend a heat wave...
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (25 of 84).jpg
  caption: La Grand Maison hosted us for a lovely dinner!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (26 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (27 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (28 of 84).jpg
  caption: Early morning ride to Col de Tourniol
  image: 2022-08a Rhone Valley (29 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (30 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (31 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (32 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (33 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (34 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (35 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (36 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (37 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (38 of 84).jpg
  caption: Early morning ride to Col de Tourniol
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (39 of 84).jpg
  caption: Last night at La Grand Maison 😢
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (40 of 84).jpg
  caption: Fresh figs off the tree
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (41 of 84).jpg
  caption: Exploring Avignon!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (42 of 84).jpg
  caption: Exploring Avignon!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (43 of 84).jpg
  caption: Exploring Avignon!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (44 of 84).jpg
  caption: Exploring Avignon!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (45 of 84).jpg
  caption: Exploring Avignon!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (46 of 84).jpg
  caption: Exploring Avignon!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (47 of 84).jpg
  caption: Exploring Avignon!
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (48 of 84).jpg
  caption: Ghost sign in Avignon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (49 of 84).jpg
  caption: Ghost sign in Avignon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (50 of 84).jpg
  caption: Ghost sign in Avignon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (51 of 84).jpg
  caption: Exploring Avignon some more
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (52 of 84).jpg
  caption: Exploring Avignon some more
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (53 of 84).jpg
  caption: Obligatory pizza pic
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (54 of 84).jpg
  caption: "Sur la pont d'Avgnon...."
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (55 of 84).jpg
  caption: Mont Ventoux in the distance! 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (56 of 84).jpg
  caption: Mont Ventoux in the distance! 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (57 of 84).jpg
  caption: Medieval vibes...
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (58 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (59 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (60 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (61 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (62 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (63 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (64 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (65 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (66 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (67 of 84).jpg
  caption: Touring the papal palace
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (68 of 84).jpg
  caption: More walking... more Avignon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (69 of 84).jpg
  caption: More walking... more Avignon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (70 of 84).jpg
  caption: More walking... more Avignon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (71 of 84).jpg
  caption: Savon le chat
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (75 of 84).jpg
  caption: Couscous for dinner in a striking glass-ceilinged restaurant
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (76 of 84).jpg
  caption: Night scene in Avignon
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (77 of 84).jpg
  caption: Breakfast on the train
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (78 of 84).jpg
  caption: Trying my hand at some iPad sketching (tracing... let's be real here)
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (79 of 84).jpg
  caption: Nuclear power plant? 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (80 of 84).jpg
  caption: Nuclear power plant? 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (81 of 84).jpg
  caption: More sketching/tracing to pass the time
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (82 of 84).jpg
  caption: Parting shots in Lyon, waiting for the train back to Paris. 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (83 of 84).jpg
  caption: Parting shots in Lyon, waiting for the train back to Paris. 
- album: 24in24/20220808-rhone-valley
  image: 2022-08a Rhone Valley (84 of 84).jpg
  caption: Parting shots in Lyon, waiting for the train back to Paris.

---

<div class="btn-row">
{{< cta cta_text="See the whole series!<br/>🇪🇺 24 in 24" cta_link="/tag/24-in-24" cta_new_tab="false" >}}
{{< cta cta_text="Next 👉<br/>🏔️ Chamonix-Mont Blanc" cta_link="/adventures/2022/08/15/24-in-24-chamonix-mont-blanc/" >}}
</div>

> In the spring of 2022, I accepted a post-doctoral fellowship at the University of Paris-Saclay that would last for two years, from September 2022 until August 2024. My partner Mandeep and I moved to France in August to set up shop before I would get to work in September.

Fast forward to August 2022, after Mandeep and I arrived in France, we spent most of the month doing some traveling around. And we decided seeing as everybody else in Paris seems to leave, why don't we do the same too. We had planned a bike touring trip down the Rhône Valley and booked a few different places to stay along the way, biking from place to place for four nights. However, before we left Montreal, Mandeep broke her foot in a freak accident when she was playing with her nieces. And so we arrived in France with Mandeep still wearing a an air boot and unable to bike for a little while. 

{{< figure src="2022-08a Rhone Valley Mandeep boot.jpg" caption="Making the most of our first days in Paris with limited mobility!" >}}

So we modified the plan. Our original route was to be from Lyon to Avignon, a stretch of about 300 kilometers to bike down the Rhône Valley along the Rhône River. So we canceled the accommodations that we had made in between, and instead just booked one in the middle that we found. It was called [La Grand Maison](http://www.lagrandemaisondrome.com/) in the village of Montmeyran a little ways outside Valence. So we booked that instead for two or three nights. We would both take the train from Paris to Lyon and then Mandeep would take the train directly there. I would bike from Lyon to Le Grand Maison. I'd arrive in the evening and we would stay for a couple nights. Then I'd bike the rest of the way to Avignon, our end point, while Mandeep once again took the train. We had two more nights booked at a hotel in Avignon, after which we would take the train back to Lyon, and then back to Paris. 

---

## 🔥 Part 1: Le canicule - cycling through a heat wave

Mandeep and I left our hotel next to Gare de Lyon in Paris before daylight. My bike was all packed up in its travel bag and we made our way to the train station and got on the train bound for Lyon. When we got to Lyon two hours later, the first item of business was for me to assemble my bike and get it packed for the adventure ahead. I did that and then we parted ways, Mandeep on the train to our destination and I set off on the 150 kilometer route that I had planned out. 

For something different I decided to Tweet about my adventure on the way, which I did for a while until the heat got too much for excess activity so the tweets end up pretty sparse for the second half of the trip. 

<div class="center-embed"><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Ok so, going on a little <a href="https://twitter.com/hashtag/cycling?src=hash&amp;ref_src=twsrc%5Etfw">#cycling</a> adventure today and the next few days... it&#39;s 5am in <a href="https://twitter.com/hashtag/Paris?src=hash&amp;ref_src=twsrc%5Etfw">#Paris</a> and this is what it looks like out the hotel window. I&#39;ll tweet about it, cause... why not 😁 🚴🇫🇷 1/??? <a href="https://t.co/dA6LXwMkAk">pic.twitter.com/dA6LXwMkAk</a></p>&mdash; Johnny Venom (@johnnyvenom@hci.social) (@jonnyvenom) <a href="https://twitter.com/jonnyvenom/status/1556480792381071362?ref_src=twsrc%5Etfw">August 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div> 

<br />

With France in the grips of a heat wave, it was [a long hot day in the saddle](https://www.strava.com/activities/7604029505). That said, it was beautiful, with a nice route running along the bank of the Rhone River. The route is part of [EuroVelo 7](http://www.eurovelo.com/en/eurovelos/eurovelo-7), "The Sun Route", an established continental bike route that traverses Europe north to south, from the top of Norway to Malta in the Mediterranean Sea. After a long, arduous and sweaty journey I arrived at Le Grand Maison in the late afternoon where Mandeep was waiting. 

<br />

<div class="center-embed">
  <div class="strava-embed-placeholder" data-embed-type="activity" data-embed-id="7604029505"></div><script src="https://strava-embeds.com/embed.js"></script>
</div>

<br />
<br />

<div class="center-embed">
  {{< cta cta_text="👇 Photos below! 👇" cta_link="#-photo-gallery" >}}
</div>

---

## 🏞️ Part 2: La campagne

{{< figure src="2022-08a Rhone Valley Le Grand Maison.jpg" caption="Le Grand Maison" >}}

Le Grand Maison was truly a sight for sore eyes, and it was a beautiful place to stay for a few days in the bucolic French countryside. [I did a nice ride](https://www.strava.com/activities/7613441098) to climb the nearby Col du Tourniol one day and we spent plenty of time just relaxing, lounging by the pool, eating good food and not too much else. By the time we were to leave we were sad to go. Because of the heat, which was formidable, I opted not to bike the second leg and instead joined Mandeep on the train to Avignon. 

---

## Part 3: Avignon

{{< figure src="2022-08a Rhone Valley Avignon.jpg" caption="Exploring Avignon" >}}

Avignon was a complete and welcome surprise. It's a beautiful, well-preserved medieval city that housed the Pope in medieval times. Remarkably well-preserved, it was a treat to discover so much history and beautiful architecture as we walked around. We spent two days there, saw the sights, enjoyed great food and beat the heat as best we could. And then finally boarded the train to go back to Lyon and then finally back to Paris.

---

# 📸 Photo Gallery:

Enjoy a (mostly chronological) photo gallery of our Rhône Valley adventure!

{{< gallery resize_options="500x" album="24in24/20220808-rhone-valley" >}}

---

