---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Hello, and welcome..."
subtitle: "Hello (and goodbye) from Hearthrow"
summary: "In 2017, I decided it was time to bid adieu to Facebook and take full ownership of my content. I launched a new site that coincided with the start of a month-long trip around India."
authors: []
tags: [ "travel", "adventure", "india", "website", "Europe"]
categories: [ "travel", "India" ]
date: "2017-12-01"
lastmod: "2017-12-01"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: #0. Colors ➡️]( {{< relref "../2017-12-02-colors" >}})**

> In 2017, I decided it was time to bid adieu to Facebook and take full ownership of my content. Though I continued to post infrequently to Instagram, I built a new website to post my travels, adventures, etc. to. This was just before my girlfriend (now wife) embarked on a month-long trip around India, which provided plenty of new things to post about. That site I built is still up at https://archive.johnnyvenom.com. However, a few years later and I opted to rebuild my site using Hugo, a static site generator, and the [Wowchemy website building platform](https://wowchemy.com/) to orient it towards my academic and professional work. 
>  
> However, I didn't want all this older content to waste away, and I love adventuring, travel, (and yes, cycling and coffee) too much to let these posts fade away. So I've created a new section of the site and brought everything over to one place. Here was the original post from that 2017 new website launch.

Hello there and welcome to the new new.johnnyvenom.com site. Consider this a soft launch of sorts, and pay no attention to the barren pages at the moment. They will all be filled in due time.

As a means of a kickstart, I'm typing this at a departure gate at London Heathrow Airport, where I am waiting for a flight to India. In lieu of Facebook, I'll send up some sights and sounds of the next month as Mandeep and I traipse around the country.

-----

