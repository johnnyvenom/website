---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Kerala: Varkala"
subtitle: "India #11"
summary: "#11 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2018-01-01"
lastmod: "2018-01-01"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

---

**[⬅️ Previous: 10. Kerala]({{< relref "../2017-12-31-kerala-the-backwaters-india-10" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}})**

> **Post #11 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

And so, at long last we reach the end of our holiday adventure on the beaches of Varkala. Little more needs to be said, a few languid days of sunshine, Kingfisher beer, salt on our skin and in our hair.

---

### Videos

<div class="yt-container">
<iframe class="yt-playlist" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22GK0wXWQDzC0K6xu0fcNlpZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

---

### Image gallery:

{{< gallery resize_options="500x" album="india_11_kerala_varkala" >}}

---

...and with Christmas celebrated, we made our way to the airport, back to Bangalore (stayed at the Taj this time, won't make the same mistake twice ;), and caught our flight home early the next morning.

{{< figure src="varkala_10" caption="Back to reality..." >}}

---

