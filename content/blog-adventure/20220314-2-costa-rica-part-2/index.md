---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Costa Rica 2022, pt. 2"
subtitle: "Monteverde and the cloudless forest"
summary: "#2 of 3: In part 2 of our Costa Rica adventure, we head for the hills and the cloud forest (spoiler alert, no clouds!) and take a swing at ziplining."
authors: []
tags: [adventure, travel, "Costa Rica", "Latin America"]
categories: [travel, "Latin America" ]
date: 2022-03-14T17:27:44-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

gallery_item:

- album: costa-rica/part-2
  image: Costa-Rica-2022-55.jpg
  caption: Not sure what this critter was at the Hotel Belmar. A little tapir? 
- album: costa-rica/part-2
  image: Costa-Rica-2022-56.jpg
  caption: Mandeep dappled in sunlight once we finally made it to Monteverde
- album: costa-rica/part-2
  image: Costa-Rica-2022-57.jpg
  caption: The Belmar gardens
- album: costa-rica/part-2
  image: Costa-Rica-2022-58.jpg
  caption: A chileguaro, kind of like a spicy caesar. Garnishes from the garden below.
- album: costa-rica/part-2
  image: Costa-Rica-2022-59.jpg
  caption: Sun setting to the west. 
- album: costa-rica/part-2
  image: Costa-Rica-2022-60.jpg
  caption: You can actually see the Pacific from here off in the distance.
- album: costa-rica/part-2/
  image: Costa-Rica-2022-62.jpg
  caption: Mandeep on the zipline!
- album: costa-rica/part-2
  image: Costa-Rica-2022-63.jpg
  caption: Mandeep on the zipline!
- album: costa-rica/part-2
  image: Costa-Rica-2022-64.jpg
  caption: Mandeep on the zipline!
- album: costa-rica/part-2
  image: Costa-Rica-2022-65.jpg
  caption: She means business
- album: costa-rica/part-2
  image: Costa-Rica-2022-66.jpg
  caption: Racoons in CR look a little different
- album: costa-rica/part-2
  image: Costa-Rica-2022-67.jpg
  caption: So it's a coati...
- album: costa-rica/part-2
  image: Costa-Rica-2022-68.jpg
  caption: They were everywhere around the sides of the road and near garbage cans..
- album: costa-rica/part-2
  image: Costa-Rica-2022-69.jpg
  caption: Private hot tub at the Belmar
- album: costa-rica/part-2
  image: Costa-Rica-2022-70.jpg
  caption: Private hot tub at the Belmar
- album: costa-rica/part-2
  image: Costa-Rica-2022-71.jpg
  caption: Private hot tub at the Belmar
- album: costa-rica/part-2
  image: Costa-Rica-2022-72.jpg
  caption: These little guys are emerald toucans
- album: costa-rica/part-2
  image: Costa-Rica-2022-73.jpg
  caption: These little guys are emerald toucans
- album: costa-rica/part-2
  image: Costa-Rica-2022-74.jpg
  caption: These little guys are emerald toucans
- album: costa-rica/part-2
  image: Costa-Rica-2022-75.jpg
  caption: Monkey business continues over our breakfast
- album: costa-rica/part-2
  image: Costa-Rica-2022-76.jpg
  caption: Monkey business continues over our breakfast
- album: costa-rica/part-2
  image: Costa-Rica-2022-77.jpg
  caption: Monkey business continues over our breakfast
- album: costa-rica/part-2
  image: Costa-Rica-2022-78.jpg
  caption: Monkey business continues over our breakfast
- album: costa-rica/part-2
  image: Costa-Rica-2022-79.jpg
  caption: A blue crowned motmot visted us by the hot tub
- album: costa-rica/part-2
  image: Costa-Rica-2022-80.jpg
  caption: Blue crowned motmot


---

In part 2 of our Costa Rica adventure, we head for the hills and the cloud forest (spoiler alert, no clouds!) and take a swing at ziplining.


{{< cta cta_text="📸 Skip the blablabla, take me to the photos" cta_link="#-photo-gallery" cta_new_tab="false" >}}

---

{{< toc >}}

---
<br>

## 🇨🇷 Day 3: La Fortuna to Monteverde
<br>

{{< figure src="Costa-Rica-2022-47.jpg" caption="Panoramic view from our balcony in the morning">}}

Our time at the Hotel Linda Vista was mainly spent from the hot tub and pool soaking in the views. From the sunset the day before to late morning relaxation, we profited from the peace and quiet on the remote end of the lake, and it felt like vacation was truly upon us.

{{< figure src="Costa-Rica-2022-51.jpg" caption="Don't make us leave" alt="selfie in hot tub" >}}

But the main order of the day was the three hour drive to the next section of our vacation: the cloud forest, Monteverde and the crown jewel of our vacation, the Hotel Belmar. Here's the funny thing: the distance between the Hotel Linda Vista and Hotel Belmar - "hot tub to hot tub", if you will - is about 15km. The drive, however, will run you about 3 hours, as the two are separated by a formidable mountainous wild area where no roads dare to pass. The road is a patchwork of mostly asphalt with plenty of potholes and frequently punctuated by stretches of rough gravel. It runs northward up the east side of Lake Arenal, crosses over the top of the lake, then ventures into the wild county south back into the hills, and eventually into the cloud forest.

Appealing signs for the Lake Arenal Brewery caught our eye and we decided to stop off at a beautiful lodge overlooking the lake to enjoy a rare craft beer and plate of nachos. Alas, the nachos were never meant to be, as the wait staff was off their game (to put it pleasantly) and, one hour and half a warm beer later, we gave up and continued on our way. 

The roads continued to get steeper and our plucky little Chevy Beep or whatever cute model our rental car was kept grinding until we reached the valhalla that is the [Hotel Belmar](http://www.hotelbelmar.net/). I'm not in the business of influencing or reviewing places, but suffice it to say this is one of the good ones, and doubtless the pinnacle of our accommodations for the trip. 

{{< figure src="Costa-Rica-2022-59.jpg" caption="Sunset view sitting on the terrasse at Hotel Belmar" alt="a photo of sunset over trees" >}}

The room was spacious and atmospheric, the restaurant featured an open air porch looking out over the hills falling away all the way out to the Gulf of Nicoya and the Pacific Ocean. They have their own brewery (TL;DR, the beer was great, but the actual little brasserie was a bit insufferable with "way too loud acoustic guitar reggae guy" providing live music for happy hour and lots of american tourists just getting their vibe on... this is a common theme throughout our travels here. though), however we chose the quieter view from the restaurant terasse to have two incredible coctails, garnished with fruits and vegetables from their own gardens. Before long we were in the private hot tub feeling well accomplished with our transition day. 

{{< figure src="Costa-Rica-2022-56.jpg" >}}

{{< figure src="Costa-Rica-2022-57.jpg" caption="Coctails at Hotel Belmar with stuff from the garden below" >}}

Our night culminated with a short walk to the town centre of Monteverde for dinner. Being a cloud forest and well up in elevation, the air was cool, and we were happy to find a warm and inviting Italian restaurant with a table next to an open fireplace. A massive pizza followed, along with a lightly strange veggie burger. It seems an actual veggie burger *patty* is not a common occurrence in Costa Rica. Thus, the veggie burger was full of flare: big bun, lettuce, tomato, pickle, mushrooms, onions, sauces... and a grilled pineapple slice. While this was, regrettably, a let down, the rest of the meal and the ambiance was top notch and the evening was a win. 

<br>

## 🇨🇷 Day 4: Monteverde, or, that day we ziplined

<br>

We found a place for breakfast: [Stella's Café](https://stellasmonteverde.com/en/): a beautiful place with very good food, but the clincher is the back terrasse on which we sit is frequented by white-faced monkeys. Most recognizable as the monkey of choice for organ grinders across the world and in many an old movie, these guys, unlike the elusive howler monkeys, are the jokesters, ready to say hello, make a scene, steal your breakfast, and generally entertain (or harass, depending on your mood). And so, after much gawking and laughter, we were off to the activity of the day.

{{< video src="Costa-Rica-2022-Stellas-2.mp4" controls="yes" >}}

Neither Mandeep nor I have ever ziplined, nor even gave it much thought as a viable enjoyable activity. And yet, hours later, there we were zooming across the canopy in a sling connected to a little wire by a big carabiner. How did we get here? Well, suffice it to say Costa Rica is *the place* for ziplining, and after hearing not one, but two enthusiastic recommendations about *how incredibly awesome this one ziplining place is, where you can **"superman"** the longest (1km long - ok that is pretty incredible) line* we said, ok what the hell let's give it a try. 

{{< figure src="Costa-Rica-2022-64.jpg" caption="Mandeep coming in hot on zipper #13" >}}

And that's what happened. I think it hit us both about how we expected. Fun? Sure. Scary? Not really so much, the lines and harnesses, etc. were all nice and secure. But more so than that, especially at the greater heights, perception the height itself is made less palpable, and the trees below just looks like miniatures, and don't elicit the stomach-turning dread of extreme heights. In the end, we enjoyed the activity, but agreed we probably didn't need to ever to it again. 

The rest of the day was leisurely, enjoyed at the Hotel Belmar. The stunning jungle hot tub, reserved for only us for an hour, and then a romantic dinner at the Belmar restaurant out on the balcony, looking off into the darkened hills below and surrounded by birds, frogs and a giant patio heater to keep us warm in the breezy elevation. Meal highlights included a lovely bottle of Chilean red and a cheese plate consisting of chesses and vegetables from the Belmar farm down the road, as well as an exquisite cocktail *se llama "Sante, Dinero y Amor"* (Health, Money and Love). Feeling successful, and continuing a lasting trend throughout this vacation, we were exhausted and well on our way to bed by 9pm. 

Welcome to vacationing in your 40s. 

## 🇨🇷 Day 5: Going coastal

Our fifth day of the trip marked a major turning point, where we left the hills and headed for the beach. Before we said goodbye to Monteverde we revisited our greatest hits. It started with Stella's for breakfast, this time opting for the daring open air "garden" table. Hilarity once again ensued as cute-ass monkeys surrounded us and continued with their monkey antics. 

#### 💩 Going apeshit

Of course, mess too much with the monkeys and and you'll surely get monkeyed with. And so it was, the first half of our breakfast punctuated by excited videography - *look! a monkey sneaking up from the back side! stealing someone's pancake! ooh, a baby monkey on mama's back!* - and so on. We munched on our food staring up into the trees at action overhead when a wet splat landed next to me, along with... spray. And thus, while we didn't fully get shit *on* by a monkey, we still got the full effects. even though the damage was largely limited to my pants and a few splatters here and there, our food was in the splash zone and we took no chances. Stella's was kind enough to remake our meals, which we ate hastily, and under a roof. All the sudden those monkeys weren't looking so cute after all. 

We enjoyed another soak in the private jungle hot tub and began the long descent to the coast. Next destination, Manuel Antonio National Park, jutting out into the Pacific Ocean. This time there were no coy attempts to snag a quick lunch on the way; our lesson was learned from our previous drive. A largely uneventful 4 hour drive brought us back to sea level, and the lush forest canopy gave way to palm tree groves and our first sightings of the sea. The temperature rose too - after the breezy and chilly cloud forest, we welcomed the warm, moist air on our faces. 

## ---

<br>

## 📸 Photo gallery: 

### Monteverde and the cloudless forest

{{< gallery resize_options="500x" album="costa-rica/part-2" >}}

-----

