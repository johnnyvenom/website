---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Toy Train: Siliguri - Darjeeling"
subtitle: "India #7"
summary: "#7 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-16"
lastmod: "2017-12-16"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: 6. New Delhi]({{< relref "../2017-12-15-new-delhi-agra-taj-mahal-india-6" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 8. Darjeeling ➡️]( {{< relref "../2017-12-19-darjeeling-india-8" >}})**

> **Post #7 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

Right then. So, maybe a few months have passed since I put up the first half of India adventures. In the meantime, a lot of life has happened, and more travel, work, etc. Time flies, eh? So before I jump into more adventures (and more are in progress, and just around the corner...), let's pick up where we left off, with Mandeep and I boarding a plane from New Delhi, headed for the mountains...

{{< gallery resize_options="500x" album="india_7_toy_train_1" >}}

---


We fly from New Delhi to Bagdogra. On the way we spot Mt. Everest out the window. We taxi it to Siliguri, a dirty and scrappy, but oddly charming transport hub city that lies at the entrance to the Himalayas in West Bengal. We stay in an appropriately scrappy hotel, where we are given a “honeymoon suite” of sorts, complete with a round bed. Our waiter in the hotel restaurant is quite excited with our presence and asks for a selfie after we eat. We wander the streets, argue with nut vendors, and turn in, savouring the incessant barking of the dogs in the alley and acrid smoke of burning trash that hangs in the air.

{{< gallery resize_options="500x" album="india_7_toy_train_2" >}}
<!-- \[gallery type="rectangular" link="file" ids="695,696,697,698,700"\] -->

We awake to a glorious sunny day, and make our way to the train station to board the Darjeeling toy train, a rickety old narrow gauge train line that will take us 8 hours up in the mountains to famed Darjeeling. (Disclaimer — we could have opted for a 3.5 hour taxi ride to take us the same distance, but what would be the fun of that?) The train pulls up an hour late, maybe more, and the hitch between the engine and passenger car is borked. The men gather around and a mechanic in smart custard button down and lime green sweater vest does a quick fix on the spot. Amazing. And then we're off.

---

{{< youtube FWfAm62QBuI >}}
<br>
{{< youtube p3AFBMLmszI >}}

---

{{< gallery resize_options="500x" album="india_7_toy_train_3" >}}


Before we reach the next stop, a motorbike passes with the mechanic on the back of a motorbike with a shiny new part for the train. Sure enough, we pull in to the next stop, and he installs the new part, ensuring our safety that the car we are traveling in won’t break free and plunge backwards down the mountain. The next 8 hours are a hypnotic melange of train horn, screeching metal on metal, jaw-dropping vistas, Scrabble games, and more. We paused midway at Tindharia, to wait for the descending train to pass us on its way down. In Kuresong, we stop again and investigate a small museum at the train station dedicated to the wonder and improbability of this incredible train, which climbs 2,100 m over 88km (check links at bottom of post to see how cool this thing is). Finally, as night falls, we arrive in Darjeeling and roll our bags up the hill to our awaiting hotel.

---

<div class="yt-container">
<iframe class="yt-playlist" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22HshtHABuYKuCzEo4qQAQS7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

- Video playlist: 
  1. "Rollin'"
  2. "Navigating one of the many reverses on the way"
  3. "Passing through Kuresong"
  4. "View from the hotel window in Darjeeling"

---

I do like me a good train (more on this a bit later…) and this was an absolutely incredible experience. The train, the company, warm and clean air (after New Delhi!), and the fact that we were traveling into the Himalayas, easily made this one of the best days of the trip - though there were many!! - and one of the best days of my life.

Truly, the images here hardly do the scene justice...

---

{{< gallery resize_options="500x" album="india_7_toy_train_4" >}}

---

#### Read more about the toy train:

  - [Darjeeling Himalayan Railway](https://en.wikipedia.org/wiki/Darjeeling_Himalayan_Railway)
  - [Darjeeling tourism](https://www.darjeeling-tourism.com/darj_000089.htm)

---



