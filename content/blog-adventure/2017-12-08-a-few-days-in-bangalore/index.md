---
title: "A few days in Bangalore"
date: "2017-12-08"
categories: 
  - "travel"
subtitle: "India #1"
summary: "#1 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure."
authors: []
tags:
  - "adventure"
  - "travel"
  - "india"
lastmod: "2017-12-08"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: "Exploring a market in Bangalore"
  focal_point: ""
  preview_only: false
  
---

**[⬅️ Previous: 0. Colors]({{< relref "../2017-12-02-colors" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 2. Bangalore, Pt. 2 ➡️]( {{< relref "../2017-12-08-a-few-days-in-bangalore-pt-ii" >}})**

> **Post #1 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

So here we are in Bangalore, and here we are with a proper post finally.

Undoubtedly the content pipeline will improve, but for now here is a bunch of images from the first few days in India. The reason for this bloggish format is my gradual cessation from social media and general mistrust - and perhaps latent paranoia - of corporate ownership of personal information. Thus, I've scrapped my Facebook account and only rarely post to IG, Twitter, etc. Instead, I present this website, with (hopefully) regularly updated content.

Anyways, Mandeep and I are in India for a month. This week we are in Bangalore, where she is doing UX research in the field for her company Shopify, which has an office here. I am still very much in work mode, despite the travel, as I continue with my doctoral research and tackle a few side projects over the break. On Saturday we'll head north for other assorted adventures throughout India before returning to Montreal to ring in the New Year.

Here are some sights from the first couple days on the ground.. More to follow soon. Thanks for looking - I encourage comments, and feel free to contact me by email, WhatsApp, Signal, iMessage, carrier pigeon, phone, etc., etc....

{{< youtube zFCVvRl-ick >}}

### Image gallery

{{< gallery resize_options="500x" album="india_1_bangalore_1" >}}

---

