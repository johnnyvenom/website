---
title: "Springer Series on Touch and Haptic Systems: Musical Haptics"
date: "2018-05-11"
categories: 
  - "music-technology"
  - "research"
coverImage: "Musical-Haptics-featured.jpg"
draft: true
---

I'm happy to announce that the volume Springer Series on Touch and Haptic Systems: Musical Haptics, containing a chapter co-authored by myself, Marcello Giordano and Marcelo M. Wanderley, is now available for free download [here](https://link.springer.com/book/10.1007/978-3-319-58316-7).

The book provides an overview of the haptic modality in music performance and perception, and presents research in theories and applications of haptic and multimodal interaction in the design and use of digital musical instruments. The book, edited by Stefano Papetti and Charalampos Saitis, contains contributions from many of the leading researchers in the field of touch and haptic systems for musical applications. Suffice it to say, it is an honor to be included in this work, and I would like to extend a big thank you to everyone involved in the project.

**Chapter abstract:**

> Haptics, and specifically vibrotactile-augmented interfaces, have been the object of much research in the music technology domain: In the last few decades, many musical haptic interfaces have been designed and used to teach, perform, and compose music. The investigation of the design of meaningful ways to convey musical information via the sense of touch is a paramount step toward achieving truly transparent haptic-augmented interfaces for music performance and practice, and in this chapter we present our recent work in this context.We start by defining a model for haptic-augmented interfaces for music, and a taxonomy of vibrotactile feedback and stimulation, which we use to categorize a brief literature review on the topic.We then present the design and evaluation of a haptic language of cues in the form of tactile icons delivered via vibrotactile-equipped wearable garments. This language constitutes the base of a “wearable score” used in music performance and practice. We provide design guidelines for our tactile icons and user-based evaluations to assess their effectiveness in delivering musical information and report on the system’s implementation in a live musical performance.

**About the book:**

> This Open Access book offers an original interdisciplinary overview of the role of haptic feedback in musical interaction. Divided into two parts, part I examines the tactile aspects of music performance and perception, discussing how they affect user experience and performance in terms of usability, functionality and perceived quality of musical instruments. Part II presents engineering, computational, and design approaches and guidelines that have been applied to render and exploit haptic feedback in digital musical interfaces.
> 
> _Musical Haptics_  introduces an emerging field that brings together engineering, human-computer interaction, applied psychology, musical aesthetics, and music performance. The latter, defined as the complex system of sensory-motor interactions between musicians and their instruments, presents a well-defined framework in which to study basic psychophysical, perceptual, and biomechanical aspects of touch, all of which will inform the design of haptic musical interfaces. Tactile and proprioceptive cues enable embodied interaction and inform sophisticated control strategies that allow skilled musicians to achieve high performance and expressivity. The use of haptic feedback in digital musical interfaces is expected to enhance user experience and performance, improve accessibility for disabled persons, and provide an effective means for musical tuition and guidance.
