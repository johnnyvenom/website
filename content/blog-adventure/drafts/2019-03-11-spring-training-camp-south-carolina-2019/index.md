---
title: "Spring Training Camp, South Carolina 2019"
date: "2019-03-11"
categories: 
  - "cycling"
  - "travel"
coverImage: "MCT-SC-2019-cover.jpg"
draft: true
---

Finally outdoors after a long winter of indoor training - it's the McGill Cycling Team Spring Training Camp! More fun, more kms, more pain...

## Lots o' photos

\[rl\_gallery id="1564"\]
