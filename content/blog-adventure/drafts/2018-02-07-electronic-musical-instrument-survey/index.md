---
title: "Electronic Musical Instrument Survey"
date: "2018-02-07"
categories: 
  - "music-technology"
  - "research"
coverImage: "survey-call-slider-title-RED.jpg"
draft: true
---

The Electronic Musical Instrument survey is now closed. Thanks to all who participated!

If you would like to learn more about this and other related research I am working on, please check these resources:

- [Input Devices and Music Interaction Laboratory](http://idmil.org/)
- [Centre for Interdisciplinary Research in Music Media and Technology](http://www.cirmmt.org/)
- [Augmented Instruments Lab, Queen Mary University (London)](http://www.eecs.qmul.ac.uk/~andrewm/)
- [McGill University, Music Technology Program](https://mt.music.mcgill.ca/)
- [New Interfaces for Musical Expression](http://nime.org)

\- Johnny

* * *

Hello! If you are an active musician who performs any style of music, I would like to invite you to take an online survey at:

## [**emisurvey.online**](https://emisurvey.online)

 

This survey is part of my ongoing PhD research on the design of new instruments, tools and techniques for music performance at McGill University. It is open to all musicians who perform, regardless of instrument or experience with technology.

By taking part in the survey, you can enter to win a $100 CAD gift certificate to an online music retailer like Moog Audio or Sweetwater.

**You must be 18 years of age or older to participate.**

This project is supervised by Dr. Marcelo Wanderley, Dept. of Music Research, McGill University, and has been certified by the review ethics board.

For complete information please visit the survey website at the URL provided above.
