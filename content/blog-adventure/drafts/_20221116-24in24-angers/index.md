---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "24 in 24: 🎭 3. Angers, FR"
subtitle: "" # shown on post page only
summary: "" # shown on aggregate pages 
authors: []
tags: []
categories: []
date: 2022-11-16T14:35:43+02:00
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

# gallery captions
# run: python captions.py <gallery_folder> <output_folder>
# to generate text file for gallery image captions
---

<!-- content goes here -->

---

## 📸 Photo Gallery


---

