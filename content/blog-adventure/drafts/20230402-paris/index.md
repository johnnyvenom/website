---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Paris / ex)situ"
subtitle: "" # shown on post page only
summary: "" # shown on aggregate pages 
authors: []
tags: []
categories: []
date: 2023-04-02T12:33:16+02:00
lastmod: 2023-04-02T12:33:16+02:00
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 1
  caption: Photo by <a href="https://unsplash.com/@allewollenalex?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Alexander Kagan</a> on <a href="https://unsplash.com/images/travel/paris?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Well then, consider this a long overdue update. As this is my (mostly) serious/professional site, here's my career life news these days. 

I [finished my PhD]({{< relref "/blog/20210314-phd-defence" >}}) in spring of 2021, delivering my defense COVID style via Zoom. For the next year, I 

-----

