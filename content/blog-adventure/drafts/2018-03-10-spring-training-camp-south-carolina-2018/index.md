---
title: "Spring Training Camp, South Carolina 2018"
date: "2018-03-10"
categories: 
  - "cycling"
  - "travel"
coverImage: "MCT-SC-2018-1-of-19-e1587434102381.jpg"
draft: true
---

I've ridden bikes pretty much my whole life. When I was young I moved lawns to earn money to buy my first mountain bike, and was a trail rider growing up. Eventually my fascination for 2 wheels morphed to motorcycles and I spent several years in New Mexico enjoying the brrrrapp life. This continued when I returned to Maine in the northeast US (where I grew up), however the lack of a long riding season combined with a relatively serious (but NOT motorcycle-related) accident cooled my enthusiasm for motorized varieties of 2-wheeled fun.

Enter road cycling, first single speed, then fixie riding, in Maine, then NYC where I lived part time for a couple years. A cycling vacation in Croatia in 2016 sealed the deal and in 2017 I bought a proper road bike and joined the McGill Cycling Team.

https://www.instagram.com/p/Bd0-IZLllHU/

2017 was a year of cycling milestones that seemed distant and impossible, but incredible to reach and surpass. Improved physical and mental health followed, and new friendships and strange tan lines formed. A long winter of indoor training came and went and in 2018 the team drove to Salem, South Carolina for the annual week-long spring training camp.

In a nutshell: 20ish student-athletes rent several cars and drive 18 hours overnight from Montreal to South Carolina (longer when we drive through a blizzard, which is basically guaranteed each year). We arrive at a large house on a lake, and each day is the same: get up early, eat everything, assemble in appropriate riding groups and ride the Smokey Mountains in and around the confluence of South Carolina, North Carolina and Georgia. 8 days later, after hundreds of kms in the saddle and many thousand meters of elevation climbed, we clean the house, pack our bikes back up and drive 18 hours back to Montreal.

## Photo Gallery

\[rl\_gallery id="1559"\]

## Rides:

\[activities date\_start="2018-03-02" date\_end="2018-03-11"\]
