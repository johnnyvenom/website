---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Greetings from DIS '23"
subtitle: "" # shown on post page only
summary: "An update from the 2023 ACM Designing Interactive Systems conference in Pittsburgh, USA" # shown on aggregate pages 
authors: []
tags: [DIS, conference]
categories: [dance, conference, HCI]
date: 2023-07-14T12:05:12-04:00
lastmod: 2023-07-14T12:05:12-04:00
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

<!-- content goes here -->
Hello from Carnegie Mellon University! 



-----

