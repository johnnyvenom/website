---
title: "Musical Instrument Sale"
date: "2019-04-29"
coverImage: "cover.jpg"
draft: true
---

Hey folks, so over 20ish years of musicking I've acquired a lot of gear that isn't getting much use these days. So, it's time to thin the herd, as it were. Here is a bunch of stuff I'm putting up for sale. Links to more images and info are provided below, and all of these items are (or will be) also posted on kijiji.ca. If you are interested in anything drop me an email: johnny \[at\] johnnyvenom \[dot\] com. Cheers!

- **SOLD!** Nord Electro3 - 73 keyboard w/ flight case: **$2000** \[[images/info](https://adobe.ly/2V06Met)\] \[[Kijiji](https://www.kijiji.ca/v-view-details.html?adId=1430456978)\]
- **SOLD!** Ibanez Artstar AS120 electric guitar: **$800** \[[images/info](https://adobe.ly/2GLGJO6)\] \[[Kijiji](https://www.kijiji.ca/v-view-details.html?adId=1431237620)\]
- **SOLD!** Ableton Push 2 controller: **$700** \[[images/info](https://adobe.ly/2GJ4TsA)\] \[[Kijiji](https://www.kijiji.ca/v-view-details.html?adId=1430476210)\]
- **SOLD!** Ibanez TS9 Tube Screamer w/ Analogman Classic mod: **$150** \[[images/info](https://adobe.ly/2GE2fUX)\]
- **SOLD!** Line6 DL4 Digital Delay/Looper: **$150** \[[images/info](https://adobe.ly/2GJH8AP)\]
- **SOLD!** Ibanez SC10 Stereo Chorus: **$100** \[[images/info](https://adobe.ly/2GE2cbJ)\]
- Jim Dunlop MXR phase 90: **$100** \[[images/info](https://adobe.ly/2V1n2vY)\]
- **SOLD!** DOD FX22 Vibro Thang: **$60** \[[images/info](https://adobe.ly/2V1Bq7r)\]
- **SOLD!** SKB PS25 pedalboard w/ OneSpot power supply: **$50** \[[images/info](https://adobe.ly/2UYxwMm)\]

and while we're at it:

- **SOLD!** iPhone 6S 64GB (unlocked, with new battery): **$250** \[[images/info](https://adobe.ly/2V345Jg)\] \[[Kijiji](https://www.kijiji.ca/v-view-details.html?adId=1431236737)\]

Everything is in good working order; most of it in excellent condition. All prices are in CAD. Pick up locally in Montreal, QC, or can ship if buyer pays shipping fees.

\[gallery order="DESC" type="rectangular" ids="1065,1066,1059,1060,1061,1064,1063,1062,1072,1069,1068,1071,1070,1067"\]

@media (min-width: 1024px) {<br /> .single-blog-post-media {<br /> margin: auto;<br /> width: 50%;<br /> }<br /> }<br />
