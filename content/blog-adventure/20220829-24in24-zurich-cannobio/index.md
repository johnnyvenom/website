---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "24-in-24: 🇨🇭🇮🇹 Zurich and the Alps"
subtitle: "Entry #0.3 in the series Paris 24 in 24. August 2022." # shown on post page only
summary: "Entry #0.3 of 24(ish) adventures while my partner and I live in Paris for 24 months. In the final prequel chapter I visit my friend in Zurich for a cycling trip in northern Italy. " # shown on aggregate pages 
authors: []
tags: [24in24, cycling, Italy, Switzerland, Zurich, Europe]
categories: [travel, cycling]
date: 2022-08-29T11:27:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false
  # alt: "Picture of a boardwalk bordering a lake. On the right, Multicolored Italian buildings and restaurant terrasses filled with people line the promenade, and on the left is the lake. A small marina with boats borders the lake, and mountains can be seen at the far end of the lake stretching into the distance."

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

# gallery captions
# run: python captions.py <gallery_folder> <output_folder>
# to generate text file for gallery image captions
gallery_item:
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220824-003.jpg
  caption: Downtown Zurich water is very clean. 
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220824-004.jpg
  caption: Zurich is pretty swanky, dont'cha think? 🦢
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220824-005.jpg
  caption: Waiting for Felix...
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220824-006.jpg
  caption: It's Felix! (Sunset ride outside of Zurich)
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220824-007.jpg
  caption: Sunset ride out of Zurich
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220824-008.jpg
  caption: Big decisions at the beer store
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-009.jpg
  caption: Out and about on the bikes around Zurich
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-010.jpg
  caption: Dinner on the terrasse
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-011.jpg
  caption: Downtown Zurich walk at night
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-012.jpg
  caption: Downtown Zurich walk at night
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-013.jpg
  caption: Downtown Zurich walk at night
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-014.jpg
  caption: Downtown Zurich walk at night
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-015.jpg
  caption: Downtown Zurich walk at night
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220825-016.jpg
  caption: Downtown Zurich walk at night
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220826-017.jpg
  caption: Another Zurich evening, another dinner on the terrasse
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-018.jpg
  caption: Felix's cool modern Swiss abode
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-019.jpg
  caption: Our B&B in Cannobio, northern Italy
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-020.jpg
  caption: Streets of Cannobio
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-021.jpg
  caption: Cannobio waterfront
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-022.jpg
  caption: Cannobio waterfront
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-023.jpg
  caption: Cycling Italy - hillside village outside of Cannobio
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-024.jpg
  caption: Cycling Italy - view back towards Cannobio
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-025.jpg
  caption: Moar views
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220827-026.jpg
  caption: A pause on the ride
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-027.jpg
  caption: Descending into Cannobio
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-028.jpg
  caption: Cycling Italy
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-029.jpg
  caption: Cycling Italy
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-030.jpg
  caption: This is where we were in the previous pictures!
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-031.jpg
  caption: Another hillside village
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-032.jpg
  caption: Another hillside village
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-033.jpg
  caption: Another hillside village
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-034.jpg
  caption: Cycling Italy
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-035.jpg
  caption: Cycling Italy
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-036.jpg
  caption: Cycling Italy
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220828-037.jpg
  caption: Post-ride rewards back in Cannobio
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220829-038.jpg
  caption: Packed up and leaving Cannobio, with a tear in our eye
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220829-039.jpg
  caption: Fabulous views driving across the Alps back to Switzerland
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220829-040.jpg
  caption: Fabulous views driving across the Alps back to Switzerland
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220829-041.jpg
  caption: Fabulous views driving across the Alps back to Switzerland
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-20220829-042.jpg
  caption: And finally, on the train back to Paris. 
- album: 24in24/20220829-cannobio
  image: 202208-Swiss-Italy-featured-20220827-001.jpg
  caption: The stunning Cannobio waterfront boardwalk. 

---

> *In the spring of 2022, I accepted a post-doctoral fellowship at the University of Paris-Saclay that would last for two years, from September 2022 until August 2024. My partner Mandeep and I moved to France in August to set up shop before I would get to work in September.*

<br />

<div class="btn-row">
  {{< cta cta_text="👈 Previous<br/>🏔️ Chamonix-Mont Blanc" cta_link="/adventures/2022/08/15/24-in-24-chamonix-mont-blanc/" cta_new_tab="false" >}}
  {{< cta cta_text="See the whole series!<br/>🇪🇺 24 in 24" cta_link="/tag/24-in-24" cta_new_tab="false" >}}
  {{< cta cta_text="Next 👉<br/>coming soon..." cta_link="#" cta_link="/" cta_new_tab="false" >}}
</div>

The month of August 2022 would hold one more final trip. While my partner Mandeep returned to Canada for a few days, I got back on the train and headed to Switzerland to see my friend Felix in Zurich. I've known Felix for the last few years since I met him on the [McGill Cycling Team](https://mcgillcycling.com/) during my PhD in Montreal. Felix was also completing his PhD and is an avid cyclist. Since our McGill days, Felix moved from Montreal to Zurich, and I to Paris, so I wanted to take the opportunity to pay a visit to my old friend and to do what we both love to do best, ride our bikes. We'd spend the first few days around Zurich and then take a long weekend to drive south across the Alps into Italy to do some rides there.

{{< figure src="202208-Swiss-Italy-20220824-001.jpg" caption="The bike(s)/luggage shuffle was becoming a part of life at this point. Here I'm dropping some of my and Mandeep's luggage at a storage facility by the train station before my bike and I heading to Zurich." >}}

## Part 1: Zurich

I took the train direct from Paris to Zurich. Not much to report, once the bike was on and stowed, which is always an adventure in and of itself, as there is never really a proper storage spot that can fit a bike bag. 

{{< figure src="202208-Swiss-Italy-20220824-002.jpg" alt="Picture of a folded down tray at a seat on a train. On the tray is a bag of roast chicken flavored potato chips (Lays 'Saveur poulet rôti' flavor), a half-eaten vegetarian sandwich in cellophane, paper cup of red wine, and a napkin." caption="Train snax (don't worry, the *saveur poulet rôti* chips are all veg, no 🐓 harmed...)" >}}

A Monop' meal from Gare de Lyon and some wine passed the time until I arrived at Zurich Central Station. Felix was driving back into town from a work event so I stretched my legs on the banks of the clear water that flows through the center of the city. 

No sooner did he arrive than we got out the bikes and headed outside of the city to stretch our legs in the setting sun. 

{{< figure src="202208-Swiss-Italy-20220824-006.jpg" caption="Cycling outside of Zurich." >}}

We spent a couple days around Zurich; Felix showed me some of the local rides, which were stunning and not for the faint of heart. All roads go up at some point. We bought and cooked good food, and walked around the old city at night. 

## Part 2: Cannobio

The real fun came when we backed the bikes up in Felix's car and headed south across the Alps to northern Italy, to the village of Cannobio. Just a few kilometers south of the Swiss border, Cannobio sits on [Lago Maggiore](https://maps.app.goo.gl/BmkcSMgfEgVASE9u6), with mountains rising up on all sides. A great B&B awaited us there, and shortly after we were off on our bike again, heading up the valley into roads unknown. The pictures can do the talking (see below), but to say just a couple words, everything was simply splendid. Like a postcard. There was one main valley road, off of which were many different tiny side roads, winding and switchbacking their way up to impressive vistas and improbable villages that look like they've been there for many centuries. 

{{< figure src="202208-Swiss-Italy-20220828-030.jpg" caption="A typical village that we would ride up to, and be rewarded with medieval architecture and stunning vistas.<br />_**Below:** From the village looking down._" >}}

{{< video src="videos/20220827_180710_2.mp4" controls="yes">}}
<p class="video-caption">Sending the report from yet another hillside village.</p>

The waterfront of Cannobio was impressive as well, a proper mix of classic Italian charm, pizza and Spritzes, not to mention a brisk dip in the lake. 

After a long weekend of daily riding and enjoying some great Italian hospitality, we headed back across the Alps to Zurich (a stunning site on it's own!) and I boarded the train back to Paris to rejoin Mandeep who was returning from Canada at the same time. 

What more to say? Cycling northern Italy in the foothills of the Alps? 5 of 5 stars / highly recommend / not enough great things to say! Added to that, hanging with Felix. Best times. 

---

## 📸 Photo Gallery

{{< gallery resize_options="500x" album="24in24/20220829-cannobio" >}}

## 📽️ Videos

{{< video src="videos/202208-Swiss-Italy-20220825-002.mp4" controls="yes" >}}
<p class="video-caption">Walking around Zurich at night</p>

{{< video src="videos/202208-Swiss-Italy-20220825-001.mp4" controls="yes" >}}
<p class="video-caption">Cycling outside of Zurich</p>

{{< video src="videos/202208-Swiss-Italy-20220827-003.mp4" controls="yes" >}}
<p class="video-caption">Sketchy 1-handed riding through narrow alleys in hilltop Italian villas</p>

{{< video src="videos/202208-Swiss-Italy-20220828-004.mp4" controls="yes" >}}
{{< video src="videos/202208-Swiss-Italy-20220828-005.mp4" controls="yes" >}}
<p class="video-caption">Enjoying the views in the hills surrounding Cannobio, parts 1 and 2</p>

---



