---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Kerala: The backwaters"
subtitle: "India #10"
summary: "#10 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-30"
lastmod: "2017-12-30"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: 9. Kochi ]({{< relref "../2017-12-21-kerala-kochi-india-9" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 11. Varkala ➡️]( {{< relref "../2018-01-01-kerala-varkala-india-11" >}})**

> **Post #10 of 11 in a series:** India 2017 continued... 
> 
> Review: In December of 2017 my partner (now [FIANCÉE]({{< relref "../2019-05-28-engaged-in-mexico" >}})!!!) and I sent a month traipsing around the subcontinent. A bit of work, a bit of family, and rounded it out with a bit of Christmas sunshine in Kerala. In this chapter: the backwaters.

{{< figure src="kumarakom_20.jpg" caption="Kerala backwaters" >}}
<br>

We left [Kochi]({{< relref "../2017-12-21-kerala-kochi-india-9">}}) and headed to Kumarakom, far from the beach and warm ocean swells, to the back reaches of the Kerala backwaters, "a network of brackish lagoons and lakes lying parallel to the Arabian Sea coast of Kerala state in southern India, as well as interconnected canals, rivers, and inlets, a labyrinthine system formed by more than 900 kilometres of waterways." We arrived there a couple days before Christmas in our swing back south to warm weather and eventually Bangalore, where we would catch a flight home on New Years Eve eve. The typical vacation activity there is to rent a [kettuvallam](https://en.wikipedia.org/wiki/Kettuvallam) (a houseboat, see videos below, they are the funny floating armadillo-looking things on the water) and cruise around Vembanad Lake. Instead we opted to head to the quiet end and stay at a small hotel overlooking the lake.

--- 

{{< gallery resize_options="500x" album="india_10_kumarakom_1" >}}

---

Everything was quiet except the birds and the sound of water. Time passed. We hired small boats to float us around. We walked to a bird sanctuary and listened to the sounds of even more birds and watched an 8' monitor lizard do its thing. We got dropped off at the Taj backwaters resort on Christmas eve for cocktails but didn't stay for dinner. (Spoiler alert: we thought we could go to another lovely resort by our hotel for dinner, but the truth was, we were in the middle of nowhere and we ended up eating leftover weird curry and rice with bugs in it at Fantastic Prawns. Mistakes were made, for sure. Everything was weird. And fantastic.) I will dream of this place often.

---

### Full image gallery (videos below!)

{{< gallery resize_options="500x" album="india_10_kumarakom_2" >}}

---

### Videos:

<div class="yt-container">
<iframe class="yt-playlist" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22FZvrkzVaEB5VwefxVsSMr4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

---


