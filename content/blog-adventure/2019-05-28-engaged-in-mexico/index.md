---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Engaged in Mexico :)"
subtitle: ""
summary: ""
tags: [ "travel", "adventure", "cycling", "Latin America"]
categories: [ "travel", "cycling", "Latin America" ]
date: "2019-05-28"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

---

Mandeep and I just got back from a great vacation in Puerto Vallarta, Mexico. My old friends John and Licia, with their 1 yr old son Cypress were there to greet us. Hikes and morning runs were had. Covered 360km on a bike (thanks @[Orucase](https://www.orucase.com/)!) including an overnight ride to Sayulita with Mandeep and a ridiculous 133km ride featuring 2600 meters of climbing in absolutely scorching heat (thanks Clarence @[PuertoVallartaCycling](https://puertovallartacycling.com/)!). Vallarta Pride was in full effect and the city of Vallarta was vibrant and energized. The food was top notch, and beer and tequila flowed.

But the absolute highlight was Mandeep's birthday, celebrated with a day long boat ride, multiple snorkeling stops, an afternoon at the remote town of Yelapa, accessible only by water, a waterfall hike, and at the end... A MARRIAGE PROPOSAL! 

{{< figure src="engaged.jpg" >}}

Many months of planning culminated in a cheeky stop-off on the ride back home. The boat dropped us off at the mouth of a grotto, where we jumped in, snorkeled our way past coral and sea urchins, and found ourselves in a secluded cave. A ring was presented, she said yes, and we celebrated...

{{< figure src="fiancees.jpg" >}}

What an adventure, and keeps getting better every day!

---

## Pictures:

### Welcome to Vallarta

 {{< gallery resize_options="500x" album="puerto_vallarta_2019/1_welcome" >}}

---

### Hike to Colomitos

 {{< gallery resize_options="500x" album="puerto_vallarta_2019/2_colomitos" >}}

---

### Bike trip to Sayulita

#### 70km out, overnight at beach hotel, 70km back to Puerto Vallarta the next day

{{< gallery resize_options="500x" album="puerto_vallarta_2019/3_cycling_sayulita" >}}

---

### Mandeep's birthday boat day...

#### ...FEATURING SURPRISE PROPOSAL IN A CAVE!!!

{{< gallery resize_options="500x" album="puerto_vallarta_2019/5_boat_day" >}}

---

### Solo morning rides

#### to Boca de Tomatlan and Jardin Botánico

{{< gallery resize_options="500x" album="puerto_vallarta_2019/6_solo_morning_rides" >}}

---

### Day spa!

#### Vallarta Pride week with unicorn

{{< gallery resize_options="500x" album="puerto_vallarta_2019/4_day_spa" >}}

---
 
### Climbing to the sun

#### An epic 133km ride to Mayto with 2600 meters of climbing in scorching heat

{{< gallery resize_options="500x" album="puerto_vallarta_2019/7_cycling_mayto" >}}

---


