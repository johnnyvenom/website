---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "24-in-24: 🪂 Back to Chamonix"
subtitle: "Another entry in the series Paris 24 in 24. March 2024." # shown on post page only
summary: "Another of 24(ish) adventures while my partner and I live in Paris for 24 months. We return to Chamonix just in time for the pre-spring sun." # shown on aggregate pages 
authors: []
tags: [24in24, France, Europe]
categories: [travel]
date: 2024-03-30T19:40:56+01:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

# gallery captions
# run: python captions.py <gallery_folder> <output_folder>
# to generate text file for gallery image captions

gallery_item:
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0001.jpg
  caption: Early arrival at Gare du Lyon in Paris bound for Geneva, Switzerland.
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0002.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0003.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0004.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0005.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0006.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0007.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0008.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0009.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0010.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0011.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0012.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0013.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0014.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0016.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0017.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0018.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0019.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0020.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0022.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0023.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0024.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0025.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0026.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0027.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0028.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0029.jpg
  caption: 
- album: 24in24/20240313-chamonix-pt3
  image: 2024_Chamonix_0030.jpg
  caption: 


---

> *In the spring of 2022, I accepted a post-doctoral fellowship at the University of Paris-Saclay that would last for two years, from September 2022 until August 2024. My partner Mandeep and I moved to France in August to set up shop before I would get to work in September.*

<br />

<div class="btn-row">
  {{< cta cta_text="👈 Previous:<br/>🇨🇭🇮🇹 Swiss/Italy" cta_link="/adventures/2022/08/29/24-in-24-zurich-and-the-alps/" cta_new_tab="false" >}}
  {{< cta cta_text="See the whole series!<br/>🇪🇺 24 in 24" cta_link="/tag/24-in-24" cta_new_tab="false" >}}
  {{< cta cta_text="Next 👉<br/>/adventures/2024/07/14/24-in-24-cycling-the-pyrenees/" cta_link="#" cta_link="/adventures" cta_new_tab="false" >}}
</div>

---

My wife and I have been living in France for about a year and a half now, and somehow we found ourselves back in Chamonix-Mont Blanc for the 3rd time already. Is it the magic of the mountains? The cheese? Just circumstance? Hard to tell, but just look at those mountains and tell me you wouldn't want to be there right now. 

{{< figure src="2024_Chamonix_Alps.jpg">}}

The truth is, it wasn't our idea. We were just along for the ride for my brother in-law's 50th birthday ski holiday extravaganza. He and my sister's whole family are fanatic skiers and have racked up a respectable collection of vacations to various North American skiing destinations. But given the magnitude of a *quinquagenariary* celebration, combined with their daughters' 21st and 18th birthdays falling in the last few months, (and my sister's own 49th for good measure) they decided to head across the Atlantic for a taste of the Alps. In fact, this was on the heels of a summer visit they made here as well, when we took a sibling/family holiday for some outdoor fun. (I'm very behind on blog posts, but there will be one... eventually...) 

{{< figure src="2023_Chamonix_teaser.jpg" caption="Chamonix summer 2023 fam jam... (📸: Daniela Trias Marza)" >}}

So, call this Chamonix, Chapter 3. 

Mandeep and I don't ski, at least I haven't in the last several years. I would be happy to get out there again, but I find myself pretty happy on a bike if it's warm enough, and on my two feet the rest of the time. I was fresh back from my [residency in Bucharest]({{< relref "/project/interactive/for-patricia" >}}) where I had been doing a good deal of running, and the Chamonix weather was decidedly spring-ish, so Mandeep and I were quite content to get out for a run, a hike, or whatever fit to get some fresh air while my sister and family shredded their way down glaciers in the back country. 

{{< figure src="2024_Chamonix_fam.jpg" caption="...9 months later (📸: Mandeep Basi)" >}}

We did the things, enjoyed great company, abundant sun and blue sky, lovely meals, cheese (fondu night, but we didn't return for raclette this time), wine, and more. And the skiers were treated to abundant fresh snow, and wore wide smiles for the duration of their time there. 

### ⛑️ The story

{{< figure src="2024_Chamonix_0021.jpg" >}}

Something did happen, that is worth telling about. On the second day of our stay, we decided to head out in the morning for a trail run. I had seen a trail of switchbacks going up under the Planpraz cable car that looked wild and challenging. So we thought we'd give it a try and try to make it to the top where we could descent back down via cable car. In total the ascent was 1000m. 

We headed out and naturally within the first few steps were gasping for breath. But we continued and got into a nice groove, and up we went. We were relatively out in the open and followed the switchbacks up and away, until at some point, we noticed that we weren't exactly under the lift any more. The trail was getting less and less defined as we went, but more and more steep. At a certain point we began encountering more snow and began to consider our options to turn around or continue to make our way. As we were already well over halfway up and the going was steep, it was not going to be easy to get back down. So, after consulting two different maps and doing my best mountaineering analysis, we pressed on towards the top. 

{{< figure src="albums/24in24/20240313-chamonix-pt3/2024_Chamonix_0012.jpg" >}}

Around this time, the air filled with paragliders above us. While we weren't exactly in any danger, we were both fully aware of our smallness in the massive and formidable mountains, and the paragliders soaring overhead gave some comfort, knowing that even if we were seemingly alone on the side of a mountain, others were nearby, having fun. 

Until. 

Our ascent was getting sketchier, as we were climbing over steep drifts of snow, angling for a route up to the apex above us where we could arrive at civilization and the top of the cable car. At this point is also where the paragliders take off from. Then, as we climbed, we looked up to see two paragliders, having just taken off, collide and become tangled in midair. In a split second they dropped from the sky and crash landed above us, just out of sight. We stared in shock and horror, not quite believing what we had just seen. How far did they fall? It was hard to tell - they had just taken off, so maybe it was a trick of the eye and they fell just a short distance, still over the gentle slope of the bank they took off from. On the other hand, we knew what we were climbing was steep, not to mention full of rocks and sheer cliffs. It could be bad. 

Just minutes later a helicopter sporting an official rescue logo on it roared up the side of the mountain straight towards us. At the point we were in the most exposed and precarious stretch of our own adventure, slowly making our way across a bridge of snow in the middle of a large basin towards the zigzag outline of switchbacks leading us to the top. Given the entirety of our concentration on our own predicament, my first thought was, "Oh dear, we been reported as two unprepared hikers trying to bushwack up the side of a mountain and they are here to ensure we don't fall to our deaths." But of course, they were coming to the site of the crash, just above our heads. 

{{< youtube VKkJ4hBXAcg >}}
<p class="video-caption" style="margin-top:0.5rem">Narrating where we were at once we made it off the snow bridge to relative safety.</p>
<br/>

A few short moments later, one of the helicopters returned to the scene, lowering a rope down and pulling one of the injured flyers up in a stretcher.

<br/>
{{< youtube w4z2Bl-dcLQ >}}
<br/>
<br/>

We continued up the switchbacks until we came around a corner and came face to face with the accident scene. Several rescue workers were giving what looked like life-saving care to a man who looked to have suffered a severe injury to the head. We moved quickly past, escorted by on the narrow trail by one of the rescue workers, and continued up to the top of the trail. We emerged into sunshine and civilization, at the top of a popular cable car, a ski station, with outdoor restaurants, skiers everywhere, paragliders taking off and swirling out of sight. It was truly surreal. We were still quite shocked by encountering the crash and rescue still in progress, and feeling energized and victorious in our own adventure. 

As if to punctuate the juxtaposition of the spring skiing party atmosphere and the true danger ever present, a rescue helicopter ominously swing into view to airlift the injured paraglider we had just walked past up and away to the hospital.

<br/>
{{< youtube vV_x5AJiF8U >}}
<br/>

Ironically, my niece and I had been considering whether or not to go paragliding that morning and decided to put it off until the next day, if at all. Premonition? Who knows. Would I go paragliding after seeing that accident? I think probably. Paragliding is an overwhelmingly safe sport. I don't know how the two ended up coming together, and not that it makes it any easier for them, but it did seem to be pilot error on one side or another. 

Reflecting on this, we watched the paragliders take off for a while. It was busy at their takeoff zone, and seeing it from that vantage point surprised me that it did seem a bit chaotic. Not a whole lot of organization, just a lot of people with chutes yeeting themselves off the side of a mountain into oblivion. 

Except for one guy. He had it right I think. We watched him go over to a wide open area all alone. He slowly arranged his chute and cords (so many cords to straighten). He stood for a few minutes, still, looking out over valley below. Then with a rustle of nylon and a few quick steps, he took flight. 

<br/>
{{< youtube 2zxYdMi-ti4 >}}
<br/>

*<span style="text-align:center; font-size:0.9rem">Later that day we read a short news story about the accident. It reported two people were gravely injured and airlifted to larger hospitals in Annecy and Geneva. I have not seen an updated report since, so I hope that means that they both survived and are recovering well. [link to article (French)](https://www.lemessager.fr/649313987/article/2024-03-14/chamonix-mont-blanc-deux-parapentistes-gravement-blesses-apres-une-collision-au)</span>*

---
<!-- ## 🎥 Videos -->


## 📸 Photo Gallery

> *(captions soon!)*

{{< gallery resize_options="500x" album="24in24/20240313-chamonix-pt3" >}}