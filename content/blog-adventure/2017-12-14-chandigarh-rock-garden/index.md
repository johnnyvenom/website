---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Chandigarh - Rock Garden"
subtitle: "India #5"
summary: "#5 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-14"
lastmod: "2017-12-14"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: 4. Bundala, Punjab]({{< relref "../2017-12-13-punjab-bundala-simbal-mazara" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 6. New Delhi ➡️]( {{< relref "../2017-12-15-new-delhi-agra-taj-mahal-india-6" >}})**

> **Post #5 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

We say goodbye to Punjab with a day in Chandigarh, where we spend a few hours wandering through the most enchanted Rock Garden. Both the city of Chandigarh and the Rock Garden are absolutely fascinating and unexpected.

### Chandigarh

> **Chandigarh** is a city and a [union territory](https://en.wikipedia.org/wiki/Union_territory "Union territory") in [India](https://en.wikipedia.org/wiki/India "India") that serves as the capital of both neighboring states of [Haryana](https://en.wikipedia.org/wiki/Haryana "Haryana") and [Punjab](https://en.wikipedia.org/wiki/Punjab,_India "Punjab, India"). The city is not part of either of the two states and is governed directly by the [Union Government](https://en.wikipedia.org/wiki/Government_of_India "Government of India"), which administers all such territories in the country. It was one of the early [planned cities](https://en.wikipedia.org/wiki/Planned_cities "Planned cities") in post-independence India and is internationally known for its architecture and urban design.[\[8\]](https://en.wikipedia.org/wiki/Chandigarh#cite_note-ci2-9) The master plan of the city was prepared by Swiss-French architect [Le Corbusier](https://en.wikipedia.org/wiki/Le_Corbusier "Le Corbusier"), which transformed from earlier plans created by the Polish architect [Maciej Nowicki](https://en.wikipedia.org/wiki/Maciej_Nowicki_(architect) "Maciej Nowicki (architect)") and the American planner [Albert Mayer](https://en.wikipedia.org/wiki/Albert_Mayer_(planner) "Albert Mayer (planner)"). Most of the government buildings and housing in the city, were designed by the Chandigarh Capital Project Team headed by [Le Corbusier](https://en.wikipedia.org/wiki/Le_Corbusier "Le Corbusier"), [Jane Drew](https://en.wikipedia.org/wiki/Jane_Drew "Jane Drew") and [Maxwell Fry](https://en.wikipedia.org/wiki/Maxwell_Fry "Maxwell Fry"). In 2015, an article published by [BBC](https://en.wikipedia.org/wiki/BBC "BBC") named Chandigarh as one of the perfect cities of the world in terms of architecture, cultural growth and modernisation. ([Wikipedia](https://en.wikipedia.org/wiki/Chandigarh))

### Rock Garden

> The **Rock Garden of Chandigarh i**s a [sculpture garden](https://en.wikipedia.org/wiki/Sculpture_garden "Sculpture garden") in [Chandigarh](https://en.wikipedia.org/wiki/Chandigarh "Chandigarh"), [India](https://en.wikipedia.org/wiki/India "India"). It is also known as **Nek Chand's Rock Garden** after its founder [Nek Chand](https://en.wikipedia.org/wiki/Nek_Chand "Nek Chand"), a government official who started the [garden](https://en.wikipedia.org/wiki/Garden "Garden") secretly in his spare time in 1957. Today it is spread over an area of 40 acres (161874.25 m²). It is completely built of industrial and home waste and thrown-away items. It is near [Sukhna Lake](https://en.wikipedia.org/wiki/Sukhna_Lake "Sukhna Lake").[\[3\]](https://en.wikipedia.org/wiki/Rock_Garden_of_Chandigarh#cite_note-3) It consists of man-made interlinked [waterfalls](https://en.wikipedia.org/wiki/Waterfall "Waterfall") and many other sculptures that have been made of scrap and other kinds of wastes ([bottles](https://en.wikipedia.org/wiki/Bottle "Bottle"), glasses, bangles, [tiles](https://en.wikipedia.org/wiki/Tile "Tile"), [ceramic](https://en.wikipedia.org/wiki/Ceramic "Ceramic") pots, [sinks](https://en.wikipedia.org/wiki/Sink "Sink"), electrical waste,broken[pipes](https://en.wikipedia.org/wiki/Pipe_(fluid_conveyance) "Pipe (fluid conveyance)"), etc.) which are placed in walled [paths](https://en.wikipedia.org/wiki/Trail "Trail").
> 
> In his spare time, Nek Chand started collecting materials from demolition sites around the city. He recycled these materials into his own vision of the divine kingdom of Sukrani, choosing a gorge in a forest near [Sukhna Lake](https://en.wikipedia.org/wiki/Sukhna_Lake "Sukhna Lake") for his work. The gorge had been designated as a land conservancy, a forest buffer established in 1902 that nothing could be built on. Chand’s work was illegal, but he was able to hide it for 18 years before it was discovered by the authorities in 1975. By this time, it had grown into a 12-acre (49,000 m2) complex of interlinked courtyards, each filled with hundreds of pottery-covered [concrete](https://en.wikipedia.org/wiki/Concrete "Concrete") sculptures of dancers, musicians, and animals. ([Wikipedia](https://en.wikipedia.org/wiki/Rock_Garden_of_Chandigarh))

#### Video: Monkey business at the Rock Garden

{{< youtube iG_IR82PYHM >}}

---

### Image gallery

{{< gallery resize_options="500x" album="india_5_chandigarh" >}}

---

