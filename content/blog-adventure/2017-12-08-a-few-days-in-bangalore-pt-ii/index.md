---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "A few more days in Bangalore"
subtitle: "India #2"
summary: "#2 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
authors: []
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-09"
lastmod: "2017-12-09"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: 1. Bangalore, Pt. 1]({{< relref "../2017-12-08-a-few-days-in-bangalore" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 3. Amritsar ➡️]( {{< relref "../2017-12-09-amritsar-golden-temple" >}})**

> **Post #2 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

More images from a week in Bangalore - tuk tuks, Commercial Street, Russell Market, MG Road... and my home office, the Hilton GolfLinks Embassy. At the end of the week we pack it in and head north for Punjab. (Videos at bottom of page!)

{{< gallery resize_options="500x" album="india_2_bangalore_2" >}}

-----

### Videos

{{< youtube PESHpPW3mW8 >}}

<br>

{{< youtube kKawfO9EK6Q >}}

---


