---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Colors"
subtitle: "Arriving in Bangalore"
summary: "#0 of 11 in a series: Day #1 India, we're strolling around in Indiranagar."
authors: []
tags: [ "travel", "adventure", "india"]
categories: [ "travel", "India" ]
date: "2017-12-02"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: Hello and welcome!]({{< relref "../2017-12-01-hello-and-welcome" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: 1. Bangalore Pt. 1 ➡️]( {{< relref "../2017-12-08-a-few-days-in-bangalore" >}})**

Day #1 India, we're strolling around in Indiranagar..

*Stay tuned for more...*

---


