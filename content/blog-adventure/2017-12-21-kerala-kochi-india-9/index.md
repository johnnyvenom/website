---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Kerala: Kochi"
subtitle: "India #9"
summary: "#9 of 11 in a series: At the end of 2017, my girlfriend and I spent a month in India. Here is a bit of what we experienced."
tags: [ "travel", "adventure", "india"]
categories: [ "travel" ]
date: "2017-12-21"
lastmod: "2017-12-21"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

**[⬅️ Previous: Darjeeling]({{< relref "../2017-12-19-darjeeling-india-8" >}}) // [⬆️ See the whole series ⬆️]({{< relref "/tags/india" >}}) // [Next: Kerala ➡️]( {{< relref "../2017-12-31-kerala-the-backwaters-india-10" >}})**

> **Post #9 of 11 in a series:** At the end of 2017, my girlfriend and I spent a month in India. Part work, part family visit, part holidays, all an adventure. Here is a bit of what we experienced.

Okay so, some nearly outdated brag-cation stuff to follow - I can't believe this report is nearing a year old! (Time to go back...) BUT. Here's what made arriving in Kochi so memorable: it took us 36 hours to get there and involved: 1 white-knuckled near-suicidal 3 hour taxi drive down a mountain with a _paan_\-ed up driver (betel leaf/nut chew, a stimulant, see the excellent movie DUNE (David Lynch version, *obvs*) for reference..), a very delayed train which led to a chance meeting young Hao Min - 18 yr old fashionista from Manipur (farthest east state in India) and all around awesome young dude, a moment of travel brilliance/improvisation from a random train station in the middle of nowhere, another three hour taxi ride, nearly an impromptu haircut in a tin shack at a crossroads, Calcutta (for a hot minute), a flight across India, another taxi, and BAM. After all that, we arrive here:

{{< figure src ="2017-12-21-17.35.53-1024x768.jpg" >}}

And this last run-on sentence makes me realize why this entry is so delayed.. Because every moment of this trip was significant and memorable, with or without the photos to prove it. Summing it up into a post always seems to fall a bit short. So, here's some pictures of the first part of our southern swing, in Kochi. We did what seemed the obvious thing to do: rented a Royal Enfield and headed down the cost to the beach.

---

{{< gallery resize_options="500x" album="india_9_kerala_kochi" >}}

Hot takes: Chinese fishing nets (see video below): fascinating; finally driving in India, on a motorcycle no less: frightening at first, exhilarating afterwards (liberal horn use always); fresh seafood: didn't eat any but looked delicious.

#### Some videos:

<div class="yt-container">
<iframe class="yt-playlist" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22FSgyoF2PGmfw21XwoNJ2GL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

- Video playlist:
  1. Chinese fishing nets doing their thing (pt. 1)
  2. Chinese fishing nets doing their thing (pt. 2)
  3. Casual Sunday afternoon net fishing (pt. 1)
  4. Casual Sunday afternoon net fishing (pt. 2 - this is fun?)

----

