---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Costa Rica 2022, pt. 3"
subtitle: "The Beach"
summary: "#3 of 3: The last leg of our sun-dappled Central American adventure takes us to the well travelled shores of the Pacific Ocean."
authors: []
tags: [adventure, travel, "Costa Rica", "Latin America"]
categories: [travel, "Latin America"]
date: 2022-04-17T06:57:13+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

---

*Part 3 of our sun-dappled Central American adventure takes us to the well travelled shores of the Pacific Ocean. In what should really be 2 or 3 posts, I sum into one big mac sized post, now finally put up eight months later. But hey, I have excuses (and other long overdue updates to post...) Anyways, enjoy. I took a ton of pictures and videos, and wrote a lot, just for the fun of it.*

{{< cta cta_text="📸 Skip to the photos and videos" cta_link="#-photo-gallery" cta_new_tab="false" >}}


---

{{< toc >}}

---

<br>

## 🐠 Day 5: Going coastal

Our fifth day of the trip marked a major turning point, where we left the hills and headed for the beach. Before we said goodbye to Monteverde we revisited our greatest hits. It started with Stella's for breakfast, this time opting for the daring open air "garden" table. Hilarity once again ensued as cute-ass monkeys surrounded us and continued with their monkey antics. 

<br> 

#### 💩 Apeshit

{{< figure src="Costa-Rica-2022-078.jpg" caption="We clearly should have understood this look as a warning." >}}

Of course, mess too much with the monkeys and and you'll surely get monkeyed with. And so it was, the first half of our breakfast punctuated by excited videography - *look! a monkey sneaking up from the back side! stealing someone's pancake! ooh, a baby monkey on mama's back!* - and so on. We munched on our food staring up into the trees at action overhead when a wet splat landed next to me, along with... spray. And thus, while we didn't fully get shit *on* by a monkey, we still got the collateral damage. Even though we missed the worst of it, our food was in the splash zone and we took no chances. Stella's was kind enough to remake our meals, which we ate hastily, and under a roof. All the sudden those monkeys weren't looking so cute after all. 



We enjoyed another soak in the private jungle hot tub and began the long descent to the coast. Next destination, Manuel Antonio National Park, jutting out into the Pacific Ocean. This time there were no coy attempts to snag a quick lunch on the way; our lesson was learned from our previous drive. A largely uneventful 4 hour drive brought us back to sea level, and the lush forest canopy gave way to palm tree groves and our first sightings of the sea. The temperature rose too - after the breezy and chilly cloud forest, we welcomed the warm, moist air on our faces. 

<br>

#### 🌴 Manuel Antonio

While nothing was going to compare to the Hotel Belmar, our accommodation in Manuel Antonio was a different breed altogether. Think... Waikiki timeshare, maybe retirement community, maybe a little something something in Florida. It was advertised on booking.com as "apartment", though despite our fantasies about a proper stove and perhaps a blender, in reality we were greeted with a single small room with a bed, a dingy fridge and coffee machine in one corner, and threadbare coverings and curtains. But all was not lost. A terrasse that outsized the actual indoor room was included, and well equipped with a hammock, cushioned benches, pillows and a top notch view over the ocean well below and the National Park peninsula. While we were not so much inclined to actually *touch* anything there, it was, in its faded way, somehow perfect for the setting. As if to welcome us to our new ecological reality, while we gained our bearings and stared out towards the ocean from the terrasse, a pair of noisy scarlet macaws flew by in tight formation.

The town of Manuel Antonio, if there even is such a thing, is really a strip a few kilometers long that leads up to the entrance to the National Park. Along the strip is the expected hodgepodge of hotels, restaurants and tourist shops selling all manner of tapestries, beach towels and useless souvenir knick knacks. The lower budget accommodations (including ours) were well featured along the main drag, while the more indulgent options were quietly tucked away behind important-looking gates and down nondescript side roads. In all, the stretch gave off a wistful vibe, punctuated by the dust of busses rolling by (both public and tourist transports) and the constant medley of Costa Rican cumbia music spilling out from the many open air bars. 

{{< video src="albums/costa-rica/part-3-videos/Costa-Rica-2022-90.mp4" controls="yes">}}

Upon arriving, we did the only thing that seemed to make sense, grabbed a six pack of Imperial beer (Costa Rica's finest, or at least the most prevalent) and drove to the nearest beach access. By chance we found the perfect little dirt road with ample free and safe parking to ditch the car (right behind the "Igloo Beach Lodge" which features - wait for it - rows of round-domed, air-conditioned concrete igloos and a sign out front sincerely proclaiming the "*your vibe attracts your tribe*". This place must have really slapped in 1993.) Anyways, we arrived at the beach and put out our towels, splashed in the warm salt water, and soaked in the kaleidoscopic sunset until all that was left of it was a deep red band across the horizon. Dinner afterwards was a dice roll but, motivated by previous veggie burger fails, we posted up at a good-enough place that had aforementioned veggie burger with an actual *veggie burger patty*. 

<br> 

---

<br>

## 🌴 Day 6 is for the Beach

{{< figure src="Costa Rica-3-biesanz-beach.jpg" caption="Biesanz Beach at Manuel Antonio" >}}

Our stay in Manuel Antonio was unique, as it was the only place where we stayed for 3 consecutive nights in the same accommodation. This meant two full days, and we planned accordingly. This first day was simple: relax and enjoy the beach. We returned to our sunset beach from the night before for an early morning barefoot run. Despite perhaps a minor case of toe blisters (running shoes are still recommended, even on the beach!), with our "work" out of the way for the day, the rest we spent easy. Mandeep spotted a small beach down a side road on the map, and we made it our destination. A short drive and hike downhill opened out to a small cove. Hardly hidden or deserted, we were welcomed by beach vendors who set us up with the standard two lounge chairs and an umbrella, and we spent the hours alternating between swimming, lounging, and snacking. 

<br>

#### 🦥 The Sloth

{{< video src="albums/costa-rica/part-3-videos/Costa-Rica-2022-97.mp4" controls="yes" >}}

Despite the significant human presence nature was on full display wherever we went. The cries of howler monkeys rang out with regular frequency even if they kept themselves largely out of sight. But upon exiting the water late in the afternoon I waled over to where a group of people congregated staring up into a tree. I expected to see more monkeys but instead was greeted by a sloth, slowly going about its business in the lower branches, making a meal of the youngest parts of the foliage. Seemingly unperturbed by the gawkers below, it enjoyed its meal before making its way back up into the leaves and out of site. 

When the shadows grew long we packed up and sought out the holy trinity of post-beach consumption: fruity drinks, nachos, and a sunset.

<br> 

---

<br>

## 🏞️ Day 7: Manuel Antonio National Park

{{< figure src="Costa-Rica-2022-098.jpg" >}}

After our previous day of relaxation, we were ready to take day 7 by storm. Up early (thanks howler monkeys), breakfast at the nearby café at 7 and off to the national park for a day of hiking, before finishing off at the beach later. Just one problem, we somehow forgot the repeated notices that tickets for park entrance must be purchased online before entering. So, after being rebuffed, we sat in the car to purchase them, though they were only available to enter the park starting at noon. So we returned to our home. I went for a run to yesterday's beach (great going down, torture going back up) and we enjoyed the sights and sounds of nature from the terrasse. 

{{< figure src="Costa-Rica-2022-100.jpg" caption="Manuel Antonio vistas" >}}

At the appointed time we returned to our (not so) secret free beach parking spot and walked the beach to the entrance of the national park. From there we hiked the length of all of the trails in the park, which amounted ot around 10 kilometers. The "sloth trail" yielded 0 sloths, but the sights and sounds were vibrant. The trails were formidable, nearly every step was either up or down, and by the end our legs were aching. The outlooks to the ocean were incredible. We finished the outing back on the beach near where we had parked, enjoying another perfect sunset. 

{{< figure src="Costa-Rica-2022-087.jpg" caption="Moar sunsetz" >}}

Our day ended with falafels and fancy drinks at an odd trendy/seedy place that featured an unexpected and uncommon large vegetarian menu, served on a terrasse over a supermarket and to the soundtrack of led lights and dance music. We could only assume that things get crazy here late at night, but for us we were back home by 9, and likely asleep by 9:15. Ahhh vacation.

<br>

---

<br>

## 🏝️ Day 8: Playa Hermosa

By this time we could feel the impending end of our vacation. Tapering, if you will. Fewer structured activities. More beach days. Total relaxation. This day we would travel to Playa Hermosa, a little surf beach one hour north, for the penultimate leg of our trip. 

But first, a return to our favourite hidden beach. Feeling like true veterans, we paid the 2000 colones (about CAD $4) to the local "security" guy who would keep an eye on our car parked on the side of the road and hiked down to the beach. (Normally we would say no to these frequent unofficial solicitations for "watch your car" security, but this one felt justified as we were checked out of the apartment and carrying all of our luggage with us. In truth, the road, neighbourhood, and really most of Costa Rica, felt quite safe and I feel like the only mischief that we risked would have come at the hands of a disgruntled car watcher who we might refuse to pay...) 

{{< figure src="Costa-Rica-2022-107.jpg" caption="Last stop at our favourite beach" >}}

At the beach I rented a mask and snorkel at the recommendation of one of the beach attendants and made my way to rock formations at the entrance to the cove. While I appreciated the swim and a chance to revive my snorkeling skills, visibility was low (fine sand leading to cloudy water) and surprisingly, the coral formations looked largely dead - bleached white and rust colored, with some algae growing here and there, and few fish to be seen. In all, the activity made me a little melancholy. Was this the deserted remains of a once vibrant coral garden? 

After a couple hours of beach time, we said our goodbyes and made our way back to the car. Our final stop before going north was a falafel restaurant we saw on our way in, that served astonishingly great falafel pitas. Full and with salt on our skin, we headed to Playa Hermosa.

#### 🌊 How big can the waves really be?

After encountering smaller but persistent waves at the larger sunset beach we enjoyed in Manuel Antonio, Mandeep had appreciated the relative calm of the smaller beach we had frequented. Coming to Playa Hermosa then, was a different story. We didn't know much about it except it sat just a few kilometers south of the larger town of Jaco and it was a "surf spot". We had initially booked a hotel in Jaco, but then after reading several reviews we wisely decided that it sounded too big, too loud and too touristy for our tastes, but a chill surf spot would be a nice way to serve out our last vacation days. But how big would the waves really be? 

{{< figure src="Costa-Rica-2022-112.jpg" caption="The calm side of Playa Hermosa" >}}

We reserved a hotel with a pool and beach view. If the ocean wasn't good for swimming in, at least we could watch it and swim in a pool. It turns out, Playa Hermosa is one of the major surf spots in Costa Rica and, despite my assessment that the ocean seemed overall pretty placid and I doubted there would be any "real surf waves of consequence", we were greeted by the constant crashing of big beautiful waves, maybe 6-8 feet high around the clock. Sure enough, surfers were out at all hours doing their thing, and it most certainly wasn't a great swimming spot. 

{{< figure src="Costa-Rica-2022-111.jpg" caption="Oh hotel, you shouldn't have...">}}

Our room was basic and beachy, and a quick walk on the beach and two lounge chairs facing the sunset made us feel at home for our waning vacation time. We ate our dinner at the hotel restaurant (which once again impressed us with a small but thoughtful selection of vegetarian choices - my vegetable-stuffed chili relleno hit the spot!). Once again early to bed, we fell asleep to the constant crashing of the waves. 

<br> 

---

<br>

## 🧘‍♀️ Day 9: Last day to do nothing


Our final full uninterrupted vacation lounge do nothing day. Our normal 4am howler monkey alarm had been replaced but the nonstop crash of the waves which was, alternately, soothing and maddening. We were both awake at 5am. I walked the beach as the sun came up. Mandeep ran the beach. We watched the morning surfers. We swam in the pool, drank cheap beer, and dozed. We repeated it all. We ate fresh empanadas from a cute little stand towards one end of the beach. We swam more, we drank more beer, and watched more surfers. 

{{< figure src="Costa-Rica-2022-127.jpg" caption="Empanadas on the beach">}}

<br>

#### 🌆 Jaco giving me mixed emotions.. 

{{< figure src="Costa-Rica-2022-148.jpg" caption="El Miro">}}

For our evening activity we decided we could have our big night out and go the the metropolis that is Jaco. There was one place in particular I was interested to check out, El Miro. It is the remains of an elaborate set of terrasses and staircases that look to have been the main outdoor areas of a large mansion overlooking Jaco from up high on a hill. But for some reason, the mansion was never finished and thee remains were abandoned long ago, adapted into a sort of public art space. We found the unmarked trailhead to walk up and parked nearby (avoiding yet another unofficial car-watcher for hire). The dusty and steep walk was about 20 minutes long and accompanied by the loud roar of cicadas. At the top we found the structure as advertised, just a few minutes before the sunset. We spent the next half hour walking around and taking pictures and wondering at the faded beauty of this unexpected gem. 

{{< video src="albums/costa-rica/part-3-videos/Costa-Rica-2022-154.mp4" controls="yes" caption="Sound of a million cicadas on the trail to El Miro" >}}

We were less impressed with Jaco itself. Loud and opinionated, it was a garish blend of cars, blinking LED neon lights, tourist traps, bad music, casinos and sketchy vibes. Our plan: find a convenient place to get a cocktail, figure out a good place for dinner, and get out. After walking and finding little, we settled for two quick beers and decided to return to a small vegetarian restaurant what we had poked our head into which was nice but a) empty, b) closing soon, and c) located in a shopping mall. But the food was enticing so we returned and had a fun vegan dinner of fried tofu sandwich, "smoked salmon" topped salad, and buffalo style cauliflower. We enjoyed the food, but were ready to make our escape, with a final stop by a *panaderia* (bakery) for a tres leches cake for Mandeep. We returned to the playa and relaxed on a large lounge chair for two by the pool, staring up at the stars. Mandeep munched on the cake while I dozed off. 

<br>

---

<br>

## 🏄 Day 10: Surf and sickness, and back to the city

{{< video src="albums/costa-rica/part-3-videos/Costa-Rica-2022-121.mp4" controls="yes" >}}

And so we made it to our final chapter of our vacation, or perhaps even an epilogue. To make things easy for our return, and with some vested interest in urban Costa Rican culture, we planned to drive back to the capital city of San José the day before our departure, where we could spend an afternoon walking around the city and seeing some sights, to be ready to make our way to the airport the following day. 

When we put together our itinerary months before we had debated what hotel we should stay at in San José. Should we splash out and stay somewhere luxurious to end our vacation on a high note? Or keep things simple and economical, as it would just be a short overnight and we would be on our way? We had ultimately decided on the latter and had booked a quaint looking room at a small and inexpensive hotel in the center of the city. But by the end of our vacation days, after the wistful Manuel Antonion timeshare and the surfy frumpy beach hotel, we had a change of heart and decided to compromise, going with a much more expensive (though still not *top* shelf) room at the four-star Hotel Presidente. We were both looking forward to a bed with quality sheets and no back pain, pillows that don't smell weird (even if that was mostly only in our heads), and surfaces that we weren't reluctant to touch. Little did we know how fortuitous this decision would be!

<br>

#### 🤢 Jaco's revenge

It could have been the tres leches. Or it could have been the salad. Who knows, but we woke for our final full vacation day in our little beach hotel at our accustomed ungodly hour before dawn, and Mandeep was starting to feel sick. Sadly something hadn't gone right and her stomach was in knots. She stayed in bed while I made my way out, did a short run, visited a market for water and electrolytes, and did what I could to make her comfortable back at the room. On the beach, it was Saturday and a surf competition kicked off around 7am. I alternated between hanging by our room and sitting on the beach watching incredible surfing and the occasional "bombas" (the biggest waves). Mandeep wasn't feeling great for traveling, so we stayed until the last possible moment to check out and made the drive to San José. 

We checked into our Hotel Presidenté, and given Mandeep's illness, we were relieved to be checking into a nice hotel and clean room. Mandeep napped while I took a short walk and returned with snacks. In the early evening we enjoyed a light dinner at the hotel's rooftop terrasse, and fell asleep watching a movie in bed. Thankfully, after a day of rest, Mandeep was feeling a bit better, and ready for our flight home the next day. 

## 🇨🇦 Back home - Travel Day

Maybe we're just city folks. But I had the soundest night of sleep yet in the San José hotel, surrounded by the city sounds out the window. Amusingly, our early morning alarm this day was the chortling of doves that started around 5am, softer than a howler monkey but similar persistence. We were well awake by 6, and planned our escape from the city. With Mandeep back in action, we found a restaurant around the corner for breakfast. Then came our mandatory COVID rapid antigen tests, required to re-enter Canada. The city hospital provides a well organized drive through testing service in the hospital parking lot, so we headed there next. Everything proceeded well, if slowly, and we were soon on our way back to our room to check out, return the rental car and head back to the airport. Our test results (negative, hopefully, because we wouldn't have been able to leave if we tested positive, and truly didn't really have much of a backup plan if this would happen) would be emailed to us "within two hours of receiving the tests" so we were promised. 

#### 🔧 A final wrench in the works

It has been since October 2019 that we took a proper vacation, making this one two years and five months in the making. While I definitely felt a little rusty with travel planning, we are both seasoned travel pros and, given the anticipation for this trip, we planned carefully and thoroughly. Still however, as evidenced by the debacle of my almost disappeared laptop and iPad on our departure, traveling in the best of times isn't guaranteed to run smoothly. 

But, my friends, this is hardly the best of times. COVID has gripped the entire world for the last two years. Just less than two months ago, Quebec had an enforced curfew and all restaurant dining rooms and bars were closed. Supply chain shortages wreak havoc across all sectors, there are employee shortages everywhere, networks are unreliable, markets are crashing. And all this without even getting into the most recent existential crisis of Russia war waged on Ukraine, threatening a new world war and causing the more rational of us to gasp a collective "WHAT THE FUCK???" into the void. Indeed, it is hardly the best of times. 

And so, it would have been too easy to just make our way quietly home, arriving early to our tests, early to drop the car off, and early to our gate. In the final minutes before boarding, we were at a small gift shop near the gate trying to spend the last of our colones on a bottle of Costa Rican hot sauce when we heard our names over the intercom getting ushered to the counter. It is never good news when you get called up to the counter before your flight, and no, they are not calling you up to offer you a free upgrade. 

They asked for our negative COVID test results, which they said were required to board our flight. I had thought that they were only required once we landed in Canada, but nonetheless we should have received them hours before in our email. Staring at our devices we realized that no, they hadn't been sent to us "within two hours" that had been promised. The counter again, while sympathetic, clearly stated that unless we could retrieve our test results we could definitely not board that plane. I called the phone number I had for the hospital but was quickly lost in a maze of auto-recorded messages and prompts. Thankfully the agent offered to call the hotel directly and within moments spoke to someone at the hospital who confirmed that in fact, no they hadn't sent the results, and perhaps didn't have them, as there was a problem with their system. They would try to sort it out and see what they could do. 

So we waited, frantically refreshing our emails, worrying that for some reason emails might not be coming through (between bad airport wifi and a dodgy data plan), which boarding proceeded, already well past the scheduled boarding time. 

And then, as panic fully set in and we openly wondered what next, once we can't board our flight home, our emails simultaneously beeped, and the negative test results came in. We flashed them to the attendant and boarded the plane, where I sit completing this travelogue. 

## 📝 Epilogue

Is it over? Is this the end? Have we made it safely home? With everything, it seems to soon to tell. We're scheduled to land in an hour or so. There has been freezing rain in Montreal. At least we have our papers in order, or so we think. 

What does it all mean? Why write this travelogue anyways? Who cares about the mundane details of a pretty conventional vacation? The truth is, I really don't know. If you, dear reader, have made it this far, I salute you. I hope you enjoyed it. Mandeep and I say to each other sometimes that the world is broken. In many ways I really believe that. Not entirely, and perhaps not beyond repair. But it's a crazy messy hard life out there. There is so much suffering, hatred, pain and misery. Uncertainty and adversity. Maybe the mundane isn't so bad once in a while. Maybe that's just the point. Well, that and the monkeys, I guess. 

*Fin.*

## ---

<br>




## 📸 Photo gallery: 

> *captions coming.. eventually ;)*

{{< gallery resize_options="500x" album="costa-rica/part-3" >}}

## 📹 Videos:

<div class="yt-container">
<iframe class="yt-playlist" src="https://www.youtube.com/embed/videoseries?list=PLPGu0ZQGW22HzJosHRGt_SkOR7SsFn9BB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>



-----

