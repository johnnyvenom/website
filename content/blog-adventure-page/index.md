---

title: "Adventures"
# subtitle: "Adventures subtitle"
summary: "A sometimes blog about various adventures here and there."
slug: "adventures"
authors: []
tags: []
categories: ["blogs"]
date: "2021-10-10T00:00:00Z"  # Add today's date.
lastmod: 2022-06-07T09:59:56-04:00
featured: false
draft: false

type: "widget_page"  # Page type is a Widget Page

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

---