---
# Documentation: https://wowchemy.com/docs/managing-content/

title: A Method for Gestural Control of Augmented Harp Performance
subtitle: 'Performance @ ICLI Conference'
summary: ''
authors:
- Alexandra Tibbitts
- John Sullivan
- Ólafur Bogason
- Brice Gatinet
tags: []
categories: []
date: '2018-01-01'
lastmod: 2022-02-18T12:12:20-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [research/gestural-control-of-augmented-instrumental-performance]
publishDate: '2022-02-18T17:12:19.969389Z'
publication_types:
- '0'
abstract: 'Here we present an interdisciplinary collaboration and performance, featuring a gestural control system designed to augment harp performance. From a performers perspective, the developed interface system and prototype presented opportunities for real-time control and manipulation of the traditional instrument. Collaborators exchanged ideas and commentary, as well as problem-solved, in real-time. This was advantageous for direct and efficient regulation and implementation of the hardware and software into the artistic phase of this project and resulting composition. For the performer, the device needed to be lightweight, ergonomic, and user-friendly. In this performance, the device uses amplified harp, as well as voice of the player and electric tape, as sound-sources for computer-based audio effects and processing.
'
publication: '*Proceedings of the International Conference on Live Interfaces*'
---
