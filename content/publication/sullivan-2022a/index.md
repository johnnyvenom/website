---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'From Fiction to Function: Imagining New Instruments Through Design Workshops'
subtitle: '*This article is published in open access and is free to download.*'
summary: ''
authors:
- John Sullivan
- Marcelo M Wanderley
- Catherine Guastavino
tags: [music technology, DMIs, interface, HCI, design fiction]
categories: [publication, article, journal, Computer Music Journal, music technology]
date: '2023-08-15'
lastmod: 2023-04-15T16:07:59+02:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

url_pdf: https://direct.mit.edu/comj/article-pdf/46/3/26/2164800/comj_a_00644.pdf
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [research/design-for-performance]
publishDate: '2023-04-15T14:07:59.811845Z'
publication_types:
- '2'
abstract: "This article introduces a series of workshop activities carried out with expert musicians to imagine new musical instruments through design fiction. At the workshop, participants crafted nonfunctional prototypes of instruments they would want to use in their own performance practice. Through analysis of the workshop activities, a set of design specifications was developed that can be applied to the design of new digital musical instruments intended for use in real-world artistic practice. In addition to generating tangible elements for instrument design, the theories and models utilized, drawn from human–computer interaction and human-centered design, are offered as a possible model for merging the generation of creative ideas with functional design outputs in a variety of applications within and beyond music and the arts."
publication: '*Computer Music Journal*'
doi: '10.1162/comj_a_00644'
---
