---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Noisebox : Design and Prototype of a New Digital Musical Instrument'
subtitle: ''
summary: ''
authors:
- John Sullivan
tags:
- design
- digital musical instruments
- NIME
categories: []
date: '2015-01-01'
lastmod: 2022-02-18T12:12:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: ["research/noisebox"]
publishDate: '2022-02-18T17:12:18.511997Z'
publication_types:
- '1'
abstract: 'This paper documents the initial prototyping of a new digital musical instrument. Specifically, it focuses on design of the interface, and contextualizes the project through some of the existing research in the field of gestural control of new musical instruments. The project began with a concept for a standalone hand-held polyphonic synthesizer called the Noisebox (Figure 1). Several key concepts and strategies were explored and implemented during its development, including: analysis and application of gesture in musical performance, choice of sensors and sensor conditioning, appropriate mapping strategies, and evaluation of user experience. The outcome yielded a functional prototype that fulfilled the initial goal of the project to design and build a working instrument from start to finish. The stage documented here represents the first phase of a longer project. Future phases will conduct user tests to measure the success of the instrument based on performer feedback and refine the design through multiple iterations, leading to a finished instrument.'
publication: '*Proceedings of the International Computer Music Conference*'
links:
- name: URL
  url: http://idmil.org/publication/noisebox-design-and-prototype-of-a-new-digital-musical-instrument/
---
