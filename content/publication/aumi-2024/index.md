---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Improvising Across Abilities: Pauline Oliveros and the Adaptive Use Musical Instrument"
authors: 
- "The AUMI Editorial Collective (Eds)"
date: 2024-01-31
doi: "10.3998/mpub.11969438"

# Schedule page publish date (NOT publication's date).
publishDate: 024-03-25T17:26:21+01:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: "University of Michigan Press, Music and Social Justice"

abstract: "Improvising Across Abilities: Pauline Oliveros and the Adaptive Use Musical Instrument (AUMI) brings together scholars, musicians, and family members of people with disabilities to  collectively recount years of personal experiences, research, and perspectives on the societal and community impact of inclusive musical improvisation. One of the lesser-known projects of composer, improviser, and humanitarian, Pauline Oliveros (1932–2016), the AUMI was designed as a liberating and affordable alternative to the constraints of instruments created only for normative bodies, thus opening a doorway for people of all ages, genders, abilities, races, and socioeconomic backgrounds to access artistic practice with others. More than a book about AUMI, this book is an invitation to readers to use AUMI in their own communities.<br/><br/>This book, which contains wisdom from many who have been affected by their work with the instrument and the people who use it, is a representation of how music and extemporized performance have touched the lives and minds of scholars and families alike. Not only has AUMI provided the opportunity to grow in listening to others who may speak differently (or not at all), but it has been used as an avenue for a diverse set of people to build friendships with others whom they may have never otherwise even glanced at in the street. By providing a space for every person who comes across AUMI to perform, listen, improvise, and collaborate, the continuing development of this instrument contributes to a world in which every person is heard, welcomed, and celebrated.<br/><br/>The <strong>AUMI Editorial Collective</strong> includes Thomas Ciufo, Abbey Dvorak, Kip Haaheim, Jennifer Hurst, IONE, Grace Shih-en Leu, Leaf Miller, Ray Mizumura-Pence, Nicola Oddy, Jesse Stewart, John Sullivan, Sherrie Tucker, Ellen Waterman, and Ranita Wilks."

# Summary. An optional shortened abstract.
summary: ""

tags: 
  - accessible design
  - NIME
  - digital musical instruments
  - interview
  - Pauline Oliveros
categories:
  - music technology
  - publication
  - book
  - research
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: 
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: ['research/adaptive-use-musical-instrument']

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

<!-- This is a [test]({{< relref "../../project/interactive/the-game-of-life" >}}). -->
