---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Reinventing the Noisebox: Designing Embedded Instruments for Active Musicians'
subtitle: ''
summary: ''
authors:
- John Sullivan
- Julian Vanasse
- Catherine Guastavino
- Marcelo M Wanderley
tags:
- ★
- digital fabrication
- embedded acoustic instruments
- participatory design
categories: []
date: '2020-01-01'
lastmod: 2022-02-18T12:12:19-05:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: ["research/noisebox"]
publishDate: '2022-02-18T17:12:19.371213Z'
publication_types:
- '1'
abstract: This paper reports on the user-driven redesign of an embedded digital musical instrument that has yielded a trio of new instruments, informed by early user feedback and co-design workshops organized with active musicians. Collectively, they share a stand-alone design, digitally fabricated enclosures, and a common sensor acquisition and sound synthesis architecture, yet each is unique in its playing technique and sonic output. We focus on the technical design of the instruments and provide examples of key design specifications that were derived from user input, while reflecting on the challenges to, and opportunities for, creating instruments that support active practices of performing musicians.
publication: '*Proceedings of the International Conference on New Interfaces for Musical Expression*'
links:
- name: URL
  url: http://idmil.org/publication/reinventing-the-noisebox-designing-embedded-instruments-for-active-musicians/
---
