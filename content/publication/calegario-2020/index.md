---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Probatio 1.0: Collaborative Development of a Toolkit for Functional DMI Prototypes'
subtitle: ''
summary: ''
authors:
- Filipe Calegario
- Marcelo M Wanderley
- João Tragtenberg
- Johnty Wang
- John Sullivan
- Eduardo Meneses
- Ivan Franco
- Mathias Kirkegaard
- Mathias Bredholt
- Josh Rohs
tags:
categories: []
date: '2020-01-01'
lastmod: 2022-02-18T12:12:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:18.155054Z'
publication_types:
- '1'
abstract: 'Probatio is an open-source toolkit for prototyping new digital musical instruments created in 2016. Based on a morphological chart of postures and controls of musical instruments, it comprises a set of blocks, bases, hubs, and supports that, when combined, allows designers, artists, and musicians to experiment with different input devices for musical interaction in different positions and postures. Several musicians have used the system, and based on these past experiences, we assembled a list of improvements to implement version 1.0 of the toolkit through a unique international partnership between two laboratories in Brazil and Canada. In this paper, we present the original toolkit and its use so far, summarize the main lessons learned from musicians using it, and present the requirements behind, and the final design of, v1.0 of the project. We also detail the work developed in digital fabrication using two different techniques: laser cutting and 3D printing, comparing their pros and cons. We finally discuss the opportunities and challenges of fully sharing the project online and replicating its parts in both countries.'
publication: '*Proceedings of the International Conference on New Interfaces for Musical Expression*'
links:
- name: URL
  url: http://idmil.org/publication/probatio-1-0-collaborative-development-of-a-toolkit-for-functional-dmi-prototypes/
---
