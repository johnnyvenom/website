---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Embracing the messy and situated design of technology for dance"
authors: 
- John Sullivan
- Sarah Fdili Alaoui
- Liz Santoro
- Pierre Godard
date: 2023-07-15T15:50:12+02:00
doi: "10.1145/3563657.3596078"

# Schedule page publish date (NOT publication's date).
publishDate: 2023-07-15T15:50:12+02:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: "*Proceedings of the Designing Interactive Systems Conference (DIS '23)*"
publication_short: "*Designing Interactive Systems Conference (DIS '23)*"

abstract: Designing technology for dance presents a unique challenge to conventional design research and methods. It is subject to the diverse and idiosyncratic approaches of the artistic practice that it is situated within. [We investigated this by joining a dance company to develop interactive technologies for a new performance]({{< relref "../../project/interactive/the-game-of-life" >}}). From our firsthand account, we show the design space to be messy and non-linear, demanding flexibility and negotiation between multiple stakeholders and constraints. Through interviews with performers and choreographers, we identified nine themes for incorporating technology into dance, revealing tensions and anxiety, but also evolution and improvised processes to weave complex layers into a finished work. We find design for dance productions to be resistant to formal interpretation, requiring designers to embrace the intertwining stages, steps, and methods of the artistic processes that generate them. We suggest that our findings can be of value in other HCI contexts requiring flexible design approaches.

# Summary. An optional shortened abstract.
summary: ""

tags: [performance, research through design, interaction design, performance-led research, dance, DIS, HCI]
categories: [conference, proceedings, publication, dance]
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://dl.acm.org/doi/pdf/10.1145/3563657.3596078
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: ['interactive/the-game-of-life']

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

<!-- This is a [test]({{< relref "../../project/interactive/the-game-of-life" >}}). -->
