---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Mid-Air Haptics for Digital Musical Instruments
subtitle: ''
summary: ''
authors:
- John Sullivan
- Aditya Tirumala Bukkapatnam
- Marcelo M. Wanderley
tags: []
categories: []
date: '2018-01-01'
lastmod: 2022-02-18T12:12:19-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:18.988641Z'
publication_types:
- '1'
abstract: For this workshop, we discuss our work implementing the Ultrahaptics system as a tactile display for music interaction. With the provided API we have compiled modules for the popular music programming environment Max, and extended an existing digital musical instrument with haptic capabilities.
publication: '*CHI’18 Workshop on Mid-Air Haptics for Control Interfaces*'
---
