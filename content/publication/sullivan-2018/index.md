---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Gestural Control of Augmented Instrumental Performance: A Case Study of the
  Concert Harp'
subtitle: ''
summary: ''
authors:
- John Sullivan
- Alexandra Tibbitts
- Brice Gatinet
- Marcelo M.M. Wanderley
tags:
- ★
- augmented instruments
- Gesture
- Harp
- Interface
- Motion capture
- NIME
categories: []
date: '2018-01-01'
lastmod: 2022-02-18T12:12:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [research/gestural-control-of-augmented-instrumental-performance]
publishDate: '2022-02-18T17:12:18.744056Z'
publication_types:
- '1'
abstract: We present a gestural control system to augment harp performance with real-time control of computer-based audio affects and processing. While the lightweight system was designed for use alongside any instrument, our choice of the concert harp represented a unique case study in gestural control of music. The instrument's large size and physically demanding playing technique leaves the performer with little spare bandwidth to devote to other tasks. A motion capture study analyzed instrumental and ancillary gestures of natural harp performances that could be mapped effectively to control of additional signal processing parameters. The initial findings of the study helped to guide the design of custom gesture control devices and user software, and a new work for solo harpist and electronics was created. We discuss our findings, successes and challenges in the study and design of gesture control for augmented instrumental performance, with particular focus on the concert harp.
publication: '*Proceedings of the International Conference on Movement and Computing*'
doi: 10.1145/3212721.3212814
---
