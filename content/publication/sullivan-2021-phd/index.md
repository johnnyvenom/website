---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Built for Performance: Designing Digital Musical Instruments for Professional Use'
subtitle: 'Ph.D. dissertation, defended February 2021'
summary: ''
authors:
- John D Sullivan
tags: []
categories: []
date: '2021-01-01'
lastmod: 2022-02-18T12:12:19-05:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:19.482010Z'
publication_types:
- '7'
abstract: "The field of digital musical instrument (DMI) design is highly interdisciplinary and comprises a variety of different approaches to developing new instruments and putting them into artistic use. While these vibrant ecosystems of design and creative practice thrive in certain communities, they tend to be concentrated within the context of contemporary experimental musical practice and academic research. In more widespread professional performance communities, while digital technology is ubiquitous, the use of truly novel DMIs is uncommon. 

The aim of this dissertation is to investigate the unique demands that active and professional performers place on their instruments, and identify ways to address these concerns throughout the design process that can facilitate development of instruments that are viable and appealing for professionals to take up into long-term practice. The work presented here represents three phases of user-driven research, using methods drawn from the fields of Human-Computer Interaction and Human-Centered Design. First, a survey of musicians was conducted to understand how DMIs are used across diverse performance practices and identify factors for user engagement with new instruments. Second, design workshops were developed and run with groups of expert musicians that employed non-functional prototyping and design fiction as methods to discover design priorities of performers and develop tangible specifications for future instrument designs. Finally, multiple new DMIs have been designed in two primary contexts: first, three instruments were developed in response to the workshop specifications to meet general criteria for DMI performance; second, two systems for augmented harp performance were built and integrated into a musician’s professional practice based on a long-term research-design collaboration. 

Through these projects, I propose the following contributions that will aid designers in the development of new DMIs intended for professional performers. The survey results have been distilled into a list of considerations for designers to address the unique demands and priorities of active performers in the development of new DMIs. A complete methodology has been developed to generate design specifications for novel DMIs that leverages the tacit knowledge of skilled performers. Finally, I offer practical guidelines, tools and suggestions in the technical design and manufacture of instruments that will be viable for use in long-term professional practice.
"
publication: '*Ph.D. thesis, McGill University*'
links:
- name: URL
  url: https://escholarship.mcgill.ca/concern/theses/tx31qp74p?locale=en
---
