---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Stability, Reliability, Compatibility: Reviewing 40 Years of DMI Design'
subtitle: ''
summary: ''
authors:
- John Sullivan
- Marcelo M. Wanderley
tags:
- ★
- digital musical instruments
- meta-review
- NIME
categories: []
date: '2018-01-01'
lastmod: 2022-02-18T12:12:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:18.872657Z'
publication_types:
- '1'
abstract: Despite the proliferation of new digital musical instruments (DMIs) coming from a diverse community of designers, researchers and creative practitioners, many of these instruments experience short life cycles and see little actual use in performance. There are a variety of reasons for this, including a lack of established technique and repertoire for new instruments, and the prospect that some designs may be intended for other purposes besides performance. In addition, we propose that many designs may not meet basic functional standards necessary for an instrument to withstand the rigors of real-world performance situations. For active and professional musicians, a DMI might not be viable unless these issues have been specifically addressed in the design process, as much as possible, to ensure trouble-free use during performance. Here we discuss findings from user surveys around the design and use of DMIs in performance, from which we identify primary factors relating to stability, reliability and compatibility that are necessary for their dependable use. We then review the state of the art in new instrument design through 40 years of proceedings from three conferences - ICMC, NIME, and SMC - to see where and how these have been discussed previously. Our review highlights key factors for the design of new instruments to meet the practical demands of real-world use by active musicians.
publication: '*Proceedings of the 15th Sound and Music Computing Conference (SMC)*'
links:
- name: URL
  url: http://idmil.org/publication/stability-reliability-compatibility-reviewing-40-years-of-dmi-design/
---
