---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Slapbox: Redesign of a Digital Musical Instrument towards Reliable Long-Term
  Practice'
subtitle: ''
summary: ''
authors:
- Brady Boettcher
- John Sullivan
- Marcelo M. Wanderley
tags: []
categories: []
date: '2022-06-01'
lastmod: 2023-04-15T16:07:58+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2023-04-15T14:07:58.406911Z'
publication_types:
- '1'
abstract: ''
publication: '*Proceedings of the International Conference on New Interfaces for Musical Expression*'
doi: 10.21428/92fbeb44.78fd89cc
url_pdf: https://assets.pubpub.org/mn1ma5tb/78fd89cc-4989-4267-a0dd-95ea2ba052b0.pdf
---
