---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Musicking the Body Electric: The "Body:Suit:Score" as a Polyvalent Score
  Interface for Situational Scores'
subtitle: ''
summary: ''
authors:
- Sandeep Bhagwati
- Isabelle Cossette
- Joanna Berzowska
- Marcelo Wanderley
- John Sullivan
- Deborah Egloff
- Marcello Giordano
- Adam Basanta
- Julian Stein
- Joseph Browne
- Alexandra Bachmeyer
- Felix Del Tredici
- Sarah Albu
- Julian Klein
tags:
- haptics
- vibrotactile
- wearable score
categories: []
date: '2016-01-01'
lastmod: 2022-02-18T12:12:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:17.896806Z'
publication_types:
- '1'
abstract: Situational scores, in this paper, are defined as scores that deliver time- and context-sensitive score information to musicians at the moment when it becomes relevant. Mnemonic (rule/style-based) scores are the oldest score models of this type. Lately, reactive, interactive, locative scores have added new options to situative scoring. The body:suit:score is an interface currently developed in collaboration of four labs at Concordia and McGill Universities in Montréal - an interface that will allow the musical use of all four types of situational score. Musicians are clad in a body-hugging suit with embedded technology - this suit becomes their score interface. Ultimately intended to enable ensembles to move through performance spaces unencumbered by visual scores and their specific locations, the project currently enters its second year of research-creation. The paper discusses the closely intertwined technological, ergonomic, performance-psychology-based and artistic decisions that have led to a first bodysuit prototype - a vibrotactile suit for a solo musician. It will also discuss three etude compositions by Sandeep Bhagwati and Julian Klein for this prototype, and their conceptual approaches to an artistic use of the body:suit:score interface. Finally, the paper discusses next steps and emergent problems and opportunities, both technological and artistic.
publication: "*Proceedings of the International Conference on Technologies for Music Notation and Representation (TENOR)*"
links:
- name: URL
  url: http://idmil.org/publication/musicking-the-body-electric-the-bodysuitscore-as-a-polyvalent-score-interface-for-situational-scores/
---
