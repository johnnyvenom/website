---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'NIME and the Environment: Toward a More Sustainable NIME Practice'
subtitle: ''
summary: ''
authors:
- Raul Masu
- Adam Pultz Melbye
- John Sullivan
- Alexander Refsum Jensenius
author_notes:
- "Equal contribution"
- "Equal contribution"
- "Equal contribution"
- "Equal contribution"
tags:
- nime/eco
categories: []
date: '2021-01-01'
lastmod: 2022-02-18T12:12:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:18.396702Z'
publication_types:
- '1'
abstract: This paper addresses environmental issues around NIME research and practice. We discuss the formulation of an environmental statement for the conference as well as the initiation of a NIME Eco Wiki containing information on environmental concerns related to the creation of new musical instruments. We outline a number of these concerns and, by systematically reviewing the proceedings of all previous NIME conferences, identify a general lack of reflection on the environmental impact of the research undertaken. Finally, we propose a framework for addressing the making, testing, using, and disposal of NIMEs in the hope that sustainability may become a central concern to researchers.
publication: '*Proceedings of the International Conference on New Interfaces for Musical Expression*'
doi: 10.21428/92fbeb44.5725ad8f
---

This paper was written collaboratively by the three environmental officers of the [International Conference on New Interfaces for Musical Expression (NIME)](https://nime.org), along with the chair of the NIME Steering Committee. Authorship is attributed equally regardless of author order.
