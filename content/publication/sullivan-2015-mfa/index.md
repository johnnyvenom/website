---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Interaction and the Art of User-Centered Digital Musical Instrument Design
subtitle: ''
summary: ''
authors:
- John Sullivan
tags:
- design
- digital musical instruments
- Installation
- interaction
- interface
- multimedia
- music performance
- user experience
- user-centered design
categories: []
date: '2015-01-01'
lastmod: 2022-02-18T12:12:19-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
# projects: ["research/noisebox", "interactive/foursquare"]
# projects: ["interactive/foursquare"]
# projects: ["design/noisebox"]

publishDate: '2022-02-18T17:12:19.844362Z'
publication_types:
- '7'
abstract: This thesis documents the formulation of a research-based practice in multimedia art, technology and digital musical instrument design. The primary goal of this research was to investigate the principles and methodologies involved in the structural design of new interactive digital musical instruments aimed at performance by members of the general public, and to identify ways that the design process could be optimized to increase user adoption of these new instruments. The work is presented in two sections. The first covers early studies in user interaction and exploratory works in web and visual design, sound art, installation, and music performance. The second section is dedicated to focused research on digital musical instrument design through two major projects. The first was the design and prototype of the Noisebox, a new digital musical instrument. The purpose of this project was to learn the various stages of instrument design through practical application. A working prototype has been presented and tested, and a second version is currently being built. The second project was a user study that surveyed musicians about digital musical instrument use. It asked questions about background, instrument choice, music styles played, and experiences with and attitudes towards new digital musical instruments. Based on the results of the two research projects, a model of digital musical instrument design is proposed that adopts a user-centered focus, soliciting user input and feedback throughout the design process from conception to final testing. This approach aims to narrow the gap between conceptual design of new instruments and technologies and the actual musicians who would use them.
publication: "*Master's thesis, University of Maine*"
links:
- name: URL
  url: https://digitalcommons.library.umaine.edu/etd/2330/
---
