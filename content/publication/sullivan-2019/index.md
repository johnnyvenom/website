---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Surveying Digital Musical Instrument Use Across Diverse Communities of Practice
subtitle: ''
summary: ''
authors:
- John Sullivan
- Marcelo M. Wanderley
tags:
- ★
- communities of practice
- digital musical instruments
- nime
- survey
categories: []
date: '2019-01-01'
lastmod: 2022-02-18T12:12:19-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:19.236873Z'
publication_types:
- '1'
abstract: 'An increasing number of studies have examined the active practice of performers who use digital musical instruments (DMIs) and applied findings towards recommendations for the design of new technologies. However, the communities of practice typically considered in these works tend to be closely aligned with the design communities themselves, predominantly found in academic research and experimental and technology-based music practices. Here we report on an online survey of musicians designed to look beyond these distinct communities to identify trends in DMI use across a wide variety of practices. Compared with current literature in the field, our diversified group of respondents revealed a different set of important qualities and desirable features in the design of new instruments. Importantly, for active and professional performers, practical considerations of durability, portability and ease of use were prioritized. We discuss the role of musical style and performance practice in the uptake and longitudinal use of new instruments, and revisit existing design guidelines to allow for the new findings presented here.'
publication: '*Proceedings of the International Symposium on Computer Music Multimedia Research*'
links:
- name: URL
  url: http://idmil.org/publication/surveying-digital-musical-instrument-use-across-diverse-communities-of-practice/
---
