---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Design of Vibrotactile Feedback and Stimulation for Music Performance
subtitle:
summary: ''
authors:
- Marcello Giordano
- John Sullivan
- Marcelo M. Wanderley
tags:
- feedback
- haptics
- tactons
- wearable display
categories: []
date: '2018-01-01'
lastmod: 2022-02-18T12:12:18-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-02-18T17:12:18.286646Z'
publication_types:
- '6'
abstract: 'Haptics, and specifically vibrotactile-augmented interfaces, have been the object of much research in the music technology domain: In the last few decades, many musical haptic interfaces have been designed and used to teach, perform, and compose music. The investigation of the design of meaningful ways to convey musical information via the sense of touch is a paramount step toward achieving truly transparent haptic-augmented interfaces for music performance and practice, and in this chapter we present our recent work in this context. We start by defining a model for haptic-augmented interfaces for music, and a taxonomy of vibrotactile feedback and stimulation, which we use to categorize a brief literature review on the topic. We then present the design and evaluation of a haptic language of cues in the form of tactile icons delivered via vibrotactile-equipped wearable garments. This language constitutes the base of a “wearable score” used in music performance and practice. We provide design guidelines for our tactile icons and user-based evaluations to assess their effectiveness in delivering musical information and report on the system’s implementation in a live musical performance.'
publication: '*Musical Haptics, Springer Series on Touch and Haptic Systems*'
doi: 10.1007/978-3-319-58316-7
links:
- name: URL
  url: http://link.springer.com/10.1007/978-3-319-58316-7_10
---
