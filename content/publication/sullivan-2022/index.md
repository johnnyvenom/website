---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Surveying Digital Musical Instrument Use in Active Practice
subtitle: ''
summary: ''
authors:
- John Sullivan
- Catherine Guastavino
- Marcelo M. Wanderley
tags:
- design
- Digital musical instruments
- engagement
- NIME
- survey
categories: []
date: '2022-02-18'
lastmod: 2022-02-18T12:12:19-05:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: ["research/EMI-survey"]
publishDate: '2022-02-18T17:12:19.709319Z'
publication_types:
- '2'
abstract: Digital musical instruments are frequently designed in research and experimental performance contexts but few are taken up into sustained use by active and professional musicians. To identify the needs of performers who use novel technologies in their practices, a survey of musicians was conducted that identified desirable qualities for instruments to be viable in active use, along with attributes for successful uptake and continued use of instruments based on frameworks of long and short term user engagement. The findings are presented as a set of design considerations towards the development of instruments intended for use by active and professional performers.
publication: '*Journal of New Music Research*'
doi: 10.1080/09298215.2022.2029912
---
