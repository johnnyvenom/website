---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Welcome"
subtitle: ""
summary: "Welcome to a new johnnyvenom.com website."
authors: []
tags: ["other"]
categories: []
date: 2020-09-30T15:24:46-04:00
lastmod: 2020-09-30T15:24:46-04:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: "Tapbox, a new DMI"
  focal_point: "Smart"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Hi folks, and welcome to a new website. I've had the same WordPress site for the last few years, but lately it was getting a bit long in the tooth so I decided to retire it. 

I've always been an ardent supporter of WordPress for both my own and client sites, but have been interested to try something new, something less fiddly, less CMS, less... bloated. Static site generators have been around for a while and seem to fit the bill for something lightweight, fast-loading and easy to manage, so here is a trial run. 

If you see this post a few months from now, it means everything went well and the site stays. For the present, stand by, it will probably look a little generic and empty, but that will change soon.. 

Cheers,
Johnny

-----

