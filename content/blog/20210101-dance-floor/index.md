---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Dance Floor"
subtitle: ""
summary: ""
authors: []
tags: ["music", "other"]
categories: []
date: 2021-01-01T18:20:36-05:00
lastmod: 2021-01-01T18:20:36-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

{{< audio src="dance_floor.mp3" >}}

## "Dance Floor"

**Artist:** Shannon Lawrence  
**Album:** Party Time EP  
**Year:** 2012  
**Production:** Johnny Venom  
**Additional production:** Erin Lawrence  

In celebration of my niece's 18th birthday today (1-January 2021... Happy New Year!!! Woot 🎉), Here's a birthday track we recorded on her 9th birthday. 

**_STILL A BANGER...._**

{{< figure src="IMG_2303.jpg" title="Shannon Lawrence, circa 2012" >}}

-----

