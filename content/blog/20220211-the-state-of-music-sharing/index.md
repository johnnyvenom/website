---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The State of Music Sharing"
subtitle: "A whitepaper on digital file sharing practices for and amongst music inductry professionals."
summary: "A survey on the complex practices, workflows and preferences for sharing audio files across the music industry."
authors: []
tags: []
categories: []
date: 2021-012-21T10:30:02-05:00
lastmod: 2022-02-11T10:30:02-05:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

links:
# - name: "Download the free whitepaper"
#   url: https://byta.com/resources/whitepaper/state-of-music-sharing-2021
- name: "Byta"
  url: https://byta.com
url_pdf: "/files/Byta_State_of_Music_Sharing_2021.pdf"
---

This last summer, after finishing my Ph.D. in May, I teamed up with the audio file sharing company [Byta](https://byta.com/) to conduct a survey with music industry professionals - artists, managers, record labels, promoters, publishers and more - to investigate and understand the complex ecosystem of tools, workflows and preferences for sharing digital audio files in their professional practices. 

The results have now been published in a whitepaper entitled "The State of Music Sharing": 

> In the world of music, few practices are more widespread than the sharing of digital audio files and streams. Getting anyone who works with music together in a room and then asking them about file sharing results in complaints about everything from poorly designed interfaces and wobbly security measures to inconvenient file formats and missing metadata. For the first time this free whitepaper, The State Of Music Sharing, will help put the daily challenges of sharing digital audio into context.
>
> Here’s what you’ll learn:
> 
> - What platforms are people using?
> - How do they prefer to send and receive files?
> - Do they pay for file-sharing services?
> - How do any of these things correlate with the various roles in the music ecosystem?

### Links

- Read the whitepaper: {{% staticref "files/Byta_State_of_Music_Sharing_2021.pdf" "newtab" %}}PDF{{% /staticref %}}
- Byta website page: [Byta.com](https://byta.com/resources/whitepaper/state-of-music-sharing-2021)

-----

