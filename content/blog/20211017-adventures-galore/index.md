---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Adventures Galore"
subtitle: "Check out the Adventures archive"
summary: "New site section: check out the new Adventures blog (under 'Etc' in the top navigation)!"
authors: []
tags: [ "personal", "blog", "adventure", "travel", "cycling" ]
categories: [ "personal", "meta" ]
date: 2021-10-17T12:52:17-04:00
lastmod: 2021-10-17T12:52:17-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

#### I've added a new section of my website: [Adventures]({{< relref "/blog-adventure-page" >}}). 

__*(It's under the 'Etc.' menu item in the top navigation bar, along with [Tastes]({{< relref "/blog-food-page">}}).)*__

The choice to put this content, which is primarily photos and videos, up on my website comes from my decision to leave most social media platforms and to self-host my own content.[^1] 

So far the Adventures section is an archive from my previous website, which was primarily dedicated to photos and videos from various travels. The bulk of it is an [11 part blog series]({{< relref "/blog-adventure/2017-12-08-a-few-days-in-bangalore" >}}) of a trip that my wife and I took to India in December 2017.

While the traveling has been toned down a bit for the last couple years (haven't really ventured too far since COVID hit), I have a backlog of adventures that haven't been posted, and recently I've been busy closer to home, bikepacking and gravel biking in the nearby Quebec countryside. So I'll continue to add new things whenever I can, and look forward to the next grand adventure. 

[^1]: There are, of course, some caveats to my social media exit: I keep my [Twitter](https://twitter.com/jonnyvenom) account active (though rarely post), ands for now my videos are hosted on YouTube. I had previously embedded videos on the site pages, but was never happy with the delivery, and have tried other platforms but haven't matched the features and usability of YouTube. 

