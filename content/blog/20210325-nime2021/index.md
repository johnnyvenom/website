---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "NIME 2021 paper accepted"
subtitle: ""
summary: ""
authors: []
tags: ["NIME", "ECO_NIME", "sustainability", "conference", "publication", "music technology", "environment"]
categories: ["music technology", "environment"]
date: 2021-03-25T11:20:27-04:00
lastmod: 2021-03-25T11:20:27-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Last week the [NIME 2021](https://nime2021.org) conference their reviews of submissions and I'm happy to say that our paper "“NIME and the Environment: Toward a More Sustainable NIME Practice” was accepted. It was coauthored by fellow NIME environmental officers [Raul Masu](https://twitter.com/raul_masu) and [Adam Pultz Melbye](https://adampultz.com/), along with NIME steering committee chair [Alexander Refsum Jensenius](https://www.uio.no/ritmo/english/people/management/alexanje/index.html). 

### Abstract

> This paper addresses environmental issues around NIME research and practice.  We discuss the formulation of an [environmental statement](https://nime.org/environment) for the conference as well as the initiation of a [NIME Eco Wiki](https://eco.nime.org) containing information on environmental concerns related to the creation of new musical instruments. We outline a number of these concerns and, by systematically reviewing the proceedings of all previous NIME conferences, identify a general lack of reflection on the environmental impact of the research undertaken. Finally, we propose a framework for addressing the making, testing, using, and disposal of NIMEs in the hope that sustainability may become a central concern to researchers. 

## IDMIL with 13 contributions accepted

Our paper is one of **_THIRTEEN_** submissions that were accepted to NIME this year from the [Input Devices and Music Interaction Laboratory](http://idmil.org) (IDMIL). This is a record for our lab! NIME reported that 55% of submissions were accepted; for our lab, we had 13 out of 14. Congratulations to all, and we certainly look forward to a great conference!

##### Read more at IDMIL.org: [NIME 2021 – 13 IDMIL contributions accepted!](http://idmil.org/2021/03/24/nime-2021-acceptances/)

{{< figure src="IDMIL_team_2019-2020.jpg" title="**Last known photo of the IDMIL research team (Fall 2019)**. *Back row:* Edu Meneses, Takuto Fukuda, Ivan Franco, Christian Frisson, Marcelo Wanderley, Felipe Verdugo, Geise Santos, Vincent Cusson, Josh Rohs, Mathias Kirkegaard, Mathias Bredholt. *Front row:* Travis West, Anésio Costa, Johnty Wang, Alex Nieva, John Sullivan." alt="IDMIL lab group photo" >}}

---

{{< cite page="/publication/masu-2021" view="4" >}}

-----

