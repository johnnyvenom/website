---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "2024 MODINA Residency - Bucharest"
subtitle: "CNDB. Bucharest, Romania. January to March 2024." # shown on post page only
summary: "From Jan 15 to March 9th, I'm in residency the National Centre for Dance Bucharest working on a new performance." # shown on aggregate pages 
authors: []
tags:
  - dance
  - music
  - research
  - residencies
  - artificial intelligence
categories: []
date: 2024-01-16T12:29:53+02:00
lastmod: 2024-01-16T12:29:53+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 3
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

{{< callout note >}}

<div style="font-size: .9rem">
To start 2024, I'm collaborating with my colleague <a href="https://saraloui.com" target="_blank">Sarah Fdili Alaoui</a> for an artist/research residency at the <a href="https://cndb.ro/en/" target="_blank">National Centre for Dance Bucharest</a>. We are creating an experimental live performance entitled "<a href="https://modina.eu/projects/for-patricia" target="_blank">For Patricia</a>" for two dancers and two musicians, where the music and choreography will be directed by an AI agent. The residency is part of the <a href="https://modina.eu" target="_blank">MODINA</a> (Movement, Digital Intelligence and Interactive Audience) project on dance performance and technology. 
<br />
I post regularly to the federated social media platform <a href="https://pixelfed.social/i/web/profile/470516890990484313" target="_blank">Pixelfed</a>, and will add content to this page to record the daily sights and sounds of my time here in Bucharest.
</div>

{{< /callout >}}

<!-- [^1]: If you like sharing content online but are ***not*** okay with handing your personal data over to evil corporate overlords, check out [Pixelfed](https://pixelfed.org)!!! -->

## Day 0: Arriving in Bucharest

<div class="center-embed"><iframe title="Pixelfed Post Embed" src="https://pixelfed.social/p/johnnyvenom/652617673640266594/embed?caption=true&likes=true&layout=full" class="pixelfed__embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script async defer src="https://pixelfed.social/embed.js"></script></div> 

<p class="video-caption">I flew from Paris, arriving in the afternoon. Once installed into my apartment in Sector 4, I took a walk around the city to get familiar with my surroundings.</p> 

## Day 1: Settling in

<div class="center-embed">
  <iframe title="Pixelfed Post Embed" src="https://pixelfed.social/p/johnnyvenom/652614435417260706/embed?caption=true&likes=true&layout=full" class="pixelfed__embed" style="max-width: 100%; border: 0" allowfullscreen="allowfullscreen"></iframe><script async defer src="https://pixelfed.social/embed.js"></script>
</div>

<p class="video-caption">Time to get to work! I arrived at CNDB, my home for the next two months.</p>

<div class="center-embed">
  <iframe title="Pixelfed Post Embed" src="https://pixelfed.social/p/johnnyvenom/652774045735048586/embed?caption=true&likes=true&layout=full" class="pixelfed__embed" style="max-width: 100%; border: 0" allowfullscreen="allowfullscreen"></iframe><script async defer src="https://pixelfed.social/embed.js"></script>
</div>

<p class="video-caption">In the afternoon, I took a break to go for a run and discover Parcul Carol I nearby my home.</p>

## Day 2: CNDB

<div class="center-embed">
  <iframe title="Pixelfed Post Embed" src="https://pixelfed.social/p/johnnyvenom/652907128516145496/embed?caption=true&likes=false&layout=full" class="pixelfed__embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script async defer src="https://pixelfed.social/embed.js"></script>
</div>

{{< callout note >}}

## Update

Well, the residency was intense and is now concluded! My best laid plans to provide daily updates ended on [checks notes] ...day 2? 

But, fear not, I have created a new [page]({{< relref "../../project/interactive/for-patricia/" >}}) that has more information, links, photos and videos! 

{{< figure src="for_patricia_walk.jpg" caption="Live performance of 'For Patricia', March 1st, 2024 at [CNDB](https://cndb.ro) in Bucharest, Romania." >}}

I will eventually come back and fill in this page with more general sights, sounds, and reflections of my two months in Bucharest, but for now please check out [For Patricia project]({{< relref "../../project/interactive/for-patricia/" >}}) for all the info about the residency!
<br/>
<br/>

{{< /callout >}}