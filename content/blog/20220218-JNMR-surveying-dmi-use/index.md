---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Surveying Digital Musical Instrument Use in Active Practice"
subtitle: ""
summary: "Happy to annouce the publication of a new research article available via Open Access from the Journal of New Music Reearch."
authors: []
tags: ["NIME", "DMI", "article"]
categories: ["music technology", "publication", "research"]
date: 2022-02-18T17:37:59-05:00
lastmod: 2022-02-18T17:37:59-05:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: true

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.

# projects: [research/EMI-survey]

links:
- name: "PDF"
  url: /publication/sullivan-2022/sullivan-2022.pdf
- name: "DOI"
  url: https://doi.org/10.1080/09298215.2022.2029912
- name: "Project page"
  url: /project/research/emi-survey

# url: #
---

[{{< figure src="featured.jpg" >}}](https://doi.org/10.1080/09298215.2022.2029912)

It's here! Our new article "Surveying digital musical instrument use in active practice" has been published in the [Journal of New Music Research](https://www.tandfonline.com/journals/nnmr20). Co-authored by Catherine Guastavino and Marcelo M. Wanderley, the article presents our wide ranging study on the adoption, continued use, and potential abandonment of digital instruments and interfaces in the real-world performance practices of musicians. 

Our intent for the research was to identify factors for, and elements of, both short and long term engagement with new performance technologies that could be of use to instrument designers, and the article summarizes the survey findings into a set of design considerations that can guide the development of new instruments. 

We are exceptionally happy to share our research with research and performance communities like [NIME](https://nime.org) (New Interfaces for Musical Expression) and beyond. While academic publishing is fraught with paywalls and barriers to free access to research, this article is available via Open Access and can be [downloaded and read for free](https://doi.org/10.1080/09298215.2022.2029912). 

I hope you enjoy reading it and, if you do and are interested in this sort of research, that it can be of use to your own endeavours. 

> ## Abstract
> 
> Digital musical instruments are frequently designed in research and experimental performance contexts but few are taken up into sustained use by active and professional musicians. To identify the needs of performers who use novel technologies in their practices, a survey of musicians was conducted that identified desirable qualities for instruments to be viable in active use, along with attributes for successful uptake and continued use of instruments based on frameworks of long and short term user engagement. The findings are presented as a set of design considerations towards the development of instruments intended for use by active and professional performers.

---

{{< cite page="/publication/sullivan-2022" view="4" >}}

-----

