---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "From Fiction to Function: New article in the Computer Music Journal"
subtitle: "Our article on envisioning new digital musical instruments through design workshops is now published in open access." # shown on post page only
summary: "Our article on envisioning new digital musical instruments through design workshops is now published in open access." # shown on aggregate pages 
authors: []
tags: ["NIME", "DMI", "article", "HCI", "design fiction", "workshops"]
categories: ["music technology", "publication", "research"]
date: 2023-11-26T12:23:22+01:00
lastmod: 2023-11-26T12:23:22+01:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 1
  caption: ""
  focal_point: ""
  preview_only: false

links:
- name: "Publication"
  url: /publication/sullivan-2022a/
- name: "DOI"
  url: https://doi.org/10.1162/comj_a_00644
- name: "Project"
  url: /project/research/design-for-performance
---

I'm happy to announce that our new article entitled "From Fiction to Function: Imagining New Instruments Through Design Workshops" is now published and on the front cover of the [Computer Music Journal](https://direct.mit.edu/comj/issue/46/3)! The article reports on design fiction workshops that we conducted with professional and expert musicians to envision new kinds of musical instruments and interfaces though a nonfunctional paper prototyping activity. Our primary results were twofold: 

First, we reflect on the use of design fiction as a valuable method for generating new ideas for designing interaction, and suggest opportunities for formulating a general framework that can link early-stage ideation and non-functional prototyping with actionable design inputs. 

Second, many of the ideas that emerged were distilled into a set of design specifications that we used to develop three new versions of an digital musical instrument called the Noisebox. To read more about the Noiseboxes, you can visit the [project page]({{< relref "project/research/noisebox" >}}) and read our [paper]({{< relref "/publication/sullivan-2020" >}}) presented at the NIME conference about the instruments that came from the workshop design ideas.

On a personal note, it has taken a relatively long time for this work to come to publication, but I am proud the work we did here and hope that it is of value to others across HCI, design, and music technology communities. Many thanks and congratulations to my co-authors and mentors Marcelo M. Wanderley and Catherine Guastavino! 🥂
