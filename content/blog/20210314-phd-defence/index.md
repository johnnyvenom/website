---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Ph.D. defended!"
slug: phd-defended
subtitle: ""
summary: 'On March 11th (the 365th day of COVID-19 realities here in Montreal as well as the rest of the world), I successfully defended my Ph.D. dissertation "Built to Perform: Designing Digital Musical Instruments for Professional Use".'
authors: []
tags: ["Ph.D.", "dissertation", "DMI", "music technology"]
categories: ["academic"]
date: 2021-03-14T15:28:54-04:00
lastmod: 2021-03-14T15:28:54-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

url_slides: "/talk/20210314_phd_defence/PhD_defence_slides.pdf"
---

On March 11th (the 365th day of COVID-19 realities here in Montreal as well as the rest of the world), I successfully defended my Ph.D. dissertation "Built to Perform: Designing Digital Musical Instruments for Professional Use". In normal times it would have taken place in a large bright room at the [Centre for Interdisciplinary Research in Music Media and Technology](https://www.cirmmt.org) with the examination committee and members of the public in attendance. However in true sign of the times, I sat at my desk alone in the lab and it was conducted via Zoom, while the public was invited to watch a livestream on YouTube. How very modern...

{{< figure src="defence_my_view.jpeg" caption="My view of the proceedings" >}}

Despite the slightly disembodied medium, the examination went exceptionally well. In the first stage, I gave a 20 minute [presentation](PhD_defence_slides.pdf) of my dissertation research. Questions from the committee followed and after a short deliberation, the committee returned to deliver the wonderful news that I had passed. 

*My final dissertation submission was made on April 13th, 2021, and is now [available](/publication/phd-thesis) publicly. You can read the abstract [below](#thesis-abstract).

{{< figure src="defence_rauls_view.jpeg" caption="Audience view, at a sunny bar somewhere in Portugal..." >}}

I owe a big thank you to my defence committee: Pro-Dean [Ante L. Padjen](https://publications.mcgill.ca/medenews/2019/04/25/i-medici-di-mcgill-celebrating-30-years-of-concerts-for-causes/), Graduate Music Associate Dean [Lena Weman](https://www.mcgill.ca/music/lena-weman), Internal Examiner [Max Evans](https://www.mcgill.ca/sis/people/faculty/evans), External Member [Paul Stapleton](http://www.paulstapleton.net/) (SARC, Queens University Belfast), and my two co-supervisors [Catherine Guastavino](https://www.mcgill.ca/sis/people/faculty/guastavino) and [Marcelo Wanderley](http://idmil.org/people/marcelo-m-wanderley/). Thanks also to External Examiner [Adnan Marquez-Borbon](https://adnanmarquezborbon.wordpress.com/) (Universidad Autónoma de Baja California (UABC)), who was not present at the defence but was an integral part of the dissertation examination.  

{{< figure src="after_party_phd.png" caption="In true COVID style, the afterparty was a [Jitsi](https://meet.jit.si) affair.">}}

<br>

---

<br>

## Thesis Abstract

The field of digital musical instrument (DMI) design is highly interdisciplinary and comprises a variety of different approaches to developing new instruments and putting them into artistic use. While these vibrant ecosystems of design and creative practice thrive in certain communities, they tend to be concentrated within the context of contemporary experimental musical practice and academic research. In more widespread professional performance communities, while digital technology is ubiquitous, the use of truly novel DMIs is uncommon. 

The aim of this dissertation is to investigate the unique demands that active and professional performers place on their instruments, and identify ways to address these concerns throughout the design process that can facilitate development of instruments that are viable and appealing for professionals to take up into long-term practice. The work presented here represents three phases of user-driven research, using methods drawn from the fields of Human-Computer Interaction and Human-Centered Design. First, a survey of musicians was conducted to understand how DMIs are used across diverse performance practices and identify factors for user engagement with new instruments. Second, design workshops were developed and run with groups of expert musicians that employed non-functional prototyping and design fiction as methods to discover design priorities of performers and develop tangible specifications for future instrument designs. Finally, multiple new DMIs have been designed in two primary contexts: first, three instruments were developed in response to the workshop specifications to meet general criteria for DMI performance; second, two systems for augmented harp performance were built and integrated into a musician's professional practice based on a long-term research-design collaboration. 

Through these projects, I propose the several contributions that will aid designers in the development of new DMIs intended for professional performers. The survey results have been distilled into a list of considerations for designers to address the unique demands and priorities of active performers in the development of new DMIs. A complete methodology has been developed to generate design specifications for novel DMIs that leverages the tacit knowledge of skilled performers. Finally, I offer practical guidelines, tools and suggestions in the technical design and manufacture of instruments that will be viable for use in long-term professional practice. 

---

{{< cite page="/publication/sullivan-2021-phd" view="4" >}}

-----

