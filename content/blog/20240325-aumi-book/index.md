---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "The AUMI book has landed!"
subtitle: "Improvising Across Abilities: Pauline Oliveros and the Adaptive Use Musical Instrument" # shown on post page only
summary: "Our new book on Pauline Oliveros and AUMI - the Adaptive Use Musical Instrument - is out now." # shown on aggregate pages 
authors: []
tags: 
  - accessible design
  - NIME
  - digital musical instruments
  - interview
  - Pauline Oliveros
categories:
  - music technology
  - publication
  - book
  - research
date: 2024-03-25T17:26:21+01:00
lastmod: 2024-03-25T17:26:21+01:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  placement: 1
  caption: ""
  focal_point: "center"
  preview_only: false

links:
- name: "Publication"
  url: /publication/aumi-2024/
- name: "DOI"
  url: https://doi.org/10.3998/mpub.11969438
- name: "Project"
  url: /project/research/adaptive-use-musical-instrument

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

<!-- content goes here -->
I returned from my [residency in Bucharest]({{< relref "20240114-bucharest-modina-residency" >}}) to find a package containing my copy of our new book hot off the presses.

AUMI, the Adaptive Use Musical Instrument, is a long-running project started by the wonderful Pauline Oliveros and her close friend and occupational therapist Leaf Miller, who envisioned the use of new technologies to enable people of all abilities to participate in making music together. During my Ph.D. [I worked on the development of the AUMI desktop application]({{< relref "../../project/research/adaptive-use-musical-instrument" >}}), a digital musical instrument that uses computer vision to map movement to music creation. 

The book is now published by the [University of Michigan press](https://doi.org/10.3998/mpub.11969438), and freely available in Open Access. 


I co-authored a chapter chronicling the development of the technology and research at McGill University. The story is based on interviews I conducted with the development team - Ivan Franco, Ian Hattwick, Thomas Ciufo, and Eric Lewis. I also was a member of the AUMI Editorial Collective, who collaboratively edited the book. 

### [👉 👉 👉 READ IT NOW! 👈 👈 👈](https://doi.org/10.3998/mpub.11969438)

{{< callout note >}}
{{< figure src="aumi_book_flyer.jpg" >}}
{{< /callout >}}

### Publication

- The AUMI Editorial Collective (Eds.) (2024). [Improvising Across Abilities: Pauline Oliveros and the Adaptive Use Musical Instrument]({{< relref "../../publication/aumi-2024" >}}). University of Michigan Press.

-----



