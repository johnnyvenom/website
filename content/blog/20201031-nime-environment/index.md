---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Working towards environmental sustainability in NIME"
subtitle: ""
summary: "I'm happy to be serving alongside Adam Pultz Melbye and Raul Masu as an environmental officer for the NIME conference. We have published statement on the environmental goals for planning of future NIME conferences as well as promoting sustainability in NIME research."
authors: []
tags: ["NIME", "sustainability", "environment", "music technology"]
categories: ["music technology", "environment"]
date: 2020-10-31T15:55:21-04:00
lastmod: 2020-10-31T15:55:21-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

The [2020 International Conference on New Interfaces for Musical Expression](https://nime2020.bcu.ac.uk/) (NIME) was special for many reasons. Personally, it marked my first active participation in the conference, with two accepted papers [[1], [2]]. For everyone, the conference was unique in that it was held entirely online due to COVID-19 restrictions. The organizers [did an incredible job](https://medium.com/swlh/organising-an-online-conference-937d0f4e4c48) putting the conference together in this alternate format on the fly, for which I would like to extend my deep thanks. 


The extraordinary time in which we live, and the circumstances around which the conference was held contributed to spirited discussions throughout the conference and after on a number of topics including accessibility, ethics, diversity and the environment, and led to the recruitment of a number of officers to address these issues, both in the planning of future NIME conferences and in the collective research practices of the NIME community. 


I am happy to be serving as one of three NIME environmental officers, alongside colleagues [Adam Pultz Melbye](http://www.adampultz.com/) and [Raul Masu](https://www.researchgate.net/profile/Raul_Masu2), and we have drafted an [environmental statement](https://nime.org/environmnent) for the NIME conference that lays out our goals. 

> Every action we perform, including research, has an impact on our ecosystem. NIME is committed to environmental sustainability and conservation in both the delivery of the annual conference and in our day to day research and artistic practices. 

Read our complete statement [here](https://www.nime.org/environment).

{{< figure src="biosphere.png" title="Biosphere at Île Sainte-Hélène, Montréal, Canada. Summer 2020" >}}

[1]: https://www.johnnyvenom.com/publication/sullivan-2020/
[2]: https://www.johnnyvenom.com/publication/nime-20-54/

-----

