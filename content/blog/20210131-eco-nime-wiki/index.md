---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "ECO_NIME: A wiki for sustainable NIME research"
subtitle: ""
summary: "The NIME officers are happy to introduce the ECO_NIME wiki, a community-based repository information and resources about environmental sustainability in NIME research and artistic practice."
authors: []
tags: ["NIME", "wiki", "sustainability", "music technology", "environment"]
categories: ["music technology", "environment"]
date: 2021-01-19T19:01:28-04:00
lastmod: 2021-03-19T19:01:28-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# To fit properly to 'card' display, images should be 9x16 landscape. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

In a previous [post]({{< relref "/blog/20201031-nime-environment" >}}), I wrote about becoming an environmental officer for the [NIME conference](https://nime.org) alongside [Adam Pultz Melbye](https://www.adampultz.com/) and [Raul Masu](https://twitter.com/raul_masu). 

A major initiative we put together is the ECO_NIME wiki, a community-based repository information and resources about environmental sustainability in NIME research and artistic practice. The wiki URL is [eco.nime.org](https://eco.nime.org). It is currently in its infancy and we look forward to growing it this year, especially around the NIME 2021 conference. 

## From the wiki:

> To support sustainable practices within and beyond NIME as outlined in the NIME Conference [Environmental Statement](https://nime.org/environment), we have created this repository of information and green resources for environmental issues in NIME research. The repository is regularly updated with new entries, ideas, and suggestions for how we can lessen the ecological footprint of our individual and institutional practices. It is softly curated by the environmental officers, and we highly encourage everyone to [submit requests](https://eco.nime.org/contribute-to-eco_nime/contribute) for additions and edits. We hope that this will become a resource, not only for evaluating our own environmental impact, but also to spawn new ideas, designs, sonic works, and collaborations, with the potential for impact beyond the boundaries of NIME research as well.

We sincerely hope that as the wiki grows, it will not only raise awareness of environmental issues that are involved with NIME practice, but also provide tools and knowledge for research and artistic communities to meet these challenges. 

-----

