+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Skills"
subtitle = ""

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons

[[feature]]
  icon = "fingerprint"
  icon_pack = "fas"
  name = "Human-Computer Interaction"
  description = "interface design and evaluation"

[[feature]]
  icon = "users"
  icon_pack = "fas"
  name = "User-Centered Design"
  description = "human-centered and participatory design"

[[feature]]
  icon = "drafting-compass"
  icon_pack = "fas"
  name = "Digital Lutherie"
  description = "digital musical instrument design"

[[feature]]
  icon = "object-group"
  icon_pack = "fas"
  name = "Rapid Prototyping"
  description = "computer-aided design, 3D printing, laser cutting"

[[feature]]
  icon = "code-branch"
  icon_pack = "fas"
  name = "Creative Coding"
  description = "audio and interactive media programming"
  
[[feature]]
  icon = "street-view"
  icon_pack = "fas"
  name = "Motion Capture"
  description = "movement and performance gesture analysis"  

# Uncomment to use emoji icons.
# [[feature]]
#  icon = ":smile:"
#  icon_pack = "emoji"
#  name = "Emojiness"
#  description = "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
# [[feature]]
#  icon = "your-custom-icon-name"
#  icon_pack = "custom"
#  name = "Surfing"
#  description = "90%"

+++
