+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Postdoctoral researcher"
  company = "ex)situ, LISN, Université Paris-Saclay"
  company_url = "https://ex-situ.lri.fr/"
  location = "Paris, France"
  date_start = "2022-09-01"
  date_end = ""
  description = """
  
  - HCI research and design for the EU funded project "LivingArchive: Interactive documentation of dance" 
  
  """

[[experience]]
  title = "Post-Doctoral Fellow"
  company = "Shared Reality Lab, McGill University"
  company_url = "http://srl.mcgill.ca/"
  location = "Montreal, Canada"
  date_start = "2021-09-01"
  date_end = "2021-04-30"
  description = """
  
  - Designer and researcher on projects involving haptics and multimodal interaction
  
  """

[[experience]]
  title = "Ph.D. researcher and lecturer"
  company = "McGill University"
  company_url = ""
  location = "Montreal, Canada"
  date_start = "2015-09-01"
  date_end = "2021-05-28"
  description = """
  
  - Scientific research on music interaction, digital interface design and HCI
  - Lecturer for course on music and interactive media programming
  
  """

  [[experience]]
  title = "Research and teaching assistant"
  company = "University of Maine"
  company_url = ""
  location = "Maine, United States"
  date_start = "2012-09-01"
  date_end = "2015-08-15"
  description = """
  
  - Practice-based research on interactive media design
  - Rapid prototyping, hardware and electronics design
  - Audio and multimedia programming
  - Teaching support for courses on new media production and design 
  
  """

[[experience]]
  title = "Owner"
  company = "Johnny Venom Media"
  company_url = ""
  location = "Montreal, Canada"
  date_start = "2011-01-01"
  date_end = ""
  description = """
  
  - Creative design and content creation for businesses and artistic projects
  - Web design
  - Audio production and editing
  - Video and multimedia production
  - Audiovisual installation design 

  """

[[experience]]
  title = "Performing and recording artist"

  company_url = ""
  location = "Maine, New York"
  date_start = "2000-01-01"
  date_end = "2015-12-31"
  description = """
  
  - Professional bassist and multi-instrumentalist
  - Released several albums in various styles: indie, alternative rock, reggae, experimental
  - Toured in United States, Canada, and Europe
  - Session musician
  - Private instruction for bass and guitar
  
  """

+++
