---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Tastes"
slug: "tastes"
subtitle: ""
summary: "Another blog, this one about various food and beverage related pursuits"
authors: []
tags: ["food", "drink"]
categories: ["blogs"]
date: 2020-10-11T07:05:22-04:00
lastmod: 2020-10-11T07:05:22-04:00
featured: false
draft: false

type: "widget_page"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

-----

