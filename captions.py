import os
import sys

def create_file_list(input_directory, output_directory):
    try:
        # Check if the provided input directory exists
        if not os.path.exists(input_directory):
            print(f"The input directory '{input_directory}' does not exist.")
            return

        # Check if the provided output directory exists, or create it if it doesn't
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)

        # Get a list of filenames in the input directory
        file_list = os.listdir(input_directory)

        # Filter out subdirectories, keeping only files
        file_list = [filename for filename in file_list if os.path.isfile(os.path.join(input_directory, filename))]

        # Sort the list of filenames alphabetically
        file_list.sort()

        album_name = input_directory.split("albums/")
        gallery_name = album_name[1].split('/') 
        output_file_name = gallery_name[len(gallery_name)-1] + "_captions_list.txt"

        # Create a new text file in the output directory to store the list of filenames
        output_file_path = os.path.join(output_directory, output_file_name)
        with open(output_file_path, 'w') as file:
            file.write(f"gallery_item:\n")
            for filename in file_list:
                file.write(f"- album: {album_name[1]}\n")
                file.write(f"  image: {filename}\n")
                file.write(f"  caption: \n")

        print(f"File list has been saved to '{output_file_path}'.")

    except Exception as e:
        print(f"An error occurred: {str(e)}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python captions.py <input_directory> <output_directory>")
    else:
        input_directory = sys.argv[1]
        output_directory = sys.argv[2]
        create_file_list(input_directory, output_directory)
